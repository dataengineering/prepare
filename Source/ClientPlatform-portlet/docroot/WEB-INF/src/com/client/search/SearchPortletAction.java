package com.client.search;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.HttpUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.util.bridges.mvc.MVCPortlet;

import core.TfIdf;

public class SearchPortletAction extends MVCPortlet{
	
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException{
		
		PrintWriter pw = resourceResponse.getWriter();
		JSONArray array = JSONFactoryUtil.createJSONArray();
		JSONObject juser = JSONFactoryUtil.createJSONObject();

		String query = ParamUtil.getString(resourceRequest,"query");// get the query of user or the duration that a pdf is opened from jsp file
		System.out.println("Query : "+ query);
		if (query.contains("Open for :") && (query.contains("Query:"))){ // write in log file the duration time
			 File folderLog = new File("/home/"+System.getProperty("user.name") + "/.PrepareData/logs_duration/"); // folder which contain the files with the duration that a pdf is opened for a specific query
			 if (!folderLog.exists()){	// if file does not exist
			 	folderLog.mkdir();
			 	System.out.println("Created folder");
			 }
			 File fileLog = new File("/home/"+System.getProperty("user.name") + "/.PrepareData/logs_duration/" + query.split("\n")[0].replace("Query: ", ""));
			 if (!fileLog.exists()) {
				 System.out.println("filelog does not exist");
				 fileLog.createNewFile();
				 BufferedWriter bw = new BufferedWriter(new FileWriter(fileLog.getAbsoluteFile()));	// create a buffer writer for a file
				 bw.write("--" + query.split("\n",2)[1] + "\n\t\t-------------\n");
				 bw.close();
			 }
			 else {
				 ArrayList<String> lineOfLog = new ArrayList<String>();
				 BufferedReader bf = new BufferedReader(new FileReader(fileLog.getAbsoluteFile()));	// create a buffer for a file
				 String tempTerm;
	          	 while((tempTerm = bf.readLine()) != null ){	// while there are terms in configuration file
	          		lineOfLog.add(tempTerm);					// list with the already inserted durations of pdfs
	          	}
	          	bf.close();
          		fileLog = new File("/home/"+System.getProperty("user.name") + "/.PrepareData/logs_duration/" + query.split("\n")[0].replace("Query: ", ""));
          		BufferedWriter bw = new BufferedWriter(new FileWriter(fileLog.getAbsoluteFile()));	// create a buffer writer
	          	for(int i=0; i<lineOfLog.size();i++){
	          		bw.write(lineOfLog.get(i) + "\n");
	          	}
	          	 bw.write("--" + query.split("\n",2)[1] + "\n\t\t-------------\n");
	          	 bw.close();
			 }
		}else { // CASE CLIENT INSERT A QUERY
			System.out.println("Client has made a query");
			Hashtable<String, String> nameWithPath = new Hashtable<String, String>(); //hash table with key: name of Pdf's folder from repository, value:path of file in Liferay repository
			try {
				System.out.println("in try");
				List<DLFileEntry> ls = com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil.getDLFileEntries(-1,-1);
				long fileEntryId = 0, folderId = 0;
				for(DLFileEntry ts:ls){ // find the list of file entry id
					fileEntryId = ts.getFileEntryId(); // get id
					DLFileEntry fileEntry = com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil.getFileEntry(fileEntryId);
					fileEntry = fileEntry.toEscapedModel();

					if (fileEntry.getExtension().equals("pdf")){	// only for pdf file
						folderId = fileEntry.getFolderId();			// get the id of the folder
						String title = fileEntry.getTitle();		// get the title of pdf file
						
						ThemeDisplay themeDisplay=null;
						if (resourceRequest != null) {
							themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
						}
						String fileUrl = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + themeDisplay.getScopeGroupId() + "//" + folderId +  "//" + HttpUtil.encodeURL(HtmlUtil.unescape(title));
						nameWithPath.put(fileEntry.getName(), fileUrl);
					}
				}
			} catch (SystemException e1) {e1.printStackTrace();
			} catch (PortalException e) {e.printStackTrace();}

			
			//-----------------------------------READ PLAIN TEXT OF PDFS FROM INDEXED DATA------------------------------------\\
			//long start2 = System.currentTimeMillis();		
			Directory index = FSDirectory.open(new File("/home/"+System.getProperty("user.name") + "/.PrepareData/index_repository").toPath()); // the folder which contains the indexed data
			
			Hashtable<String, String[]> listOfPdfMetadata = new Hashtable<String, String[]>();//hash table with, key: name of Pdf, value:array with metadata
			ArrayList<String []> listOfPdfText = new ArrayList<String []>();
						
			IndexReader reader = DirectoryReader.open(index);	// open index
			int maxDoc=reader.maxDoc();							// Returns one greater than the largest possible document number
			for (int i=0; i < maxDoc; i++) {
				String [] pdfMetadata = new String [3];
			    String [] pdfElement = new String [2];
				Document doc=reader.document(i);
				pdfElement[0] = doc.get("\"name\"");
				pdfElement[1] = doc.get("\"text\"");
				pdfMetadata[0] = doc.get("\"title\"");
				pdfMetadata[1] = doc.get("\"author\"");
				pdfMetadata[2] = doc.get("\"keywords\"");

				listOfPdfMetadata.put(pdfElement[0], pdfMetadata);
				listOfPdfText.add(pdfElement);				// insert array in list
			 }
			//System.out.println("Time 2: " + (System.currentTimeMillis()-start2) + "\n\n");

			
			//-----------------------------------CODE FOR THE ESTIMATION OF TFIDF-----------------------------------\\
			//long start3 = System.currentTimeMillis();
			String [] wordsOfQuery = query.toLowerCase().split(" ");	// term of query
			System.out.println("wordsOfQuery : "+ wordsOfQuery);
			TfIdf tf = new TfIdf(listOfPdfText, "/home/" + System.getProperty("user.name") + "/.PrepareData/mystopword.txt"); // constructor has 2 argument: first is a array list with text file, second is the list with the stopwords
			String filePDF;	//Contains file name being processed
			
			ArrayList<Double> totalTfIdf = new ArrayList<Double>(); 		// array list with the total tf-idf value for each pdf for the giving query
			ArrayList<Double> totalTfIdfNotSort = new ArrayList<Double>(); 	// array list with the total tf-idf value for each pdf for the giving query, with sort descending
			ArrayList<String> arrayOfFiles = new ArrayList<String>();		// array list with the pdfs of repository which are classified
	
			tf.buildAllDocuments();
			Map<String, Double[]> myMap = new HashMap<String, Double[]>();
			Double [] values;
			for (Iterator<String> it = tf.documents.keySet().iterator(); it.hasNext(); ) {
				Double sumTfIdf = 0.0;
				filePDF = it.next();								// pointer to next file
				myMap = tf.documents.get(filePDF).getF_TF_TFIDF();
				
				for (int i=0;i<wordsOfQuery.length;i++){
					if(myMap.containsKey(wordsOfQuery[i])){
						values = myMap.get(wordsOfQuery[i]);
						sumTfIdf += values[2];	// Frequency = value[0], Term Frequency = value[1], TF-IDF = value[2]
					}
				}
				totalTfIdf.add(sumTfIdf);
				totalTfIdfNotSort.add(sumTfIdf);
				arrayOfFiles.add(filePDF);			
			}
			// find max of array list with total tf-idf => get the index of array list of the specific value => get the file from the specific position
			Collections.sort(totalTfIdf, Collections.reverseOrder());
			 for(Double tfidf: totalTfIdf){
				 if (tfidf>0.0){
					 String pdfName = arrayOfFiles.get(totalTfIdfNotSort.indexOf(tfidf));
					 array.put("Url of article: " + nameWithPath.get(pdfName.split("/")[1]) + " Title: " + listOfPdfMetadata.get(pdfName)[0] + "<br><font size=\"3\" color=\"Gray\">Author: " + listOfPdfMetadata.get(pdfName)[1] +" <br>Keywords: " + listOfPdfMetadata.get(pdfName)[2] + "</font><br><br>");
				 }
			}
			juser.put("recommendation", array);
			pw.println(juser.toString());
			System.out.println("Array : "+ array.toString());
			//System.out.println("Time 3: " + (System.currentTimeMillis()-start3) + "\n\n");
		}
	}
}
