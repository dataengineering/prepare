<%--
 @create on:	     1/04/2014
 @author:			 Konstantinos Pechlivanis
 @research program:  PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 @project:			 Analytic Platform (Work package 2)
  		-Task 2.1:   Development of Scientific means 
  		-Task 2.3:   Technical Platform
 
 @document: 		 view.jsp from file /html/jsps/receiver
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import = "com.liferay.portal.kernel.servlet.SessionMessages" %>
<%@ page import = "javax.portlet.PortletPreferences" %>
<portlet:defineObjects />
<portlet:resourceURL var="getActivationOpt"></portlet:resourceURL>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type='text/javascript' src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
    <script type='text/javascript' src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type='text/javascript' src='http://code.jquery.com/jquery.min.js'></script>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/buttons.css">
 	<script type="text/javascript" src="<%=request.getContextPath()%>/js/buttons.js"></script>
	
	<%-- import script for crontab --%>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-cron.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-cron.css"/>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-gentleSelect.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-gentleSelect.css"/>
	
    <link rel="stylesheet" href="<%=request.getContextPath()%>/css/jqx.base.css" type="text/css" />
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jqxcore.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jqxdata.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jqxdraw.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jqxchart.core.js"></script>
    	
	<script>
	var crawler = ''; var numOfTerm = 0; var cron_field = '';
	</script>
	  	
	<%-- take terms from SenderPortletAction class and insert them to receiver portlet and database --%>
	<script type="text/javascript"> 	
	$(function(){
		cron_field = $('#gentle').cron({// Initialise a cron object
		    useGentleSelect: true // default: false
		});
	});
	</script>
	
	<script>
	function warning() {
	    return "If you leave or refresh the page, the crawler will be canceled";
	}</script>
	
	<%-- receive from sender portlet the crawler to remove --%>
	<script>
	Liferay.on('removeCrawler',function(event) {
		$("#crawlerInformation").empty();$("#termInformation").empty();$("#activeTD").empty();$("#inActiveTD").empty();$("#crawledDoc").empty();
		$('#chartContainer1').empty();$('#chartContainer2').empty();
	});
	</script>
	
	<%-- receive terms from SenderPortletAction class and insert them to receiver portlet and database --%>
	<script>
	Liferay.on('getTermValue',function(event) {// length -1 because there is the term for database in last position
		numOfTerm = event.termValue.jsTerms.length-1;
		if ( (event.termValue.jsTerms[0].indexOf("Insert")>-1) || event.termValue.jsTerms[0] == "Delete" ) {//if there are change to the number of inserted terms
			$("#termInformation").empty();
			for (var i=1;i<numOfTerm;i++){
				$("#termInformation").append("<li><cl>" + event.termValue.jsTerms[i] + "</cl></li>"); //gather terms
			}
			if(event.termValue.jsTerms[0] == "Insert") { //case of insert a term
				insertTerm('http://localhost:8081/PrepareCrawlers/insert?queries=[{"query_term":"' + event.termValue.jsTerms[numOfTerm].split("#")[0].split(',')[1] + '", ' + '"language_id":2},'+
				'{"query_term":"' + event.termValue.jsTerms[numOfTerm].split("#")[0].split(',')[1] + '", ' + '"language_id":4}]');
			} else if(event.termValue.jsTerms[0] == "Insert account") { //case of insert a account
				insertTerm('http://localhost:8081/PrepareCrawlers/InsertSource?twitter_sources=[{"account_name":"'+ event.termValue.jsTerms[numOfTerm].split("#")[0].split(',')[1] + '", "active":1}]');				
			}
		}});
	</script>
	
	<%-- Insert terms in database using .ajax() calling a url...Jason type response--%>
	<script>
	function insertTerm(http_url) {
	    $.ajax({
	        type: "GET",
	        url: http_url,
	        error: function(request, status, error) {alert("The insertion of term is failed. Check the status of the external apache-server.")}, 	//error in server response
	        success: function (data) {$(data).each(function (index, value) {if(value.status=="OK"){alert("Term inserted successfully in database");}});}
	    });
	}</script>

	
	<%-- receive the name of selected crawler and the terms that contains, data are received from CrawlerPortletAction --%>
	<script>
	Liferay.on('getSelectCrawler',function(event){
		$("#crawlerInformation").empty();
		crawler = event.selectCrawler.jsCrawlerTerm[0];					// get the name of selected crawler
		$("#crawlerInformation").append(crawler);						// send the name of selected crawler
		cron_field.cron("value",event.selectCrawler.jsCrawlerTerm[7]);	// Updating the value of an existing cron object
		if (event.selectCrawler.jsCrawlerTerm[1] == "Not activate") {	// selected crawler is not activated
			$("#activeTD").empty();$("#inActiveTD").empty();$("#crawledDoc").empty();
			$('#chartContainer1').empty();$('#chartContainer2').empty();
		} else{// selected crawler is already activated
			$("#activeTD").empty();
			$("#activeTD").append(event.selectCrawler.jsCrawlerTerm[1]);
			$("#inActiveTD").empty();
			$("#inActiveTD").append(event.selectCrawler.jsCrawlerTerm[2]);
			$("#crawledDoc").empty();

			if (crawler.indexOf("_monitor")==-1){ // searching crawler
				$("#crawledDoc").append(event.selectCrawler.jsCrawlerTerm[3] + "</p><p>" + event.selectCrawler.jsCrawlerTerm[4] + "<p>" + event.selectCrawler.jsCrawlerTerm[5] + "<p>" + event.selectCrawler.jsCrawlerTerm[6]);
			}else{ // monitoring crawler
				$("#crawledDoc").append("<p>" + event.selectCrawler.jsCrawlerTerm[6]);}
			
			var documentDay = [];
			for (var i=0;i<event.selectCrawler.numDocFig.length;i++){// get the number of the returned document of crawler
				documentDay.push({
					"Day" : i, "Documents" : event.selectCrawler.numDocFig[i]
				});
			}
		}
		$("#termInformation").empty();
		for (var i=8;i<event.selectCrawler.jsCrawlerTerm.length;i++){// get the terms that crawler contains
			$("#termInformation").append("<li><cl>" + event.selectCrawler.jsCrawlerTerm[i] + "</cl></li>"); // gather terms
		}
		$('#chartContainer1').empty();
		if (documentDay.length>0)
			figure(documentDay, event.selectCrawler.numDocFigXX, crawler, '1', Math.max.apply(null, event.selectCrawler.numDocFig), event.selectCrawler.numDocFig.length);

		$('#chartContainer2').empty();
	});
	</script>


	<%-- function that estimate the date and time of the crawler activation and active available crawlers --%>
	<script type="text/javascript">
	function activeCrawler(){
		if(crawler != '') {
			if (confirm('You are about to execute the crawler. In order to complete this process you will have to keep the page open without refreshing it. If you want to continue please click "OK", otherwise click "Cancel"')) {
				var d = new Date();
				var month = (d.getMonth()+1);
				var minutes = d.getMinutes();
				if (month<10){month = '0' + month;}
				if (minutes<10){minutes = '0' + minutes;}
				$("#inActiveTD").empty();
				$("#crawledDoc").empty();
				var activeElemnt = [];
				activeElemnt.push(crawler);
				activeElemnt.push('Active');
				activeElemnt.push('Date:' + d.getFullYear() + '-' + month + '-' + d.getDate() + '  Time:' + d.getHours() + ':' + minutes);
				activeElemnt.push(cron_field.cron("value"));
				returnCrawlerCond(activeElemnt); // return the list of terms for the specific crawler and update active/crontab time and date
			}
		} else
		    alert("You have to select a crawler");
		
	}</script>
	
	<script type="text/javascript">
	function defineSchedule(){
		if(crawler != '' && cron_field.cron("value")!='* * * * *'){
			var activeElemnt = [];
			activeElemnt.push(crawler);
			activeElemnt.push('Active');
			activeElemnt.push('Not available yet');
			activeElemnt.push(cron_field.cron("value"));
			returnCrawlerCond(activeElemnt);//return the list of terms for the specific crawler and update active/crontab time and date		
		}else if(crawler== '')
			alert("You have to select a crawler");
		else if(cron_field.cron("value")=='* * * * *')//case without crontab defenition
			alert("It is not available to schedule jobs for every minute");
	}
	</script>
	
	<%-- send items about activation to ReceiverPortletAction class --%>
	<script type="text/javascript">
	function returnCrawlerCond(activeElemnt){
		var myJsonString = JSON.stringify(activeElemnt);
		$.ajax({
			url:'<%=getActivationOpt%>',
			dataType: "json",
			data:{<portlet:namespace/>myJsonString:myJsonString},
			type: "get",
			success: function(data){Liferay.fire('getActivationOpt', {myJsonString:data});},
			beforeSend: function(){},	//before send this method will be called
			complete: function(){}		//after completed request then this method will be called.
		});
	}</script>
	
	
	<%-- function that estimate the date and time of the crawler inactivation --%>
	<script type="text/javascript">
	function inActiveCrawler(){
		var activeElemnt = [];
		activeElemnt.push(crawler);
		activeElemnt.push('Inactive');
		activeElemnt.push('Unk');
		activeElemnt.push('No crontab');
		returnCrawlerCond(activeElemnt); // Upadate that crawler is Inactivated
	}
	</script>
	
	
	<%-- receive date and time from ReciverPortletAction class --%>
	<script>
	Liferay.on('getActivationOpt',function(event) {
		if (event.myJsonString.crawlerCond[1] == "Active"){
			$("#activeTD").empty();
			$("#activeTD").append(event.myJsonString.crawlerCond[2]);
			var stringOfCrawlerTerm = "";								// list with the crawler's term which is being active
			for (var j=4;j<event.myJsonString.crawlerCond.length;j++){	// get the terms that crawler contains
				stringOfCrawlerTerm += '"' + event.myJsonString.crawlerCond[j] + '",';
			}
			if(event.myJsonString.crawlerCond[2] != 'Not available yet'){
				stringOfCrawlerTerm = stringOfCrawlerTerm.substring(0, stringOfCrawlerTerm.length-1);
				if (event.myJsonString.crawlerCond[0].indexOf("_monitor")==-1) {
					crawling('http://localhost:8081/PrepareCrawlers/Crawl?bing=true&gplus=true&youtube=false&twitter=true'+
							'&query_list=['+ stringOfCrawlerTerm +']&crawl_name='+event.myJsonString.crawlerCond[0]);
				} else if (event.myJsonString.crawlerCond[0].indexOf("_monitor")>-1)
					crawling('http://localhost:8081/PrepareCrawlers/Monitor?twitter=true&twitter_sources=['+ stringOfCrawlerTerm +']&monitor_name='+event.myJsonString.crawlerCond[0]);
			}
		
		} else if (event.myJsonString.crawlerCond[1] == "Inactive") {
			$("#inActiveTD").empty();
			$("#inActiveTD").append(event.myJsonString.crawlerCond[2]);
			$("#crawledDoc").empty();

			if (event.myJsonString.crawlerCond[0].indexOf("_monitor")==-1){ // searching crawler
				$("#crawledDoc").append(event.myJsonString.crawlerCond[4] + "</p><p>" + event.myJsonString.crawlerCond[5] + "<p>" + event.myJsonString.crawlerCond[6] + "<p>" + event.myJsonString.crawlerCond[7]);
			} else { // monitoring crawler
				$("#crawledDoc").append("<p>" + event.myJsonString.crawlerCond[7]);}
			
			var documentDay = [];
			for (var i=0;i<event.myJsonString.numDoc.length;i++){// get the number of the returned document of crawler
				documentDay.push({
					"Day" : i, "Documents" : event.myJsonString.numDoc[i]
				});
			}
			figure(documentDay, event.myJsonString.numDocXX, event.myJsonString.crawlerCond[0], '1', Math.max.apply(null, event.myJsonString.numDoc), event.myJsonString.numDoc.length);
			$('#chartContainer2').empty();
		
		} else if (event.myJsonString.crawlerCond[1] == "figureWord") {
			var documentDay = [];
			for (var i=0;i<event.myJsonString.numDoc.length;i++){// get the number of the returned document of crawler
				documentDay.push({
					"Day" : i, "Documents" : event.myJsonString.numDoc[i]
				});
			}
			figure(documentDay, event.myJsonString.numDocXX, event.myJsonString.crawlerCond[0], '2', Math.max.apply(null, event.myJsonString.numDoc), event.myJsonString.numDoc.length);
		}
	});</script>
	
	<%-- Active crawling using .ajax() calling a url...Jsonp type response--%>
	<script>
	function crawling(http_url, method) {
	    $.ajax({
	        type: "GET",
	        url: http_url,
	        beforeSend:  function(){
	        	window.onbeforeunload = warning;
	        },
	        error: function(request, status, error) {alert("Crawling failed. Check the status of the external apache-server.")}, 	//error in server response
	        success: function(data){
	        	window.onbeforeunload = null;
	        	alert("Crawling is done!")
	        	inActiveCrawler();
	        }
	    });
	}</script>

	
	<%-- Create figure --%>
	<script type="text/javascript">
    function figure(documentDay, docXX, input, num, maxYY, maxXX){
    	if (num=='1'){
    		titleOfFig = "Documents per day for crawler: " + input
    		descr = 'Crawled-Documents';
    		unitInter = 20;
    		maxV = maxYY+10;
    		schema = 'scheme02';
    	}else if (num=='2'){
    		titleOfFig = "Word-occurrences per day for word: " + input
    		descr = 'Number-of-occurrences';
    		unitInter = 200;
    		maxV = maxYY+10;
    		schema = 'scheme05';
    	}
        $(document).ready(function () {  // prepare jqxChart settings
            var settings = {
                title: titleOfFig,
                description: "",
                enableAnimations: true,
                showLegend: true,
                padding: { left: 5, top: 5, right: 10, bottom: 5 },
                titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
                source: documentDay,
                xAxis:
                    {
                        textRotationAngle: 0,
                        dataField: 'Day',
                        formatFunction: function (value) {
                            return docXX[value];
                        },
                        showTickMarks: true,
                        tickMarksStartInterval: 0,
                        tickMarksInterval: 1,
                        tickMarksColor: '#888888',
                        unitInterval: 1,
                        showGridLines: true,
                        gridLinesStartInterval: 0,
                        gridLinesInterval: 3,
                        gridLinesColor: '#888888',
                        axisSize: 'auto',
                        minValue: 0,
                        maxValue: (maxXX-1),
                        valuesOnTicks: false
                    },
                colorScheme: schema,
                seriesGroups:
                    [
                        {
                            type: 'column',
                            valueAxis:
                            {
                                unitInterval: unitInter,
                                minValue: 0,
                                maxValue: maxV,
                                displayValueAxis: true,
                                description: descr,
                                axisSize: 'auto',
                                tickMarksColor: '#888888'
                            },
                            series: [
                                    { dataField: 'Documents', displayText: 'Documents', showLabels: true }
                                ]
                        }
                    ]
            };
            $('#chartContainer'+num).jqxChart(settings);	// setup the chart
        });
    }</script>
    
    <script>
    function wordOccur(word){
    	if (crawler != ""){
	    	myJsonString = "stemmedWord:" + word.Textword.value.trim() + ":" + crawler
	     	$.ajax({
	     		url:'<%=getActivationOpt%>',
	 			dataType: "json",
	 			data:{<portlet:namespace/>myJsonString:myJsonString},
	 			type: "get",
	 			success: function(data){Liferay.fire('getActivationOpt', {myJsonString:data});},
	 			beforeSend: function(){},	//before send this method will be called
	 			complete: function(){}		//after completed request then this method will be called.
	 		});
    	}
    	return false;
    }
    </script>

</head>
<body>
	<div id="header" style="background-color:#687AA3;letter-spacing: 5px;">
	<h1 style="text-align:center;margin-bottom:0;"><u id="crawlerInformation"></u></h1></div>

	<div id="header" style="background-color:LightSlateGray;letter-spacing:5px; opacity: 0.8;">
	<h1 style="text-align:center;margin-bottom:0;">Crawl Parameters</h1></div>
	<ul><b id="termInformation" class="context-menu-one box menu-1"></b></ul>
	
	<div id="header" style="background-color:LightSlateGray;letter-spacing:5px; opacity: 0.8;">
	<h1 style="text-align:center;margin-bottom:0;">Statistics</h1></div>
	
	<b>Active Since:</b><p id="activeTD"></p>
	<b>Inactive Since:</b><p id="inActiveTD"></p>
	<b>Latest Crawled Documents: </b><p id="crawledDoc"></p><br>

	<a href="#" onClick="activeCrawler()" class="button button-3d-primary button-rounded" title="Click to execute the selected crawler only once. You have to keep the page open until the crawler is finished"><i class="fa fa-refresh"></i>Execute Once</a>
	<a href="#" onClick="defineSchedule()" class="button button-3d-action button-pill" title="Click to set an execution schedule for the selected crawler">Set Execution Schedule</a>

	<br><br>Define the time that will be activated a crawler every day:<br><div id="gentle" style="margin-bottom:20px;"></div><br><br>
	
	<form action="" method="GET" onsubmit="return wordOccur(this);" style="float:left; margin-right:30px; margin-bottom:30px;">
	<b>Word:</b>
		<input type="text" name="Textword" value="" placeholder="Type a word here" style="font-family:Times New Roman,Times,serif; font-style: italic;"/>
		<input type="submit" value="Search" title="Click to show the figure with the word's occurrences">
	</form>
	
	<div id="chartContainer1" style="width:850px; height:500px; margin-bottom:100px;"></div>
	<div id="chartContainer2" style="width:850px; height:500px;"></div>
	
</body>
</html>