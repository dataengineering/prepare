<%--
 @create on:	     1/04/2014
 @author:			 Konstantinos Pechlivanis
 @research program:  PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 @project:			 Analytic Platform (Work package 2)
  		-Task 2.1:   Development of Scientific means 
  		-Task 2.3:   Technical Platform
  		
 @document: 		 view.jsp from file /html/jsps/manage_crawlers
--%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.File" %>

<portlet:defineObjects />
<portlet:resourceURL var="getSelectCrawler"></portlet:resourceURL>
<portlet:resourceURL var="rightSelectCrawler"></portlet:resourceURL>
<portlet:resourceURL var="getTermAccount"></portlet:resourceURL>

<!DOCTYPE html>
<html>
<head>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<script type='text/javascript' src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
    <script type='text/javascript' src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type='text/javascript' src='http://code.jquery.com/jquery.min.js'></script>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/buttons.css">
 	<script type="text/javascript" src="<%=request.getContextPath()%>/js/buttons.js"></script>
 	<%@ page import="javax.portlet.PortletPreferences" %>
 	
 	<script>var crawlers = []; var crawlersTwitter = []; var RigSelCrawler; var selCrawler='';</script>

	<%-- catch the event of selection a crawler, send the name of selected crawler to CrawlersPortletAction class --%>
	<script type="text/javascript">
	$(function(){
		$("#newCrawler, #newCrawlerTw").delegate("li","mousedown",function(e){
			var res = $(this).text().split(")");	// split the list of crawlers
			selCrawler = res[res.length-1];
			if( e.button == 0 ){// left mouse button in crawler
				var selectCrawler = selCrawler + ",left";	// catch the selected crawler
				$.ajax({
					url:'<%=getSelectCrawler%>',
					dataType: "json",
					data:{<portlet:namespace/>termCrawl:selectCrawler},
					type: "get",
					success: function(data){ Liferay.fire('getSelectCrawler', {selectCrawler:data}); },
					beforeSend: function(){},	// before send this method will be called
					complete: function(){}		// after completed request then this method will be called
				});
			
			} else if( e.button == 2 ){// right mouse button in crawler
				RigSelCrawler = "Remove:" + selCrawler;	// catch the selected crawler
				$(function(){
				    $.contextMenu({
				        selector: '.context-menu-two', 
				        callback: function(key, options) {
				            var m = "clicked: " + key;
				            window.console && console.log(m);
				            if (key=='quit'){// case termination
				            	alert('Not available option yet');
				            }else if (key=='delete'){// selection of delation
								$.ajax({
									url:'<%=rightSelectCrawler%>',
									dataType: "json",
									data:{<portlet:namespace/>termCrawl:RigSelCrawler},
									type: "get",
									success: function(data){ Liferay.fire('rightSelectCrawler', {RigSelCrawler:data}); },
									beforeSend: function(){},// before send this method will be called
									complete: function(){}	// after completed request then this method will be called
								});
				            }
				        },
				        items: {
				            "delete": {name: "Delete Crawler", icon: "delete"},
				            "sep1": "---------",
				            "quit": {name: "Quit", icon: "quit"}
				        }
				    });
				    $('.context-menu-two').on('click', function(e){
				        console.log('clicked', this);
				    })
				});
			}
		});
	});
	</script>
	
	
	<%-- receive from sender portlet the crawler that select to remove --%>
	<script>
	Liferay.on('removeCrawler',function(event) {
		removeCrawler(event.termValue.RemoveCr); 	// call function to remove crawler
	});
	</script>
	
	
	<%-- if crawler does not exist, add crawler in the list of available crawlers --%>
	<script type="text/javascript">
	function addExistCrawler(c){
    	if (crawlers.indexOf(c) == -1){// if crawler does not already exist
    		crawlers.push(c);
    		
    		$.each(crawlers, function(i,v) {// dynamic list
           	if (i==(crawlers.length-1)){	// last inserted crawler
           		$("#newCrawler").append("<li><a href='#'>" + (i+1) + ")" + v + "</a></li>");
           	}});
    	}
    	return false;
    }
	
	<%-- if crawler does not exist, add crawler in the list of available crawlers --%>
	function addExistCrawlerTwitter(c){
    	if (crawlersTwitter.indexOf(c) == -1){// if crawler does not already exist
    		crawlersTwitter.push(c);
    		
    		$.each(crawlersTwitter, function(i,v) {// dynamic list
           	if (i==(crawlersTwitter.length-1)){	// last inserted crawler
           		$("#newCrawlerTw").append("<li><a href='#'>" + (i+1) + ")" + v + "</a></li>");
           	}});
    	}
    	return false;
    }
	
	function removeCrawler(cr){
		if (cr.indexOf("_monitor")>-1) { // crawler monitor
			if (crawlersTwitter.indexOf(cr.replace('_monitor','')) != -1){// if crawler already exist
				crawlersTwitter.splice(crawlersTwitter.indexOf(cr),1);
	    		$("#newCrawlerTw").empty(); // empty html data
	    		$.each(crawlersTwitter, function(i,v) {// dynamic list
	           		$("#newCrawlerTw").append("<li><a href='#'>" + (i+1) + ")" + v + "</a></li>"); // display a list with the available crawlers
	           	});
	    	}

		} else { //crawler with searching API
	    	if (crawlers.indexOf(cr) != -1){// if crawler already exist
	    		crawlers.splice(crawlers.indexOf(cr),1);
	    		$("#newCrawler").empty(); // empty html data
	    		$.each(crawlers, function(i,v) {// dynamic list
	           		$("#newCrawler").append("<li><a href='#'>" + (i+1) + ")" + v + "</a></li>"); // display a list with the available crawlers
	           	});
	    	}
		}
    	return false;
    }
	
    function addCrawler(x){
    	if (x.crawler.value.trim().indexOf("_monitor")>-1){
			alert("Name of crawler can not contain the string '_monitor'");
			return false;
		}
    	if (crawlers.indexOf(x.crawler.value.trim()) == -1 ){// if crawler does not already exist
    		crawlers.push(x.crawler.value.trim());
    	
   			$.each(crawlers, function(i,v) {// dynamic list
       		if (i==(crawlers.length-1)){	// last inserted crawler
       			$("#newCrawler").append("<li><a href='#'>" + (i+1) + ")" + v + "</a></li>");
       		}});
    	}
    	return false;
    }
    
    function addCrawlerTwitter(x){
    	if (x.crawlerTw.value.trim().indexOf("_monitor")>-1){
			alert("Name of crawler can not contain the string '_monitor'");
			return false;
		}
    	if (crawlersTwitter.indexOf(x.crawlerTw.value.trim()) == -1 ){// if crawler does not already exist
    		crawlersTwitter.push(x.crawlerTw.value.trim());
    	
   			$.each(crawlersTwitter, function(i,v) {// dynamic list
       		if (i==(crawlersTwitter.length-1)){	// last inserted crawler
       			$("#newCrawlerTw").append("<li><a href='#'>" + (i+1) + ")" + v + "_monitor" + "</a></li>");
       		}});
    	}
    	return false;
    }
    </script>
    
    <script> <%-- function for case, user wants type term for inserting --%>
	function typeTerm(input, typeOfInput){
		if (selCrawler != ''){// store input if a crawler have selected

			if(selCrawler.indexOf("_monitor")==-1 && typeOfInput=="Account"){ // wrong use of form
				alert("Wrong insertion! In this form you can only insert twitter account");	
			} else if (selCrawler.indexOf("_monitor")>-1 && typeOfInput=="Term") { // wrong use of form
				alert("Wrong insertion! In this form you can only insert term, manually");
			} else if (input.inputText.value.trim().indexOf(' ')>-1) {
				alert("Wrong insertion! Twitter accounts can not contain white space");
			}
			else {
				if (input.inputText.value.trim() != ''){// use this case to prevent empty request
					var termAcc = 'Term:2' + input.inputText.value.trim() + "," + input.inputText.value.trim() + "--" + selCrawler;// insert term
		 			$.ajax({
	 	 				url:'<%=getTermAccount%>',
						dataType: "json",
						data:{<portlet:namespace/>termCrawl:termAcc},
						type: "get",
						success: function(data){
						Liferay.fire('getTermAccount', {termAcc:data});
						},
						beforeSend: function(){},	// before send this method will be called
						complete: function(){}		// after completed request then this method will be called.
					});
				}
			}

		}else
			alert('You have to select a crawler!')
		return false;
	}</script>

</head>

<body>
	
	<form action="" method="GET" onsubmit="return addCrawler(this);" style="float:left; margin-right:1.5cm;">
	<b>Crawler - Searching:</b><br>
		<input type="text" name="crawler" value="" placeholder="Type a crawler here"/>
		<input type="submit" value="Create" title="Click to create a new crawler">
	</form>
	
	<span class="button-dropdown" data-buttons="dropdown" style="float:left;">
	    <a href="#" class="button button-rounded button-flat-primary" > Searchers <i class="fa fa-caret-down"></i></a>
	    <ul class="button-dropdown-menu-below">
	    	<b id="newCrawler" class="context-menu-two box menu-1"></b>
	    </ul>
	</span>
	
	<%-- take the list of available crawler --%>
	<script>
	<%final File folder = new File("/home/"+System.getProperty("user.name") + "/.PrepareData");
	for (final File fileEntry : folder.listFiles()) {
        if (!fileEntry.isDirectory()) {
            if (fileEntry.getName().contains("crawler_") & !fileEntry.getName().equals("crawler_crontab.txt") & !fileEntry.getName().contains("_monitor")){%>
            	addExistCrawler('<%=fileEntry.getName().replaceFirst("crawler_","").replace(".txt","")%>');<%
            }
        }
	}%>
	</script><br><br><br>
	
    <!-- HTML for INSERT BAR -->
    <form action="" method="GET" onsubmit="return typeTerm(this,'Term');" style="margin-bottom:20px;">
	<b>Type a term:</b><br>
		<input type="text" name="inputText" value="" placeholder="Type a term here"/>
		<input type="submit" value="Insert" title="Click to insert a term into selected crawler">
	</form><br><br><br><br>

<!----------------------------------------------------------------------------------------------------------------------------------------------------------------->
	
	<form action="" method="GET" onsubmit="return addCrawlerTwitter(this);" style="float:left; margin-right:1.5cm;">
	<b>Crawler - Monitoring:</b><br>
		<input type="text" name="crawlerTw" value="" placeholder="Type a crawler here"/>
		<input type="submit" value="Create" title="Click to create a new crawler">
	</form>
	
	
	<span class="button-dropdown" data-buttons="dropdown" style="float:left;">
	    <a href="#" class="button button-rounded button-flat-primary" > Monitors <i class="fa fa-caret-down"></i></a>
	    <ul class="button-dropdown-menu-below">
	    	<b id="newCrawlerTw" class="context-menu-two box menu-1"></b>
	    </ul>
	</span>
	
	<%-- take the list of available crawler --%>
	<script>
	<%for (final File fileEntry : folder.listFiles()) {
        if (!fileEntry.isDirectory()) {
            if (fileEntry.getName().contains("_monitor") & !fileEntry.getName().equals("crawler_crontab.txt")){%>
            	addExistCrawlerTwitter('<%=fileEntry.getName().replaceFirst("crawler_","").replace(".txt","")%>');<%
            }
        }
	}%>
	</script>

	<!-- HTML for INSERT BAR --><br><br><br>
    <form action="" method="GET" onsubmit="return typeTerm(this, 'Account');" style="margin-bottom:20px;">
	<b>Type a twitter account:</b><br>
		<input type="text" name="inputText" value="" placeholder="Type a account here"/>
		<input type="submit" value="Insert" title="Click to insert account into selected crawler">
	</form>
	
</body>

</html>