/*
 @create on	:	    1/04/2014
 @author	:		Konstantinos Pechlivanis
 @research program: PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 @project	:		Analytic Platform (Work package 2)
  		-Task 2.1:  Development of Scientific means 
  		-Task 2.3:  Technical Platform
 
 @document	: 		ReceiverPortletAction.java from file /WEB-INF/src/com/clientside/ipc/receiver/ReceiverPortletAction.java

 @summary	:		this class handles the state of crawler (activate, inactivate, date, time), estimate diagrams about number of crawled documents and number
 					of word occurrences, execute crontab and send these information to receiver portlet
*/

package com.clientside.ipc.receiver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.util.bridges.mvc.MVCPortlet;

import porterStemmer.Stemmer;

import java.sql.*;
import java.util.Properties;

public class ReceiverPortletAction extends MVCPortlet {
	Hashtable<String, String> crawlerCronTab = new Hashtable<String, String>();//hash table with, key: name of Crawler, value:crontab
	Hashtable<String, ArrayList<String> > crawlerTermHash = new Hashtable<String, ArrayList<String> >();
	Logger myLog = Logger.getLogger(getClass().getName());
	
	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String [] activationData = resourceRequest.getParameterValues("myJsonString");
		String crawler=null, state=null, date=null, date_crontab=null, inputWord=null, nameCrawler=null;
		
		if (activationData[0].contains("stemmedWord:")) {// case find stemmed text term occurrences per day
			inputWord = activationData[0].split(":")[1];
			nameCrawler = activationData[0].split(":")[2];
			crawler = inputWord;	// in this case the crawler variable has stored the name of stemmed text term 
			state = "figureWord";
		} else {
			activationData[0] = activationData[0].replace("\"", "");
			crawler =  activationData[0].split(",")[0].replace("[", "").trim();		// get the name of crawler
			state =  activationData[0].split(",")[1].trim();						// get the state of crawler: active/inactive
			date = activationData[0].split(",")[2].trim();							// get the date of status
			date_crontab = activationData[0].split(",")[3].replace("]", "").trim();	// get the date of crontab
		}
//----------------------------------------load data----------------------------------------------------------------------------------//
		final File loadFolder = new File("/home/"+System.getProperty("user.name") + "/.PrepareData");
		for (final File fileEntry_cr : loadFolder.listFiles()) {// load all crawlers
			if (!fileEntry_cr.isDirectory()) {// for each file
				if (fileEntry_cr.getName().equals("crawler_crontab.txt")){
					BufferedReader bf = new BufferedReader(new FileReader(fileEntry_cr.getAbsoluteFile()));	// create a buffer for a file
					String tempTerm;
	          		while((tempTerm = bf.readLine()) != null )		// while there are terms in configuration file
	          			crawlerCronTab.put(tempTerm.split("-->")[0].trim(), tempTerm.split("-->")[1].trim());
	          		bf.close();
				 }
			}
		}
//----------------------------------------load data----------------------------------------------------------------------------------//
		for (final File fileEntry : loadFolder.listFiles()) {
	        if (!fileEntry.isDirectory()) {
	            if (fileEntry.getName().contains("crawler_") && !fileEntry.getName().equals("crawler_crontab.txt")){
	            	String crawlerName = fileEntry.getName().replaceFirst("crawler_","").replace(".txt","");
	            	if (!crawlerTermHash.containsKey(crawlerName)){ // add new entry in hash table if crawler does not exist
	    				crawlerTermHash.put(crawlerName, new ArrayList<String>());
	    				BufferedReader bf = new BufferedReader(new FileReader(fileEntry.getAbsoluteFile()));	// create a buffer for a file
		          		String tempTerm;
		          		while((tempTerm = bf.readLine()) != null ) {							// while there are terms in configuration file
		          			tempTerm = tempTerm.split("#")[0];
		          			crawlerTermHash.get(crawlerName).add(tempTerm.split(",")[0]);		// add a term in the hash table, English
		          			if (!tempTerm.split(",")[0].equals(tempTerm.split(",")[1]))
		          				crawlerTermHash.get(crawlerName).add(tempTerm.split(",")[1]);	// add a term in the hash table, French
		          		}
		          		bf.close();
	    			}
	            }
	        }
		}
//--------------------------------------------------------------------------------------------------------------------------------//
		PrintWriter pw = resourceResponse.getWriter();
		JSONObject juser = JSONFactoryUtil.createJSONObject();
		JSONArray array = JSONFactoryUtil.createJSONArray();
		JSONArray arrayFig = JSONFactoryUtil.createJSONArray();
		JSONArray arrayFigXX = JSONFactoryUtil.createJSONArray();
		array.put(crawler);
		array.put(state);
		if (state.equals("Active")){ // call ajax for activation of crawler
			try {
				// take the list of terms for the specific crawler
				array.put(date);
				array.put(date_crontab);
				
				File fileCrawler = new File("/home/"+ System.getProperty("user.name") + "/.PrepareData"+"/crawler_" + crawler + ".txt");
				if (fileCrawler.exists()) {// if file exists
					if (!crawlerTermHash.containsKey(crawler))//add new entry in hash table if crawler does not exist
	    				crawlerTermHash.put(crawler, new ArrayList<String>());

					BufferedReader bf = new BufferedReader(new FileReader(fileCrawler.getAbsoluteFile()));	// create a buffer for a file
					String term;
					while ((term = bf.readLine()) != null ){// while there are terms in configuration file
						term = term.split("#")[0];			// catch only terms
						array.put(term.split(",")[0]);		// insert English term
						if(!term.split(",")[1].equals(term.split(",")[0]))	//not insert same term
							array.put(term.split(",")[1]);	// insert French term
			  		}
					bf.close();
				}

				crawlerCronTab.put(crawler, date_crontab); //update cralwer's crontab
				File fileCron = new File("/home/"+System.getProperty("user.name") + "/.PrepareData/crawler_crontab.txt");
				File fileCronUnix = new File("/home/"+System.getProperty("user.name") + "/.PrepareData/crontab_unix.txt");
				if (!fileCron.exists()) // if file not exist
					fileCron.createNewFile();
				if (!fileCronUnix.exists())// if file not exist
					fileCron.createNewFile();

				FileWriter fw = new FileWriter(fileCron.getAbsoluteFile());
				FileWriter fwU = new FileWriter(fileCronUnix.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				BufferedWriter bwU = new BufferedWriter(fwU);
				Enumeration<String> keys = crawlerCronTab.keys();	// take the keys of hash table
				while(keys.hasMoreElements()){						// for every key
					String key = keys.nextElement();
					bw.write(key + "-->" + crawlerCronTab.get(key) + "\n");
					if(!crawlerCronTab.get(key).equals("* * * * *")){
						JSONArray arrayUnix = JSONFactoryUtil.createJSONArray();
						for(int i=0; i<crawlerTermHash.get(key).size(); i++){
							arrayUnix.put(crawlerTermHash.get(key).get(i));
						}
						if (!key.contains("_monitor")) {
							bwU.write(crawlerCronTab.get(key) + " wget --timeout=0 -O - 'http://localhost:8081/PrepareCrawlers/Crawl?bing=true&gplus=true&youtube=false&twitter=true&query_list="+ arrayUnix +"&crawl_name=" + key + "' >/dev/null 2>&1");
						} else if (key.contains("_monitor")) {
							bwU.write(crawlerCronTab.get(key) + " wget --timeout=0 -O - 'http://localhost:8081/PrepareCrawlers/Monitor?twitter=true&twitter_sources="+ arrayUnix + "&monitor_name=" + key + "' >/dev/null 2>&1");}
						bwU.write("\n");
					}
				}
				bw.close();bwU.close();
				Runtime.getRuntime().exec("crontab -u " + System.getProperty("user.name") + " /home/"+System.getProperty("user.name")+"/.PrepareData/crontab_unix.txt");
			} catch (IOException e) {e.printStackTrace();}

		//---------------------------------------------------------------------------------------------------------------------------------------------//
		} else if ( (state.equals("Inactive")) || (state.equals("figureWord")) ){ // call ajax for inactivation of crawler
		    Properties properties = new Properties();	// set properties of database
		    properties.put("user","searchuser");
		    properties.put("password","searchUser_@!");
		    properties.put("characterEncoding", "UTF-8");
			try {
				try {
				    Class.forName("com.mysql.jdbc.Driver");	//loads the jdbc classes and creates a driver manager class factory
				    
				} catch (ClassNotFoundException e) {
					myLog.info("Where is your MySQL JDBC Driver?");
					e.printStackTrace();
					return;
				}
				Connection conn = null;
				try {
					conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/prepare_crawler_db",properties);
				} catch (SQLException e) {
					myLog.info("Connection Failed! Check output console");
					e.printStackTrace();
					return;
				}
				if (conn == null)
					myLog.info("Failed to make connection!");

				//--------------------------Get data from database to display them in page-----------------------------------------------------------------------------------------------
				Statement st = conn.createStatement(), st1 = conn.createStatement();
				String endDate="", last_Crawl_id="", content="";;

				//--------------------------A CRAWLER IS FINISHED-------------------------------------------------//
				if (state.equals("Inactive")){
					int bing=0, gplus=0, twitter=0, num_docs = 0, total_num_docs=0;
					ResultSet rs= null, rs1= null;

					if (!crawler.contains("_monitor")) { // case for full crawling (bing, gplus, twitter)
					    
						rs = st.executeQuery("SELECT date(end_date), end_date FROM crawl_log where crawl_name = '" + crawler + "' group by date(end_date) order by start_date asc");// get the different execution days of crawler
					    while(rs.next()){
					    	endDate = rs.getString(2);		// the timestamp of crawler end
					    	
					    	num_docs=0;total_num_docs=0;	// crawled data for a specific day
					    	rs1 = st1.executeQuery("SELECT crawl_id, num_docs, previous_docs_sum FROM crawl_log where crawl_name = '" + crawler + "' AND date(end_date) = '" + rs.getDate(1) + "'");// get crawl_id, num_docs and previous_docs_sum by day
					    	while(rs1.next()){
					    		last_Crawl_id = rs1.getString(1);
					    		num_docs = rs1.getInt(2);
					    		total_num_docs += (rs1.getInt(2) + rs1.getInt(3));
					    	}
					    	arrayFig.put(total_num_docs);
					    	arrayFigXX.put(rs.getString(1));
					    }st1.close();

					    rs = st.executeQuery("select crawler_name, num_docs from crawl_query_lkp where crawl_id='" + last_Crawl_id + "'");// get from crawler_name, num_docs the last crawling for each NoMad crawler
					    while(rs.next()){
					    	if (rs.getString(1).equals("BingCrawler"))
					    		bing += rs.getInt(2);
					    	else if (rs.getString(1).equals("GplusCrawler"))
					    		gplus += rs.getInt(2);
					    	else if (rs.getString(1).equals("TwitterCrawler"))
					    		twitter += rs.getInt(2);
					    }
					    
					    if (endDate!="" && endDate!=null){
					    	array.put("Date: " + endDate.split(" ")[0] + " Time:" + endDate.split(" ")[1]);
					    }else
					    	array.put("Not available yet");
						array.put("* * * * *");
						array.put("Bing: " + bing);
						array.put("Google Plus: " + gplus);
						array.put("Twitter: " + twitter);
						array.put("Total documents: " + num_docs);
						
						// get clean_context of crawled documents
						rs = st.executeQuery("select clean_text from content where crawl_id='" + last_Crawl_id + "'");
						while(rs.next()){
							content = content + " " + rs.getString(1);
					    }
					
					} else if (crawler.contains("_monitor")) {
						Hashtable<String, Integer > figureOccurHash = new Hashtable<String, Integer >();

						rs = st.executeQuery("SELECT end_date, num_docs, monitor_id FROM monitor_log WHERE monitor_name='" + crawler + "' ORDER BY end_date asc");
						while(rs.next()){
							endDate = rs.getString(1);
							num_docs = rs.getInt(2);
							last_Crawl_id = rs.getString(3);

					    	rs1 = st1.executeQuery("SELECT count(*), date(created_at) FROM twitter_post "
						    			+ "WHERE engine_id='" + last_Crawl_id + "' and engine_type_id='2' group by date(created_at) order by date(created_at) asc");
					    	while(rs1.next()){
					    		if (!figureOccurHash.containsKey(rs1.getString(2)))
					    			figureOccurHash.put(rs1.getString(2), 0);
					    		figureOccurHash.put(rs1.getString(2), figureOccurHash.get(rs1.getString(2)) + rs1.getInt(1));
					    	}
						}
						
				    	// get clean_context of crawled documents
						rs1 = st1.executeQuery("select text from twitter_post where engine_id='" + last_Crawl_id + "' and engine_type_id='2'");
						while(rs1.next()){
							content = content + " " + rs1.getString(1);
						}
					  		
				  		ArrayList<String> dates = new ArrayList<String>(figureOccurHash.keySet());
				  	    Collections.sort(dates);	// sort hash-table according to the date of twitter post

				  	    for (String str : dates) {
				  			arrayFig.put(figureOccurHash.get(str));
				  			arrayFigXX.put(str);
				  	    }

						if (endDate!="" && endDate!=null) {
					    	array.put("Date: " + endDate.split(" ")[0] + " Time:" + endDate.split(" ")[1]);
						} else
					    	array.put("Not available yet");
						array.put("* * * * *");
						array.put("Not available");array.put("Not available");array.put("Not available");
						array.put("Total monitored documents: " + num_docs);
						st1.close();
					}
					
					//--------------------------INDEX CRAWLED DATA-------------------------------------------------//
					Stemmer myStemmer = new Stemmer();
					StandardAnalyzer analyzer = new StandardAnalyzer();	// Specify the analyzer for tokenizing text.
					Directory index = FSDirectory.open(new File("/home/" + System.getProperty("user.name") + "/.PrepareData/index_data_"+crawler).toPath()); // store data to the specific folder
					IndexWriterConfig config = new IndexWriterConfig(analyzer);
					IndexWriter w = new IndexWriter(index, config);

					String [] contentTokens = content.replaceAll("[^\\dA-Za-z ]", " ").toLowerCase().split(" ");	// convert text to lower-case and remove "non-letter"
					for (int j=0;j<contentTokens.length;j++){	// applying the stemmer to each word of clean text
						char [] contentArray = contentTokens[j].toCharArray();
						if (contentArray.length==0)// if there is not text
							continue;
						myStemmer.add(contentArray, contentArray.length);// add word in "collation" of chars
						myStemmer.stem();// execute stemming
						addDoc(w, myStemmer.toString().trim(), endDate.split(" ")[0], crawler, last_Crawl_id);	// add a new doc with 3 fields
					}w.commit();w.close();st.close();conn.close();


				//--------------------------FIND THE NUMBER OF OCCURRENCE FOR GIVEN WORD-------------------------------------------------//
				} else if (state.equals("figureWord")){
					StandardAnalyzer analyzer = new StandardAnalyzer();	// Specify the analyzer for tokenizing text.
					Directory index = FSDirectory.open(new File("/home/"+System.getProperty("user.name") + "/.PrepareData/index_data_" + nameCrawler).toPath()); // store data to the specific folder
					
					Stemmer myStemmer = new Stemmer();
					String [] inputWordArray = inputWord.split(" "); // split input to tokens
					inputWord = "";
					Query [] myQueries = new Query[inputWordArray.length];
					for (int i=0;i<inputWordArray.length;i++){
						char [] arrayWord = inputWordArray[i].trim().toLowerCase().toCharArray();
						myStemmer.add(arrayWord, arrayWord.length);
						myStemmer.stem();	// stemming of word
						inputWord = myStemmer.toString();	// return stemmed word
						String querystr = "\""+ inputWord +"\""; 		// text to search
						Query q = new QueryParser("\"text\"", analyzer).parse(querystr); // set the query for stemmed text term
						myQueries[i] = q;
					}

					String queryCrawler = "\""+ nameCrawler +"\""; 	// for specific crawler
					Query q_crawler = new QueryParser("\"crawler\"", analyzer).parse(queryCrawler); // set the query for the current crawler
					
					BooleanQuery termQuery = new BooleanQuery(); // query -- OR
					for (int i=0;i<myQueries.length;i++){
						termQuery.add(myQueries[i], Occur.SHOULD); // SHOULD implies that the keyword could be occur
					}
					// (word1 OR word2 OR ....) AND crawler
					BooleanQuery finalQuery = new BooleanQuery(); // sub-query -- AND
					finalQuery.add(termQuery, Occur.MUST);
					finalQuery.add(q_crawler, Occur.MUST);  // MUST implies that the keyword must occur for the specific crawler
					
					// Searching code
					int hitsPerPage = 10000;					// collect the top 10000 scoring hits.
					if (DirectoryReader.indexExists(index)){	// if already exist
					    IndexReader reader = DirectoryReader.open(index);	// set index
					    IndexSearcher searcher = new IndexSearcher(reader);
					    TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage);
					    searcher.search(finalQuery, collector);				// search occurrences for the specific query
					    ScoreDoc[] hits = collector.topDocs().scoreDocs;
					    
					    Hashtable<String, Integer> wordOccurDay = new Hashtable<String, Integer>();	// hash table with, key: date of occurrences, value:num of occurrences
					    for(int i=0;i<hits.length;++i) { // for every occur
					    	int docId = hits[i].doc;
					    	Document d = searcher.doc(docId);
					    	if (wordOccurDay.containsKey(d.get("\"day\"")))
					    		wordOccurDay.put(d.get("\"day\""), wordOccurDay.get(d.get("\"day\""))+1); // find the number of occurrences in a specific date
					    	else
					    		wordOccurDay.put(d.get("\"day\""), 1);
					    }
					    
					    ArrayList <String> keys = new ArrayList<String>(wordOccurDay.keySet());
					    Collections.sort(keys);						// sort hash table by day(key)
					    
					    for (String day : keys) {
							arrayFig.put(wordOccurDay.get(day));	// fill the values of y'y axis
							arrayFigXX.put(day);
						}
					}
				}
			} catch (SQLException ex) { // handle any errors
				myLog.info("SQLException: " + ex.getMessage());
				myLog.info("SQLState: " + ex.getSQLState());
				myLog.info("VendorError: " + ex.getErrorCode());
			} catch (ParseException e) { e.printStackTrace(); }
		}
		juser.put("crawlerCond", array);// data for the portlet
		juser.put("numDoc", arrayFig); // data for the diagrams
		juser.put("numDocXX", arrayFigXX);
		pw.println(juser.toString());
	}
	
	private static void addDoc(IndexWriter w, String text, String day, String crawler, String crawl_id) throws IOException{
		Document doc = new Document();

		doc.add(new TextField("\"text\"", text, Field.Store.YES));// a text field will be tokenized
		doc.add(new StringField("\"day\"", day, Field.Store.YES));// we use a string field for the day of crawling
		doc.add(new StringField("\"crawler\"", crawler, Field.Store.YES));// we use a string field for the name of crawler
		doc.add(new StringField("\"id\"", crawl_id, Field.Store.YES));// we use a string field for the id of crawler
		w.addDocument(doc);
	}
}