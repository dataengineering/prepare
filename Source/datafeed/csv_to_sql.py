import csv
import sys
import MySQLdb as mysqldb
import os

with open(sys.argv[1], 'rb') as csv_file:					#first argument is the csv file
    reader = csv.reader(csv_file)
    try:
        os.remove(sys.argv[2])						 	#second argument is the output sql script
    except OSError:
        pass
    sql_script = open(sys.argv[2], 'w')						
    counter = 0;
    try:
    	user_id_list = []
    	account_name_list = []

    	connection = mysqldb.connect('localhost', 'searchuser', 'searchUser_@!', 'prepare_crawler_db')
    	cur = connection.cursor()

    	for row in reader:
	    	counter += 1
	    	if counter == 1:
	    		continue
	    	ID = row[0]						#twitter_post.post_id
	    	created_at = row[1]					#twitter_post.created_at
	    	id_name = row[2]					#twitter_user.screen_name
	    	tweet_text = row[3]					#twitter_post.text
	    	followers = row[4]					#twitter_post.followers_when_published, twitter_user.followers_count
	    	retweet_count = row[5]					#twitter_post.retweet_count
	    	user_lang = row[6]					#twitter_post.language
	    	user_location = row[7]					#twitter_user.location
	    	user_time = row[8]					#twitter_user.timezone
	    	tweet_location = row[9]					#twitter_post.place

	    	followers = followers.replace(" ","")			#CSV had some spare spaces
	    	retweet_count = retweet_count.replace(" ","")		#CSV had some spare spaces
		retweet_count = retweet_count.replace("+","")		#retweet_count had the value of 100+
	    	user_lang = user_lang.replace(" ","")			#CSV had some spare spaces
	    	user_location = user_location.replace(" ","")		#CSV had some spare spaces
	    	user_time = user_time.replace(" ","")			#CSV had some spare spaces
	    	tweet_location = tweet_location.replace(" ","")		#CSV had some spare spaces
		created_at = created_at.replace(" ","")
		year = created_at[-4:]					#created_at is not compatible with mySQL date format
		month = created_at[3:6]
		month = month.lower()
		if month == 'jan':
			month = '01'
		elif month == 'feb':
			month = '02'
                elif month == 'mar':
                        month = '03'
                elif month == 'apr':
                        month = '04'
		elif month == 'may':
                        month = '05'
                elif month == 'jun':
                        month = '06'
                elif month == 'jul':
                        month = '07'
                elif month == 'aug':
                        month = '08'
                elif month == 'sep':
                        month = '09'
                elif month == 'oct':
                        month = '10'
                elif month == 'nov':
                        month = '11'
                elif month == 'dec':
                        month = '12'
		day = created_at[6:8]
		hh_mm_ss = created_at[8:16]
		yyyy_mm_dd = year + "-" + month + "-" + day
		created_at = yyyy_mm_dd + " " + hh_mm_ss
	    	account_name = id_name.replace(" ","")							#generate a fake account_name from screen_name by removing its whitespaces
	    	user_id = 0
	    	char_place = 1
	    	for char in account_name:								#generate a fake user_id based on the account_name
	    		char_place += 1									#the formula is: Sum([ASCII character code] * ([ASCII code digits' sum] / 3) ^ [character place]
	    		user_id += ord(char)*((sum(map(int,str(ord(char))))/3)**char_place)		#which should be unique in almost every case and will generate the same id for the same account_name
	    		
	    	insert_user = "INSERT IGNORE INTO twitter_user (user_id, followers_count, name, screen_name, location, timezone) VALUES ("+str(user_id)+","+str(followers)+",'"+id_name+"','"+account_name+"','"+user_location+"','"+user_time+"');"
	    	insert_source = "INSERT IGNORE INTO twitter_source (account_name, active) VALUES ('"+account_name+"',0);"
	    	
		cur.execute(insert_user)
		cur.execute('SELECT id FROM twitter_user WHERE user_id='+str(user_id)+';')
        	last_user_id = int(cur.fetchone()[0])

		sql_script.write(insert_user)
                sql_script.write("\n")


		cur.execute(insert_source)
		cur.execute("SELECT id FROM twitter_source WHERE account_name='"+account_name+"';")
                last_source_id = int(cur.fetchone()[0])

		sql_script.write(insert_source)
	        sql_script.write("\n")


	    	insert_post = "INSERT INTO twitter_post (post_id, created_at, place, retweet_count, followers_when_published, text, language, twitter_user_id, twitter_source_id, engine_type_id, engine_id) VALUES ("+ID+",'"+created_at+"','"+tweet_location+"',"+retweet_count+","+followers+",'"+tweet_text+"','"+user_lang+"',"+str(last_user_id)+","+str(last_source_id)+", 1, 1);"
	    	

	    	if False:							#set to true if you want to check for user_ids that exceed mysql's bigint max value
		    	if user_id > 9223372036854775807:
		    		print user_id
	    	if False:							#set this to True if you want to check for duplicate user_ids
		    	user_id_list.append(user_id)
		    	account_name_list.append(account_name)
		    	for index, ui in enumerate(user_id_list):
		    		if user_id == ui:
		    			if account_name != account_name_list[index]:
		    				print account_name
		    				print account_name_list[index]
		cur.execute(insert_post)
	    	#connection.commit()	
	    	sql_script.write(insert_post)
	    	sql_script.write("\n")
		#csv_file.close()
		#sql_script.close()
    except mysqldb.Error, e:
    	print "MySQL error %d: %s" % (e.args[0],e.args[1])
    	csv_file.close()
    	sql_script.close()
    	if connection:
    		connection.close()
    	sys.exit(1)
    finally:
	if connection:
                connection.close()
	csv_file.close()
	sql_script.close()
