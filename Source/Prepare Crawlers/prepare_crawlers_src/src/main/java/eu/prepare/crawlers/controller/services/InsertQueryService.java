package eu.prepare.crawlers.controller.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import eu.prepare.crawlers.storage.dba.AbstractDBA;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.prepare.crawlers.storage.dba.QueryDBA;
import java.io.PrintStream;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 * Servlet implementation class
 * <br>------------------------------------ ////
 * <br> EXAMPLE CALL: INSERT
 * <br> http://localhost:8080 /PrepareCrawlers/insert?
 * queries=[{"query_term":"Φυσική", "language_id":1}, {"query_term":"CERN
 * Institute","language_id":2}]
 * <br>it will insert these 2 queries and return a JSON (map) of query: response
 * <br>{"Φυσική":"OK","CERN Institute":"OK"}
 * <br>if the queries already exist in the DB, the response will be
 * {"Φυσική":"Omitted (already exists)","CERN Institute":"Omitted (already
 * exists)"}
 * <br> and if an error occurs, the response will be {"Φυσική":"Error: the
 * exception message thrown","CERN Institute":"Error: the exception message
 * thrown"}
 * <br> CAUTION language_id MUST be specified: 1:gr, 2:en, 3:de, 4:fr (verify
 * with 'language' lookup table for that)
 * <br>------------------------------------------------------------
 *
 * @author George K. <gkiom@iit.demokritos.gr>
 */
@WebServlet(urlPatterns = {"/insert"})
public class InsertQueryService extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @param request the request to handle
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     */
    protected void processRequest(final HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json;charset=UTF-8");
        DataSource ds = CrawlService.getDS();
        final PrintStream out = new PrintStream(response.getOutputStream());

        String insert = request.getParameter("queries");
        try {
            if (insert != null) {
                String res = insertQueries(ds, insert);
                out.print(res);
            } else {
                throw new IllegalArgumentException(PARAMS);
            }
        } finally {
            out.close();
            ds = null;
        }
    }

    private String insertQueries(DataSource ds, String insert) {
        Type listType = new TypeToken<List<Map<Object, Object>>>() {
        }.getType();
        List<Map<Object, Object>> expList = new Gson().fromJson(insert, listType);
        AbstractDBA storage;
        try {
            storage = new QueryDBA(ds);
            // insert query - ies
            String res = storage.insertQueries(expList);
            int terms = expList.size();
            System.out.format("OK, %d %s inserted%n", terms, terms == 1 ? "term" : "terms");
            return res;
        } finally {
            storage = null;
        }
    }
    public static final String PARAMS = "params:\n"
            + "queries=[{\"query_term\":\"term1\", \"language_id\":1|2|3}, {\"query_term\":\"term2\", \"language_id\":1|2|3}]";
    
  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
