/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.controller.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import eu.prepare.crawlers.impl.Crawler;
import eu.prepare.crawlers.storage.dba.AbstractDBA;
import eu.prepare.crawlers.storage.dba.MonitorSourceDBA;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.EnumMap;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class
 * <br>------------------------------------------------------------
 * <br> EXAMPLE CALL: INSERT SOURCES
 * <br>
 * http://localhost:8080/PrepareCrawlers/InsertSourceService?twitter_sources=[{"account_name":"source1", "active":1}, {"account_name":"source2","active":1}]
 * <br>it will insert these 2 twitter sources and return a JSON (map) of source: response {"source1":"OK","source2":"OK"}
 * <br>if the sources already exist in the DB, the response will be {"aekfc":"Omitted (already exists)","queen":"Omitted (already exists)"}
 * <br> and if an error occurs, the response will be {"source1":"Error: the exception message thrown", "source2":"Error: the exception message thrown"}
 * <br> CAUTION active MUST be specified: 1 for monitor, 0 for not monitor
 * <br> currently supports only twitter
 * <br>------------------------------------------------------------
 *
 * @author George K. <gkiom@iit.demokritos.gr>
 */
@WebServlet(name = "InsertSource", urlPatterns = {"/InsertSource"})
public class InsertSourceService extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("application/json;charset=UTF-8");

        DataSource ds = CrawlService.getDS();

        PrintWriter out = null;
        try {
            out = response.getWriter();
            String res = insertSources(ds, request);
            out.print(res);
        } finally {
            if (out != null) {
                out.close();
            }
            ds = null;
        }
    }

    private String insertSources(DataSource ds, HttpServletRequest request) throws IllegalArgumentException {

        Type listType = new TypeToken<List<Map<Object, Object>>>() {
        }.getType();
        Enumeration<String> params = request.getParameterNames();
        EnumMap<Crawler, List<Map<Object, Object>>> sources = new EnumMap<Crawler, List<Map<Object, Object>>>(Crawler.class);
        while (params.hasMoreElements()) {
            String nextParam = params.nextElement();
            if (nextParam.contains("_sources")) {
                String insert = request.getParameter(nextParam);
                List<Map<Object, Object>> expList = new Gson().fromJson(insert, listType);
                sources.put(
                        Crawler.valueOf(nextParam.split("_")[0].toUpperCase()),
                        expList);
            }
        }

        if (sources.isEmpty()) {
            throw new IllegalArgumentException(PARAMS);
        }

        AbstractDBA storage;
        try {
            storage = new MonitorSourceDBA(ds);
            // insert query - ies
            String res = storage.insertSources(sources);
            return res;
        } finally {
            storage = null;
        }
    }
    public static final String PARAMS = "params:\n"
            + "twitter_sources=[{\"account_name\":\"acc1\", \"active\":1|0}, {\"account_name\":\"acc2\", \"active\":1|0}]";

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
