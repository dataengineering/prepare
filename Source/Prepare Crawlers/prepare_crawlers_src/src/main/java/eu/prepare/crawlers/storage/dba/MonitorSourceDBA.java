/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.storage.dba;

import com.google.gson.Gson;
import eu.prepare.crawlers.impl.Crawler;
import eu.prepare.crawlers.storage.util.SQLUtils;
import eu.prepare.crawlers.utils.response.IResponse;
import eu.prepare.crawlers.utils.response.SourceResponse;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class MonitorSourceDBA extends AbstractDBA {

    public static final String ACC_NAME = "account_name";
    public static final String ACTIVE = "active";
    public static final String TWITTER_SOURCE_TBL = "twitter_source";

    public MonitorSourceDBA(DataSource datasource) {
        super(datasource);
    }

    /**
     *
     * @param sources
     * @return
     */
    @Override
    public String insertSources(EnumMap<Crawler, List<Map<Object, Object>>> sources) {
        Map<String, List<IResponse>> results = new LinkedHashMap();
        if (sources.containsKey(Crawler.TWITTER)) {
            List<IResponse> lRes = insertTwitterSources(sources.get(Crawler.TWITTER));
            results.put(Crawler.TWITTER.getDecl(), lRes);
        }
        // TODO: DO SO FOR OTHER THAT MAY BE REQUIRED FOR MONITORING
        return new Gson().toJson(results, Map.class);
    }

    protected List<IResponse> insertTwitterSources(List<Map<Object, Object>> sources) {
        List<IResponse> lRes = new ArrayList<IResponse>();
        String SQLINSERT
                = "INSERT INTO "
                .concat(TWITTER_SOURCE_TBL).concat("(")
                .concat(ACC_NAME).concat(", ").concat(ACTIVE)
                .concat(") VALUES (?, ?) ")
                .concat("ON DUPLICATE KEY UPDATE ")
                .concat(ACTIVE).concat("=").concat("?;");
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        String source_acc = "";
        try {
            dbConnection = datasource.getConnection();
            for (Map<Object, Object> sourceMap : sources) {
                source_acc = (String) sourceMap.get(ACC_NAME);
                Double activ = (Double) sourceMap.get(ACTIVE);
                pStmt = dbConnection.prepareStatement(SQLINSERT);
                pStmt.setString(1, source_acc);
                // set active
                pStmt.setInt(2, activ.intValue());
                pStmt.setInt(3, activ.intValue());
                int iRes = pStmt.executeUpdate();
                SourceResponse ir;
                if (iRes == 1) {
                    System.out.println("Inserted account: " + source_acc);
                    ir = new SourceResponse(source_acc, RES_OK);
                    lRes.add(ir);
                } else if (iRes == 2) {
                    System.out.println("Updated account: " + source_acc);
                    ir = new SourceResponse(source_acc, RES_UPDATED_ACTIVE_STATE);
                    lRes.add(ir);
                }
            }
        } catch (ClassCastException ex) {
            Logger.getLogger(QueryDBA.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            IResponse ir = new SourceResponse(source_acc, RES_ERROR + ex.toString());
            lRes.add(ir);
        } catch (SQLException ex) {
            Logger.getLogger(QueryDBA.class.getName()).log(Level.SEVERE, null, ex);
            IResponse ir = new SourceResponse(source_acc, RES_ERROR + ex.toString());
            lRes.add(ir);
        } catch (Exception ex) {
            Logger.getLogger(QueryDBA.class.getName()).log(Level.SEVERE, null, ex);
            IResponse ir = new SourceResponse(source_acc, RES_ERROR + ex.toString());
            lRes.add(ir);
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
//        return new Gson().toJson(lRes, List.class);
        return lRes;
    }

    @Override
    public String insertQueries(List<Map<Object, Object>> queries) {
        throw new UnsupportedOperationException("Not supported");
    }

}
