/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.storage.model;

/**
 * represents a query string, and it's attributes, such as id, language_id, etc.
 *
 * @author George K. <gkiom@iit.demokritos.gr>
 */
public class PQuery {

    /**
     * The query ID
     */
    private long id;
    /**
     * The actual query Term
     */
    private String query_term;
    /**
     * The language ID of the query
     */
    private int language_id;

    public PQuery(long id, String query_term, int language_id) {
        this.id = id;
        this.query_term = query_term;
        this.language_id = language_id;
    }

    public long getId() {
        return id;
    }

    public String getQueryTerm() {
        return query_term;
    }

    public int getLanguageID() {
        return language_id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setQueryTerm(String query_term) {
        this.query_term = query_term;
    }

    public void setLanguageID(int language_id) {
        this.language_id = language_id;
    }

    @Override
    public String toString() {
        return "Query{" + "id=" + id
                + ", query_term=" + query_term
                + ", language_id=" + language_id + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.query_term != null ? this.query_term.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PQuery other = (PQuery) obj;
        return !((this.query_term == null) ? (other.query_term != null) : !this.query_term.equals(other.query_term));
    }
}
