package eu.prepare.crawlers.impl.gplus.json;

import com.google.gson.annotations.SerializedName;

public class Comment {

    public String content;

    public String updated;

    @SerializedName("user id")
    public String userId;

    @SerializedName("number of plusoners")
    public long numberOfPlusoners;

    public String activity;

    @SerializedName("user url")
    public String userUrl;

    public String published;

    @SerializedName("comment selfLink")
    public String commentSelfLink;

    @SerializedName("comment id")
    public String commentId;

    @SerializedName("user displayName")
    public String userDisplayName;

    @SerializedName("user gender")
    public String userGender;

    @Override
    public String toString() {
        return "Comment [content=" + content + ", updated=" + updated
                + ", userId=" + userId + ", userGender=" + userGender
                + ", numberOfPlusoners=" + numberOfPlusoners + ", activity=" + activity + ", userUrl="
                + userUrl + ", published=" + published + ", commentSelfLink="
                + commentSelfLink + ", commentId=" + commentId
                + ", userDisplayName=" + userDisplayName + "]";
    }

}
