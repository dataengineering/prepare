/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl.twitter.storage;

import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.storage.model.Storable;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import twitter4j.Status;
import twitter4j.User;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public interface IRepository {

    public String TBL_TWITTER_POST = "twitter_post";
    public String TBL_TWITTER_USER = "twitter_user";

    /**
     *
     * @param fetch_all if true, return all entered accounts, else only the
     * 'active' ones
     * @return the accounts to monitor from the repository
     */
    Map<Long, String> getAccounts(boolean fetch_all);

    /**
     * loads the sources by the name provided. If the source does not exist,
     * ignores it
     *
     * @param sources the specific sources to load
     * @return
     */
    Map<Long, String> getAccountsFromSpecifiedSources(Collection<String> sources);

    void switchAccountStatus(String account);

    boolean isActiveAccount(String account);

    /**
     * saves a userID (account name) in the DB
     *
     * @param accountName the twitter account name to save
     * @param active true if the source will be used for crawl, false if it used
     * only for reference on post found by search queries
     * @return the id of the newly inserted source
     */
    long insertSourceAccount(String accountName, boolean active);

    public long getSourceAccountID(String sourceAcc);

    public boolean existSource(String sourceAcc);

    /**
     *
     * @param user the {@link User} object to insert
     * @return the user ID generated
     */
    long insertUser(User user);

    void updateUser(User user);

    /**
     *
     * @param userID the user ID to check against
     * @return the ID of the supplied user_id in the DB
     */
    public long existsUser(long userID);

    /**
     *
     * @param post the status to insert
     * @param userKey the user key
     * @param sourceID the source key (in the DB)
     * @param followersWhenPublished
     * @param engine_type_id crawl | monitor
     * @param engine_id
     */
    void insertPost(Status post, long userKey, long sourceID, int followersWhenPublished, int engine_type_id, long engine_id);

    /**
     *
     * @param postID the post ID to update
     * @param retweetCount the current retweet count
     */
    void updatePost(long postID, long retweetCount);

    /**
     *
     * @param postID the (twitter api) post ID to check for existence
     * @return true if exists in DB, false otherwise
     */
    public boolean existsPost(long postID);

    /**
     *
     * @param generatedKey the post ID
     * @param hashtag the hashtag contained in the tweet
     */
    void insertHashtag(long generatedKey, String hashtag);

    void insertExternalURLs(long generatedID, List<String> lsURLs);

    Map<Integer, String> getTotalRetweets();

    /**
     * update content with the status found
     *
     * @param crawl_id
     * @param query
     * @param status
     * @return
     */
    int updateContent(long crawl_id, PQuery query, Status status);

    /**
     * store demographics for twitter user
     */
    void storeTwitterUserInfo(long userId, String url, String timestamp, String sourceTable, long queryId)
            throws SQLException;

    /**
     * search the twitter repository and update with the content found the main
     * content repository
     *
     * @param query
     * @return
     */
    Set<Storable> searchContent(PQuery query);

    int updateContent(long crawl_id, Set<Storable> to_store, String term, int min_token_cnt);
}
