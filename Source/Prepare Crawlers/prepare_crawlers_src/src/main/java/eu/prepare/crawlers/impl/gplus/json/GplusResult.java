package eu.prepare.crawlers.impl.gplus.json;

import java.util.Arrays;

public class GplusResult {

    public String query;

    public Result[] results;

    @Override
    public String toString() {
        return "GplusResult [query=" + query + ", results="
                + Arrays.toString(results) + "]";
    }

}
