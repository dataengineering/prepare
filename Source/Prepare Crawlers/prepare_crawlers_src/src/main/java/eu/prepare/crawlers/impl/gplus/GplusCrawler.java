package eu.prepare.crawlers.impl.gplus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.ParseException;

import com.google.gson.Gson;
import eu.prepare.crawlers.impl.AbstractCrawler;
import eu.prepare.crawlers.impl.ICrawler;
import eu.prepare.crawlers.impl.gplus.storage.GplusSQLStorage;
import eu.prepare.crawlers.impl.gplus.json.Comment;
import eu.prepare.crawlers.impl.gplus.json.GplusResult;
import eu.prepare.crawlers.impl.gplus.json.Result;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.utils.Configuration;
import eu.prepare.crawlers.utils.CrawlerUtils;
import eu.prepare.crawlers.utils.MonitorUtils;
import java.nio.channels.FileLock;
import java.util.Collection;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import javax.sql.DataSource;

public class GplusCrawler extends AbstractCrawler implements ICrawler {

    public static final String TBL_GPLUS_POST = "gplus_post";
    public static final String TBL_GPLUS_COMMENT = "gplus_comment";

    private String db_host;
    private String db_user;
    private String db_pwd;
    private String query_db_name;
    private String query_table;
    private String output_file;
    private String max_activities;
    private String max_comments;
    private String workingDir;
    private final String api_key;

    private GplusSQLStorage storage;

    public static int MIN_TOKENS = 10;

    public GplusCrawler(DataSource dataSource, String db_host,
            String db_user, String db_pwd, String query_db_name,
            String query_table, String output_file, String max_activities,
            String max_comments, String workingDir, String api_key) throws SQLException {
        super(dataSource);
        this.db_host = db_host;
        this.db_user = db_user;
        this.db_pwd = db_pwd;
        this.query_db_name = query_db_name;
        this.query_table = query_table;
        this.output_file = output_file;
        this.max_activities = max_activities;
        this.max_comments = max_comments;
        this.workingDir = workingDir;
        this.storage = new GplusSQLStorage(
                dataSource,
                Integer.parseInt(max_activities),
                Integer.parseInt(max_comments)
        );
        this.min_tokens = MIN_TOKENS;
        this.api_key = api_key;
    }

    public GplusCrawler(DataSource dataSource, Configuration config) {
        super(dataSource);
        this.db_host = config.getGplusDBHost();
        this.db_user = config.getDatabaseUserName();
        this.db_pwd = config.getDatabasePassword();
        this.query_db_name = config.getGplusQueryDBName();
        this.query_table = config.getGplusQueryTable();
        this.output_file = config.getGplusOutputFile();
        this.workingDir = config.getWorkingDir();
        this.max_activities = config.getGplusMaxActivities();
        this.max_comments = config.getGplusMaxComments();
        this.min_tokens = config.getMinTokens();
        this.storage = new GplusSQLStorage(
                dataSource,
                Integer.parseInt(max_activities),
                Integer.parseInt(max_comments)
        );
        this.api_key = config.getGplusAPIKey();
    }

    @Override
    public int crawl(long crawl_id, PQuery qQuery) {
        return crawl(qQuery.getQueryTerm(), qQuery.getId(), crawl_id);
    }

    protected int crawl(String query, long query_id, long crawl_id) {
        int cntRes = 0;
        Random r = new Random();
        // first of all sleep for 1 - 5 seconds
        int sleep_time = 1 + r.nextInt(5);
        FileLock lock = null;
        try {
            TimeUnit.SECONDS.sleep(sleep_time);
//            // afterwards, file lock only one execution at a time
//            lock = MonitorUtils.acquireFileLock(workingDir, "gplus");
            // call python
            int termValue = runScript(query);
            if (termValue == 0) {
//                System.out.println("python finalized... parsing results...");
                GplusResult[] gplusResults = parseJson();
                storeGplusResults(gplusResults, query_id);
            }
            cntRes += storage.moveContent(crawl_id, query_id, TBL_GPLUS_POST);
            cntRes += storage.moveContent(crawl_id, query_id, TBL_GPLUS_COMMENT);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
//            MonitorUtils.releaseLock(lock);
        }
        return cntRes;
    }

    public int runScript(String sQuery) throws IOException, InterruptedException {
//        LOGGER.info("init python gplus script..."); // debug
        String line = null;
        ProcessBuilder processBuilder = new ProcessBuilder("python", "gplus_main.py", "--db_host=" + db_host,
                "--db_user=" + db_user, "--db_pwd=" + db_pwd, "--query_db_name=" + query_db_name,
                "--query_table=" + query_table, "--output_file=" + output_file,
                "--max_activities=" + max_activities, "--max_comments=" + max_comments,
                "--query_string=" + sQuery,
                "--gplus_API_Key=" + api_key);

        File scriptDir = new File(workingDir + "gplus_script/");

        processBuilder.directory(scriptDir);
        Process p = processBuilder.start();

        BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
        BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        while ((line = bri.readLine()) != null) {
            System.out.println(line);
        }
        bri.close();
        while ((line = bre.readLine()) != null) {
            System.err.println(line);
        }
        bre.close();
        int termValue = p.waitFor();
        //System.out.println("Done running script.");
//        LOGGER.log(Level.INFO, "finalized python gplus script: {0}", (termValue == 0 ? "OK" : "Error")); // debug
        return termValue;
    }

    public GplusResult[] parseJson() throws FileNotFoundException {
        Gson gson = new Gson();
        //String workingDir = System.getProperty("user.dir");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(workingDir + "gplus_script/" + output_file));
        GplusResult[] gplusResults = gson.fromJson(bufferedReader, GplusResult[].class);
        return gplusResults;
    }

    public void storeGplusResults(GplusResult[] gplusResults, long queryID) {
        for (GplusResult gplusResult : gplusResults) {
            String query = gplusResult.query;
            for (Result result : gplusResult.results) {
                try {
                    // if should store
                    if (CrawlerUtils.shouldStore(result.content, query, min_tokens)) {
                        long activityKey = storage.storeActivity(result, queryID);
                        for (Comment comment : result.comments) {
                            // strip html from content
                            String striped_content = CrawlerUtils.stripHtmlTagsWithNewLines(comment.content, true);
                            if (CrawlerUtils.shouldStore(striped_content, query, min_tokens)) {
                                storage.storeComment(activityKey, comment, striped_content, queryID, query);
                            }
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int monitor(long monitor_id, Collection<String> sources) {
//        System.out.println("gplus_monitor: " + sources.toString());
//        return 4;
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
