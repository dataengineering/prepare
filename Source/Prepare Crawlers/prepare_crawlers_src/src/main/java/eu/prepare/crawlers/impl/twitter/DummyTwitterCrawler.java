/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl.twitter;

import eu.prepare.crawlers.impl.AbstractCrawler;
import eu.prepare.crawlers.impl.ICrawler;
import eu.prepare.crawlers.storage.model.PQuery;
import java.util.Collection;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class DummyTwitterCrawler extends AbstractCrawler implements ICrawler {

    public DummyTwitterCrawler(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public int crawl(long crawl_id, PQuery query) {
        try {
            Thread.sleep(2000l);
        } catch (InterruptedException ex) {
            Logger.getLogger(DummyTwitterCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Random().nextInt(15);
    }

    @Override
    public int monitor(long monitor_id, Collection<String> sources) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

}
