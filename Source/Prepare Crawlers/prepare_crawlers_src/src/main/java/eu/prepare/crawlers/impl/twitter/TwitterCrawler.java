/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl.twitter;

import eu.prepare.crawlers.impl.AbstractCrawler;
import eu.prepare.crawlers.impl.ICrawler;
import eu.prepare.crawlers.storage.dba.CrawlerDBA;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.storage.model.Storable;
import eu.prepare.crawlers.impl.twitter.storage.IRepository;
import eu.prepare.crawlers.impl.twitter.storage.MySQLRepository;
import eu.prepare.crawlers.storage.dba.CrawlerDBA.Engine;
import eu.prepare.crawlers.storage.dba.MonitorDBA;
import eu.prepare.crawlers.storage.demographics.DemographicsDB;
import eu.prepare.crawlers.utils.Configuration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import javax.sql.DataSource;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class TwitterCrawler extends AbstractCrawler implements ICrawler {

    private final String twitterConsumerKey;
    private final String twitterConsumerKeySecret;
    private final String twitterAccessTokken;
    private final String twitterAccessTokkenSecret;

    private final IRepository repository;
    private final Twitter twitter;
    public static final int MAX_RESULTS_PER_SINGLE_QUERY = 80;

    /**
     *
     * @param config
     * @param dataSource
     */
    public TwitterCrawler(Configuration config, DataSource dataSource) {
        super(dataSource);
        this.twitterConsumerKey = config.getTwitterConsumerKey();
        this.twitterConsumerKeySecret = config.getTwitterConsumerKeySecret();
        this.twitterAccessTokken = config.getTwitterAccessTokken();
        this.twitterAccessTokkenSecret = config.getTwitterAccessTokkenSecret();
        this.twitter = initialize();
        this.min_tokens = 5;
        this.repository = new MySQLRepository(dataSource, new DemographicsDB(dataSource));
    }

    @Override
    public int crawl(long crawl_id, PQuery query) {
        int engine_type_id = CrawlerDBA.getEngineTypeId(dataSource, Engine.CRAWL);
        int res = 0;
        // search content already found for the query and store it
        Set<Storable> found = repository.searchContent(query);
        // update content with storables
        res += repository.updateContent(crawl_id, found, query.getQueryTerm(), min_tokens);
        // query twitter API 
        Query q = new Query(query.getQueryTerm());
        // set lang
        q.setLang(CrawlerDBA.getLanguageKey(dataSource, query.getLanguageID()));
        // set max possible results
        q.setCount(MAX_RESULTS_PER_SINGLE_QUERY);
        try {
            // query
            QueryResult qr = twitter.search(q);
            // get tweets
            List<Status> statuses = qr.getTweets();

            // insert into twitter repository
            List<Status> unique_tweets = processStatuses(statuses, engine_type_id, crawl_id);

            // insert into content repository
            for (Status status : unique_tweets) {
                res += repository.updateContent(crawl_id, query, status);
            }
        } catch (TwitterException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return res;
    }

    /**
     * Will load all accounts from the DB and update with the latest tweets,
     * also update existing tweets's retweet counts
     */
    @Override
    public int monitor(long monitor_id, Collection<String> sources) {

        int engine_type_id = CrawlerDBA.getEngineTypeId(dataSource, Engine.MONITOR);

        /*System.out.println("Remaining API requests before the API limit is reached for the current hour: " + twitter.getRateLimitStatus());
         System.out.println();*/
        Map<Long, String> accounts;
        // if no accounts provided, fetch all active
        if (sources == null || sources.isEmpty()) {
            // get accounts to crawl from the database
            accounts = repository.getAccounts(false);
            LOGGER.log(Level.INFO, "Started monitor at {0}, accounts: {1}",
                    new Object[]{new Date(), accounts.size()});
        } else {
            // fetch all provided, that already exist in the db (ignore non existing)
            accounts = repository.getAccountsFromSpecifiedSources(sources);
            LOGGER.log(Level.INFO, "Started monitor at {0}, accounts: {1}",
                    new Object[]{new Date(), accounts.values().toString()});
        }

        int iCount = 1;
        int iTotal = accounts.size();
        // for each account
        for (Map.Entry<Long, String> sourceAccount : accounts.entrySet()) {
            try {
                // get account name
                String sourceName = sourceAccount.getValue();
                LOGGER.log(Level.INFO, "Parsing: {0} : {1}/{2} accounts",
                        new Object[]{sourceName, iCount++, iTotal});
                // get posts from selected account
                List<Status> statuses = twitter.getUserTimeline(sourceName);
                // insert statuses to DB
                processStatuses(statuses, engine_type_id, monitor_id);

            } catch (TwitterException ex) {
                LOGGER.severe(ex.toString());
            }
        }
        LOGGER.log(Level.INFO, "Finished monitor at {0}", new Date());
        int res = MonitorDBA.numDocsFound(dataSource, engine_type_id, monitor_id);
        return res;
    }

    protected List<Status> processStatuses(List<Status> statuses, int engine_type_id, long engine_id) {
        List<Status> res = new ArrayList();
        // for each status
        for (Status status : statuses) {
            long sourceID = -1l;
            // if it is a retweet, get the original tweet
            while (status.isRetweet()) {
                status = status.getRetweetedStatus();
            }
            // add status to result list (avoid retweets)
            res.add(status);
            // proceed with storing in twitter repository
            long postID = status.getId();
            User user = status.getUser();
            // check for existance of post in DB
            boolean exists = repository.existsPost(postID);
            // if post already in the db then update post and user info
            if (exists) {
                long retweetCount = status.getRetweetCount();
                repository.updatePost(postID, retweetCount);
                repository.updateUser(user);
            } else {
                // get User ID
                long userID = user.getId();
                // check if user exists in the DB
                long userGeneratedKey = repository.existsUser(userID);
                String sourceAcc = user.getScreenName();
                if (userGeneratedKey != -1) {
                    // if user is in the database, update
                    repository.updateUser(user);
                } else {
                    // else insert and get auto generated ID
                    userGeneratedKey = repository.insertUser(user);
                }
                // get source ID
                if (!repository.existSource(sourceAcc)) {
                    // also insert as a source
                    sourceID = repository.insertSourceAccount(sourceAcc, false);
                } else {
                    // fetch sourceID
                    sourceID = repository.getSourceAccountID(sourceAcc);
                }
                if (userGeneratedKey == -1) {
                    LOGGER.warning("Exception during user insert. See previous message");
                }
                // get followers of user when post was published
                int followersWhenPublished = user.getFollowersCount();
                // finally, insert the post in the DB
                repository.insertPost(status, userGeneratedKey, sourceID, followersWhenPublished, engine_type_id, engine_id);
            }
        }
        return res;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    protected final Twitter initialize() {
        //connect
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(twitterConsumerKey)
                .setOAuthConsumerSecret(twitterConsumerKeySecret)
                .setOAuthAccessToken(twitterAccessTokken)
                .setOAuthAccessTokenSecret(twitterAccessTokkenSecret);
        TwitterFactory tf = new TwitterFactory(cb.build());
        // get active instance
        return tf.getInstance();
    }

//    // TODO: design & implement
//    private void checkUnmonitoredSources() {
//        Map<Long, String> all_sources = repository.getAccounts(true);
//
//        Map<String, Double> account_activities = new HashMap<String, Double>();
//
//        for (String account : all_sources.values()) {
//            double activity = getActivity(account);
//            account_activities.put(account, activity);
//        }
//
//        double activity_threshold = 0.1;
//
//        for (Map.Entry<String, Double> entry : account_activities.entrySet()) {
//            String account = entry.getKey();
//            Double activity = entry.getValue();
//
//            if (activity >= activity_threshold) {
//                if (!repository.isActiveAccount(account)) {
//                    repository.switchAccountStatus(account);
//                }
////            } else {
////                if (repository.isActiveAccount(account)) {
////                    repository.switchAccountStatus(account);
////                }
//            }
//        }
//
//    }
//
//    /**
//     *
//     * @param account
//     * @return
//     */
//    private double getActivity(String account) {
//        double activity = 0;
//        // find the sum of results that this crawler holds
//        // return the percentage of results that this source holds over the total results
//
//        return activity;
//    }
}
