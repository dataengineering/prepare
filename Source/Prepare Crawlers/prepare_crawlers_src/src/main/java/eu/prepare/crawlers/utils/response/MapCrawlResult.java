/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.utils.response;

import java.util.List;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class MapCrawlResult {

    private String name;
    private List<CrawlResult> crawl_results;

    public MapCrawlResult(String name, List<CrawlResult> crawl_results) {
        this.name = name;
        this.crawl_results = crawl_results;
    }

    public MapCrawlResult(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CrawlResult> getCrawlResults() {
        return crawl_results;
    }

    public void setCrawlResults(List<CrawlResult> crawl_results) {
        this.crawl_results = crawl_results;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 43 * hash + (this.crawl_results != null ? this.crawl_results.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MapCrawlResult other = (MapCrawlResult) obj;
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if (this.crawl_results != other.crawl_results && (this.crawl_results == null || !this.crawl_results.equals(other.crawl_results))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "MapCrawlResponse{" + "name=" + name + ", crawl_results=" + crawl_results + '}';
    }
}
