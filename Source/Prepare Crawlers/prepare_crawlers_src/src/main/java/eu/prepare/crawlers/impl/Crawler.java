/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl;

import eu.prepare.crawlers.impl.bing.BingCrawler;
import eu.prepare.crawlers.impl.gplus.GplusCrawler;
import eu.prepare.crawlers.impl.youtube.YoutubeCrawler;
import eu.prepare.crawlers.impl.twitter.TwitterCrawler;
import java.util.EnumMap;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public enum Crawler {

    TWITTER("twitter"),
    GPLUS("gplus"),
    YOUTUBE("youtube"),
    BING("bing");

    private String decl;

    private Crawler(String decl) {
        this.decl = decl;
    }

    public String getDecl() {
        return decl;
    }

    public static final EnumMap<Crawler, String> crawler_declarations = new EnumMap<Crawler, String>(Crawler.class);

    static {
        crawler_declarations.put(BING, BingCrawler.class.getSimpleName());
        crawler_declarations.put(YOUTUBE, YoutubeCrawler.class.getSimpleName());
        crawler_declarations.put(GPLUS, GplusCrawler.class.getSimpleName());
        crawler_declarations.put(TWITTER, TwitterCrawler.class.getSimpleName());
    }
}
