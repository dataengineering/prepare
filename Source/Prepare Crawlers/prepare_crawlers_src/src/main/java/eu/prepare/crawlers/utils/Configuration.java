/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Properties;

/**
 *
 * @author George K. <gkiom@iit.demokritos.gr>
 */
public class Configuration {

    private final Properties properties;
    private File file;
    public static final String FILE_SEPARATOR = System.getProperty("file.separator");

    private static Connection dbCon;

    public Configuration(String configurationFileName) {
        file = new File(configurationFileName);
        this.properties = new Properties();
        try {
            this.properties.load(new FileInputStream(file));
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public String getDatabaseHost() {
        return properties.getProperty("databaseHost");
    }

    public String getDatabaseUserName() {
        return properties.getProperty("databaseUsername");
    }

    public String getDatabasePassword() {
        return properties.getProperty("databasePassword");
    }

    /**
     *
     * @return the directory where all files are stored and read from
     */
    public String getWorkingDir() {
        String sWorkingDir = properties.getProperty("workingDir");

        if (!sWorkingDir.endsWith(FILE_SEPARATOR)) {
            return sWorkingDir + FILE_SEPARATOR;
        } else {
            return sWorkingDir;
        }
    }

    /**
     * Manually sets 'working directory'
     *
     * @param sWorkingDir the path to set
     * @see {@link #getWorkingDir() }
     */
    public void setWorkingDir(String sWorkingDir) {
        properties.put("workingDir", sWorkingDir);
    }

    public String getBingAppID() {
        return properties.getProperty("bingAppId");
    }

    /**
     *
     * @return the ip of the DB needed in blogger and gplus
     */
    public String getGplusDBHost() {
        return properties.getProperty("db_host");
    }

    /**
     *
     * @return the database name that contains the queries needed in blogger and
     * gplus
     */
    public String getGplusQueryDBName() {
        return properties.getProperty("query_db_name");
    }

    /**
     *
     * @return the name of the table containing the queries
     */
    public String getGplusQueryTable() {
        return properties.getProperty("query_table");
    }

    /**
     *
     * @return the permitted number of activities to store per query result
     */
    public String getGplusMaxActivities() {
        return properties.getProperty("max_activities");
    }

    /**
     *
     * @return the permitted number of comments to store per query result
     */
    public String getGplusMaxComments() {
        return properties.getProperty("max_comments");
    }

    /**
     *
     * @return the name of the file that the gplus script will store results
     * into.
     */
    public String getGplusOutputFile() {
        return properties.getProperty("gplus_output_file");
    }

    public String getGplusAPIKey() {
        return properties.getProperty("gplus_API_Key");
    }

    public String getYouTubeAppName() {
        return properties.getProperty("youtubeAppName");
    }

    public String getYouTubeDevID() {
        return properties.getProperty("youtubeDevID");
    }

    public int getMaxCrawlingThreads() {
        return Integer.valueOf(properties.getProperty("max_crawl_threads", "10"));
    }

    public int getMinTokens() {
        return Integer.valueOf(properties.getProperty("min_tokens", "10"));
    }

    /**
     *
     * @return the parent domains that will be omitted from crawling.
     */
    public String[] getBingAbortURLs() {
        return properties.getProperty("abort_urls").split("\\s*,\\s*");
    }

    public String getTwitterConsumerKey() {
        return properties.getProperty("twitterConsumerKey");
    }

    public String getTwitterConsumerKeySecret() {
        return properties.getProperty("twitterConsumerKeySecret");
    }

    public String getTwitterAccessTokken() {
        return properties.getProperty("twitterAccessTokken");
    }

    public String getTwitterAccessTokkenSecret() {
        return properties.getProperty("twitterAccessTokkenSecret");
    }

    /**
     *
     * @return the time limit set for a query to block the threads (in minutes)
     */
    public int getQueryTimeLimit() {
        return Integer.parseInt(properties.getProperty("query_time_limit", "30"));
    }
}
