/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.prepare.crawlers.utils.response;

import com.google.gson.Gson;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class MonitorResponse implements IResponse {

    private final long monitor_id;
    private final String monitor_name;
    private final Date started;
    private Date ended;
    private Map<String, Integer> results; 

    public MonitorResponse(long monitor_id, String monitor_name, Date started) {
        this.monitor_id = monitor_id;
        this.monitor_name = monitor_name;
        this.started = started;
        this.results = new HashMap();
    }

    public void setEnded(Date ended) {
        this.ended = ended;
    }

    public void setFetched(Map<String, Integer> fetched) {
        this.results = fetched;
    }

    public void appendFetched(String crawler, int num_docs) {
        this.results.put(crawler, num_docs);
    }
    
    @Override
    public String toString() {
        return "MonitorResponse{" + "monitor_id=" + monitor_id + ", monitor_name=" + monitor_name + ", started=" + started + ", ended=" + ended + ", fetched=" + results + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (int) (this.monitor_id ^ (this.monitor_id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MonitorResponse other = (MonitorResponse) obj;
        if (this.monitor_id != other.monitor_id) {
            return false;
        }
        return true;
    }

    @Override
    public String toJSON() {
        return new Gson().toJson(this, MonitorResponse.class);
    }
    
}
