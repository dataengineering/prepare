/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.utils.response;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class CrawlResponse {

    protected long crawl_id;
    protected MapCrawlResult crawl_response;

    public CrawlResponse(long crawl_id, MapCrawlResult crawl_results) {
        this.crawl_id = crawl_id;
        this.crawl_response = crawl_results;
    }

    public CrawlResponse(long crawl_id) {
        this.crawl_id = crawl_id;
    }

    public long getCrawlID() {
        return crawl_id;
    }

    public void setCrawl_id(int crawl_id) {
        this.crawl_id = crawl_id;
    }

    public MapCrawlResult getCrawler_results() {
        return crawl_response;
    }

    public void setCrawlerResults(MapCrawlResult crawler_results) {
        this.crawl_response = crawler_results;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + (int) (this.crawl_id ^ (this.crawl_id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CrawlResponse other = (CrawlResponse) obj;
        if (this.crawl_id != other.crawl_id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CrawlResponse{" + "crawl_id=" + crawl_id + ", crawl_response=" + crawl_response + '}';
    }

}
