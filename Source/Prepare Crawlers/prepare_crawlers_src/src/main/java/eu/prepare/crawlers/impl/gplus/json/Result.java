package eu.prepare.crawlers.impl.gplus.json;

import java.util.Arrays;

import com.google.gson.annotations.SerializedName;

public class Result {

    public String updated;

    @SerializedName("number of plusoners")
    public long numberOfPlusoners;

    @SerializedName("number of reshares")
    public long numberOfReshares;

    @SerializedName("user displayName")
    public String userDisplayName;

    @SerializedName("user id")
    public String userId;

    @SerializedName("activity id")
    public String activityId;

    @SerializedName("activity url")
    public String activityUrl;

    @SerializedName("user url")
    public String userUrl;

    public String title;

    public Comment[] comments;

    public String content;

    @SerializedName("content url")
    public String contentUrl;

    @SerializedName("user gender")
    public String userGender;

    public String published;

    @Override
    public String toString() {
        return "Result [updated=" + updated + ", numberOfPlusoners="
                + numberOfPlusoners + ", numberOfReshares=" + numberOfReshares
                + ", userDisplayName=" + userDisplayName + ", userId=" + userId
                + ", userGender=" + userGender + ", activityId=" + activityId + ", activityUrl=" + activityUrl
                + ", userUrl=" + userUrl + ", title=" + title + ", comments="
                + Arrays.toString(comments) + ", content=" + content
                + ", contentUrl=" + contentUrl + ", published=" + published + "]";
    }

}
