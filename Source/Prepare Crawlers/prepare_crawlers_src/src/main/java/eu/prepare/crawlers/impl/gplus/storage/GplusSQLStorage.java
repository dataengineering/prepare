/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl.gplus.storage;

import eu.prepare.crawlers.impl.gplus.GplusCrawler;
import eu.prepare.crawlers.impl.gplus.json.Comment;
import eu.prepare.crawlers.impl.gplus.json.Result;
import eu.prepare.crawlers.storage.dba.CrawlerDBA;
import eu.prepare.crawlers.utils.CrawlerUtils;
import eu.prepare.crawlers.storage.util.SQLUtils;
import eu.prepare.crawlers.storage.demographics.DemographicsDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import javax.sql.DataSource;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class GplusSQLStorage {

    private final DataSource dataSource;
    private final DemographicsDB demogStore;

    /**
     * max activities and comments fetched at a single execution (setup from
     * system configuration)
     */
    private int max_activities, max_comments;
    private static final String JSON_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    public GplusSQLStorage(DataSource dataSource, int max_activities, int max_comments) {
        this.dataSource = dataSource;
        this.demogStore = new DemographicsDB(dataSource);
    }

    public long storeActivity(Result activity, long queryId)
            throws SQLException, ParseException {

        SimpleDateFormat sdfToSQL = new SimpleDateFormat(CrawlerDBA.DATE_FORMAT);
        SimpleDateFormat sdfFromJson = new SimpleDateFormat(JSON_DATE_FORMAT);
        String updated = sdfToSQL.format(sdfFromJson.parse(activity.updated));
        // Strip html tags, keep plain text
        String sContent = CrawlerUtils.stripHtmlTagsWithNewLines(activity.content, true);
//                String sContent = CrawlerUtils.getCleanContent(activity.content, null)

        Long id = getPostKey(activity.activityId, queryId);
        if (id != null) {
            updatePost(id, updated, activity.numberOfPlusoners, activity.numberOfReshares);
            return id;
        }
//        System.out.println("Storing Post...");
        String authorKey
                = storeAuthor(activity.userId, activity.userUrl, activity.userDisplayName, activity.userGender);
        String sql = "INSERT INTO " + GplusCrawler.TBL_GPLUS_POST + " "
                + "(`activity_id`, `activity_url`, `published`, `updated`, `plusoners`, `reshares`, "
                + "`title`, `content_url`, `content`, `language`, `gplus_user_id`, `query_id`)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
                + "ON DUPLICATE KEY UPDATE updated=?, plusoners=?, reshares=?;";
        String published = sdfToSQL.format(sdfFromJson.parse(activity.published));
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, activity.activityId);
            pStmt.setString(2, activity.activityUrl);
            pStmt.setString(3, published);
            pStmt.setString(4, updated);
            pStmt.setLong(5, activity.numberOfPlusoners);
            pStmt.setLong(6, activity.numberOfReshares);
            pStmt.setString(7, activity.title);
            pStmt.setString(8, activity.contentUrl);
            pStmt.setString(9, sContent);
            pStmt.setString(10, CrawlerUtils.identifyLanguage(sContent));
            pStmt.setString(11, authorKey);
            pStmt.setLong(12, queryId);
            pStmt.setString(13, updated);
            pStmt.setLong(14, activity.numberOfPlusoners);
            pStmt.setLong(15, activity.numberOfReshares);
            pStmt.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
        return getPostKey(activity.activityId, queryId);
    }

    public void storeComment(long activityKey, Comment comment, String striped_content, long queryId, String sQuery) {
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {

            SimpleDateFormat sdfToSQL = new SimpleDateFormat(CrawlerDBA.DATE_FORMAT);
            SimpleDateFormat sdfFromJson = new SimpleDateFormat(JSON_DATE_FORMAT);
            String updated = sdfToSQL.format(sdfFromJson.parse(comment.updated));

            Long id = getCommentKey(comment.commentId, queryId);
            if (id != null) {
                updateComment(id, updated, comment.numberOfPlusoners);
                return;
            }

//            System.out.println("Storing Comment...");
            String authorKey
                    = storeAuthor(comment.userId, comment.userUrl, comment.userDisplayName, comment.userGender);
            String sql = "INSERT INTO gplus_comment "
                    + "(`comment_id`, `api_self_link`, `published`, `updated`, `plusoners`, `activity`, "
                    + "`content`, `language`, `gplus_post_id`, `gplus_user_id`, `query_id`) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
                    + "ON DUPLICATE KEY UPDATE updated=?, plusoners=?;";
            String published = sdfToSQL.format(sdfFromJson.parse(comment.published));

            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, comment.commentId);
            pStmt.setString(2, comment.commentSelfLink);
            pStmt.setString(3, published);
            pStmt.setString(4, updated);
            pStmt.setLong(5, comment.numberOfPlusoners);
            pStmt.setString(6, comment.activity);
            pStmt.setString(7, striped_content);
            pStmt.setString(8, CrawlerUtils.identifyLanguage(striped_content));
            pStmt.setLong(9, activityKey);
            pStmt.setString(10, authorKey);
            pStmt.setLong(11, queryId);
            pStmt.setString(12, updated);
            pStmt.setLong(13, comment.numberOfPlusoners);
            pStmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    public void updateComment(long id, String updated, long numberOfPlusoners) throws SQLException {
        String sql = "UPDATE gplus_comment SET updated = ?, plusoners = ? WHERE id = ?";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, updated);
            pStmt.setLong(2, numberOfPlusoners);
            pStmt.setLong(3, id);
            pStmt.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    public void updatePost(long id, String updated, long numberOfPlusoners, long numberOfReshares) throws SQLException {
        String sql = "UPDATE gplus_post SET updated = ?, plusoners = ?, reshares = ? WHERE id = ?";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, updated);
            pStmt.setLong(2, numberOfPlusoners);
            pStmt.setLong(3, numberOfReshares);
            pStmt.setLong(4, id);
            pStmt.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    public String storeAuthor(String authorId, String authorUrl, String authorDisplayName, String gender)
            throws SQLException {
        String sql = "INSERT INTO gplus_user (`user_id`, `url`, `name`, `gender`) VALUES (?, ?, ?, ?) "
                + "ON DUPLICATE KEY UPDATE name=?;";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, authorId);
            pStmt.setString(2, authorUrl);
            pStmt.setString(3, authorDisplayName);
            if (gender != null) {
                if (gender.equalsIgnoreCase("male") || gender.equalsIgnoreCase("female")) {
                    pStmt.setString(4, gender);
                } else {
                    pStmt.setNull(4, Types.VARCHAR);
                }
            } else {
                pStmt.setNull(4, Types.VARCHAR);
            }
            pStmt.setString(5, authorDisplayName);
            pStmt.executeUpdate();
            return getAuthorKey(authorId);
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    public String getAuthorKey(String authorId) throws SQLException {
        String id = "";
        String sql = "SELECT id FROM gplus_user WHERE user_id=?;";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, authorId);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            if (rSet.next()) {
                id = rSet.getString(1);
            }
            return id;
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
    }

    public Long getPostKey(String activityId, long queryId) throws SQLException {
        Long id = null;
        String sql = "SELECT id FROM gplus_post WHERE activity_id=? AND query_id=?;";
        Connection dbCon = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbCon = dataSource.getConnection();
            pStmt = dbCon.prepareStatement(sql);
            pStmt.setString(1, activityId);
            pStmt.setLong(2, queryId);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            if (rSet.next()) {
                id = rSet.getLong(1);
            }
        } finally {
            SQLUtils.release(dbCon, pStmt, rSet);
        }
        return id;
    }

    public Long getCommentKey(String commentId, long queryId) throws SQLException {
        Long id = null;
        String sql = "SELECT id FROM gplus_comment WHERE comment_id=? AND query_id=?;";
        Connection dbCon = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbCon = dataSource.getConnection();
            pStmt = dbCon.prepareStatement(sql);
            pStmt.setString(1, commentId);
            pStmt.setLong(2, queryId);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            if (rSet.next()) {
                id = rSet.getLong(1);
            }
        } finally {
            SQLUtils.release(dbCon, pStmt, rSet);
        }
        return id;
    }

    /**
     * Will store the gender in the demographics table for the supplied
     * content_id, if the user that posted / commented has provided it.
     *
     * @param gplus_user_id the unique ID of the user
     * @param url the url of the post/comment
     * @param timestamp the timestamp
     * @param sourceTable the table it originates from
     * @param queryId the query_id it comes from
     * @throws SQLException
     */
    private void storeGender(int gplus_user_id, String url, String timestamp, String sourceTable, long queryId)
            throws SQLException {
        long nomadContentId
                = CrawlerDBA.getContentID(dataSource, url, timestamp, sourceTable, queryId);
        if (nomadContentId != 0) {
            String gender = null;
            String sql = "SELECT gender FROM gplus_user WHERE id = ?;";
            Connection dbCon = null;
            PreparedStatement pStmt = null;
            ResultSet rSet = null;
            try {
                dbCon = dataSource.getConnection();
                pStmt = dbCon.prepareStatement(sql);
                pStmt.setLong(1, gplus_user_id);
                rSet = pStmt.executeQuery();
                if (rSet.next()) {
                    gender = rSet.getString(1);
                }
                if (gender != null) {
                    demogStore.insertGender(nomadContentId, gender, Boolean.TRUE);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                SQLUtils.release(dbCon, pStmt, rSet);
            }
        }
    }

    /**
     * Will load #max_activities, #max_comments from the activities, comments
     * tables and store the results to general repository
     *
     * @param crawl_id the unique ID of the crawl
     * @param queryId the query ID that the content was fetched for
     * @param tableName the source table (post/comment)
     * @return
     */
    public int moveContent(long crawl_id, long queryId, String tableName) {
        int cntRes = 0;
        String sql = "SELECT id, content, updated, language, ";
        sql
                += tableName.equals(GplusCrawler.TBL_GPLUS_POST)
                ? "content_url"
                : "api_self_link";
        sql += ", gplus_user_id FROM " + tableName + " WHERE query_id = ? ORDER BY id DESC LIMIT ?;";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setLong(1, queryId);
            pStmt.setInt(2, tableName.equals(GplusCrawler.TBL_GPLUS_POST) ? max_activities : max_comments);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            while (rSet.next()) {
                //long sourceId = resultSet.getLong("id");
                String url;
                if (tableName.equals(GplusCrawler.TBL_GPLUS_POST)) {
                    url = rSet.getString("content_url");
                } else {
                    url = rSet.getString("api_self_link");
                }
                String clean_text = rSet.getString("content");
                String timestamp = rSet.getString("updated");
                String language = rSet.getString("language");
                int gplus_user_id = rSet.getInt("gplus_user_id");
                try {
                    cntRes
                            += CrawlerDBA.storeContent(dataSource, url,
                                    clean_text, timestamp, language, tableName, queryId, crawl_id);
                    // store gender in nomad_demographics
                    storeGender(gplus_user_id, url, timestamp, tableName, queryId);
                } catch (ParseException e) {
                    e.printStackTrace();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
        return cntRes;
    }
}
