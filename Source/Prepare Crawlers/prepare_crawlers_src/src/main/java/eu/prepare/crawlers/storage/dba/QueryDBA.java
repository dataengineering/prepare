/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.storage.dba;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import eu.prepare.crawlers.controller.services.CrawlService;
import eu.prepare.crawlers.impl.Crawler;
import eu.prepare.crawlers.storage.util.SQLUtils;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.utils.response.IResponse;
import eu.prepare.crawlers.utils.response.InsertResponse;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class QueryDBA extends AbstractDBA {

    private final Integer iMax;

    public static final String QUERY = "query_term";
    public static final String LANG_ID = "language_id";

    /**
     * Default Constructor. Will fetch maximum {@link Crawl.MAX_QUERIES}
     * queries.
     *
     * @param datasource
     */
    public QueryDBA(DataSource datasource) {
        super(datasource);
        this.iMax = CrawlService.MAX_QUERIES;
    }

    /**
     * Default Constructor.
     *
     * @param datasource
     * @param iMaxQueriesToFetch max queries to fetch
     */
    public QueryDBA(DataSource datasource, Integer iMaxQueriesToFetch) {
        super(datasource);
        this.iMax = iMaxQueriesToFetch;
    }

    /**
     * @return a list of queries for crawling
     * @throws java.sql.SQLException
     */
    public Set<PQuery> fetchQueries() throws SQLException {
        // if queries will be fetched
        Set<PQuery> hsQueries = new HashSet<PQuery>();
        String SQLSelect = "SELECT id, query_term, language_id FROM query ORDER BY num_of_executions ASC LIMIT ?;";
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            dbConnection = datasource.getConnection();
            preparedStatement = dbConnection.prepareStatement(SQLSelect);
            preparedStatement.setInt(1, iMax);
            preparedStatement.execute();
            resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                long queryId = resultSet.getLong(1);
                String query = resultSet.getString(2);
                int language_id = resultSet.getInt(3);
                // add to the return queries
                hsQueries.add(new PQuery(queryId, query.trim(), language_id));
            }
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, resultSet);
        }
        return hsQueries;
    }

    public void insertQueries(String[] queries) throws SQLException {
        String SQLINSERT = "INSERT IGNORE INTO query(query_term, language_id, num_of_executions) "
                + "VALUES (?, ?, ?);";
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            dbConnection = datasource.getConnection();
            for (String query_term : queries) {
                preparedStatement = dbConnection.prepareStatement(SQLINSERT);
                preparedStatement.setString(1, query_term);
                // set language_id 2=en
                preparedStatement.setInt(2, 2);
                // set 0 executions
                preparedStatement.setInt(3, 0);
                int iRes = preparedStatement.executeUpdate();
                if (iRes > 0) {
                    System.out.println("Inserted query_term: " + query_term);
                }
            }
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, null);
        }
    }

    /**
     *
     * @param queries
     * @return
     */
    @Override
    public String insertQueries(List<Map<Object, Object>> queries) {
        List<IResponse> lRes = new ArrayList<IResponse>();
        String SQLINSERT = "INSERT IGNORE INTO query(query_term, language_id, num_of_executions) "
                + "VALUES (?, ?, ?);";
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        String query_term = "";
        try {
            dbConnection = datasource.getConnection();
            for (Map queryMap : queries) {
                query_term = (String) queryMap.get(QUERY);
                Double lang_id = (Double) queryMap.get(LANG_ID);
                preparedStatement = dbConnection.prepareStatement(SQLINSERT);
                preparedStatement.setString(1, query_term);
                // set language_id 2=en
                preparedStatement.setInt(2, lang_id.intValue());
                // set 0 executions
                preparedStatement.setInt(3, 0);
                int iRes = preparedStatement.executeUpdate();
                IResponse ir;
                if (iRes == 1) {
                    System.out.println("Inserted query_term: " + query_term);
                    ir = new InsertResponse(query_term, RES_OK);
                    lRes.add(ir);
                } else if (iRes == 0) {
                    ir = new InsertResponse(query_term, RES_EXISTS);
                    lRes.add(ir);
                }
            }
        } catch (ClassCastException ex) {
            Logger.getLogger(QueryDBA.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
            IResponse ir = new InsertResponse(query_term, RES_ERROR + ex.toString());
            lRes.add(ir);
        } catch (SQLException ex) {
            Logger.getLogger(QueryDBA.class.getName()).log(Level.SEVERE, null, ex);
            IResponse ir = new InsertResponse(query_term, RES_ERROR + ex.toString());
            lRes.add(ir);
        } catch (Exception ex) {
            Logger.getLogger(QueryDBA.class.getName()).log(Level.SEVERE, null, ex);
            IResponse ir = new InsertResponse(query_term, RES_ERROR + ex.toString());
            lRes.add(ir);
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, null);
        }
        return new Gson().toJson(lRes, List.class);
    }

    public Set<PQuery> fetchQueriesFromList(String query_list) {
        Set<PQuery> qRes = new HashSet<PQuery>();
        Type listType = new TypeToken<List<String>>() {
        }.getType();
        // get list of queries
        List<String> queries = new Gson().fromJson(query_list, listType);
        int iTermCnt = queries.size();
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rSet = null;
        try {
            dbConnection = datasource.getConnection();
            for (String query_term : queries) {
                String SQL_SELECT = "SELECT id, query_term, language_id FROM query WHERE query_term = ?;";
                preparedStatement = dbConnection.prepareStatement(SQL_SELECT);
                preparedStatement.setString(1, query_term);
                rSet = preparedStatement.executeQuery();
                if (rSet.next()) {
                    long id = rSet.getLong(1);
                    String q_term = rSet.getString(2);
                    int lang_id = rSet.getInt(3);

                    qRes.add(new PQuery(id, q_term, lang_id));
                } else {
                    Logger.getLogger(QueryDBA.class.getName()).log(Level.WARNING, String.format("'%s' does not exist in DB, omitting%n", query_term));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(QueryDBA.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, rSet);
        }
        if (qRes.isEmpty()) {
            throw new IllegalArgumentException("No term exists in DB, Please use terms that already exist in DB");
        }
        return qRes;
    }

    public static void updateQueryExecTimes(DataSource dataSource, long queryId) {
        Connection dbCon = null;
        PreparedStatement preparedStatement = null;
        try {
            dbCon = dataSource.getConnection();
            String sql = "UPDATE query SET num_of_executions=num_of_executions+1 WHERE id=?;";
            preparedStatement = dbCon.prepareStatement(sql);
            preparedStatement.setLong(1, queryId);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbCon, preparedStatement, null);
        }
    }

    @Override
    public String insertSources(EnumMap<Crawler, List<Map<Object, Object>>> sources) {
        throw new UnsupportedOperationException("Not supported");
    }
}
