/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.storage.demographics;

import eu.prepare.crawlers.storage.util.SQLUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import javax.sql.DataSource;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class DemographicsDB {

    private DataSource dataSource;
    private final Set<Integer> validAgeIDs;
    private final Set<Integer> validGenderIDs;
    private final Set<Integer> validEducationIDs;
    private DemoUtils utils;

    public DemographicsDB(DataSource dataSourceArg) {
        this.dataSource = dataSourceArg;
        // get utils class
        this.utils = new DemoUtils(dataSource);
        // get valid IDs from the 'age', 'gender', 'education' tables
        this.validAgeIDs = utils.getValidAgeIDs();
        this.validGenderIDs = utils.getValidGenderIDs();
        this.validEducationIDs = utils.getValidEducationIDs();
    }

    /**
     * Will check for an existing age value for the supplied content_id, and if
     * not there it will insert it, else it will update it (replacing the old
     * entry).
     * <i>Used only by crawlers: youtube</i>
     *
     * @param content_id The content_id to insert the value for
     * @param age The age to insert.
     * @param bUpdate true if you want to update an existing value.
     *
     * <p>
     * <b>Ages are currently represented by these IDs</b><p>
     * 1: 18 to 24
     * <p>
     * 2: 25 to 34
     * <p>
     * 3: 35 to 44
     * <p>
     * 4: 45 to 54
     * <p>
     * 5: 55 to 64
     * <p>
     * So if the value 20 is passed to the method, the number 1 will be added to
     * the age table for this content_id
     * @throws SQLException
     */
    public void insertAge(long content_id, int age, boolean bUpdate)
            throws SQLException {
        // check if age is in a valid range
        int age_id = utils.translatetoAgeID(age);
        // if age is valid
        if (age_id != -1) {
            // check if there is an entry for that content_id
            int[] iEntry = hasEntry(content_id);
            // if an entry exists for that id
            if (!utils.isEmptyRow(iEntry)) {
                // if user wants to update with new value
                if (bUpdate) {
                    // update (will replace existing value)
                    updateAgeID(content_id, age_id, true);
                } // else pass
                // if new entry
            } else {
                // insert it
                insertNewAgeID(content_id, age_id);
            }
        } else {
            System.err.println("Error: Age " + age + " does not belong in a valid age range in age table");
        }
    }

    /**
     * Insert a new age_id extracted from demographics extractor. If age_id
     * exists for that content_id entry, it will ignore the entry if bUpdate is
     * false.
     *
     * @param content_id
     * @param age_id
     * @param bUpdateRow
     * @throws java.sql.SQLException
     */
    public void insertAgeID(long content_id, int age_id, boolean bUpdateRow) throws SQLException {

        // first check if it has already an insert registered for that  content id
        int[] iEntry = hasEntry(content_id);
        // if age_id is valid
        if (validAgeIDs.contains(age_id)) {
            // if exists an entry for that content_id
            if (!utils.isEmptyRow(iEntry)) {
                // check if it contains age_id
                boolean iAge = (iEntry[1] != 0);
                // if not an age value (has smth other)
                if (!iAge) {
                    if (bUpdateRow) {
                        updateAgeID(content_id, age_id, false); // will update row (add a new column entry)
                    } // if not contains

                }
                // if this is a new entry
            } else {
                // perform a regular insert
                insertNewAgeID(content_id, age_id);
            }
        } else {
            System.err.println("Error: " + age_id + " is not a valid age_id");
        }
    }

    /**
     * Will insert a new gender_id that links to the supplied content_id.
     * <i>Used only by crawlers: youtube, facebook</i>
     *
     * @param content_id The content id that this value belongs to
     * @param gender the string representing the gender (male, female). The ID
     * that will be inserted in gender is 1 or 2 (1means male, 2 means female).
     * @param bUpdate true if you want to update an existing value.
     *
     * @throws SQLException
     */
    public void insertGender(long content_id, String gender, boolean bUpdate) throws SQLException {
        // check if gender is in a valid range
        int gender_id = utils.translatetoGenderID(gender);
        // if age is valid
        if (gender_id != -1) {
            // check if there is an entry for that content_id
            int[] iEntry = hasEntry(content_id);
            // if an entry exists for that id
            if (!utils.isEmptyRow(iEntry)) {
                // if user wants to update with new value
                if (bUpdate) {
                    // update
                    updateGenderID(content_id, gender_id, true);
                } // else pass
                // if new entry
            } else {
                // insert it
                insertNewGenderID(content_id, gender_id);
            }
        } else {
            System.err.println("Error: " + gender + " does not belong in a valid gender range");
        }
    }

    /**
     * Will insert the supplied gender_id, only if the row does not already have
     * a gender_id for the supplied content . Used only by demographics
     * extractor.
     *
     * @param content_id the content_id relevant
     * @param gender_id the gender_id to insert for the specified content_id.
     * @param bUpdate true if you want to update an existing value.
     */
    public void insertGenderID(Long content_id, int gender_id, boolean bUpdate) throws SQLException {
        // first check if it has already an age registered for that  content id
        int[] iEntry = hasEntry(content_id);
        // if gender_id is valid
        if (validGenderIDs.contains(gender_id)) {
            // if exists an entry for that content_id
            if (!utils.isEmptyRow(iEntry)) {
                // check if it contains age_id
                boolean iGender = (iEntry[2] != 0);
                if (!iGender) { // will add new entry, only if it is not gender.
                    if (bUpdate) {
                        updateGenderID(content_id, gender_id, false);
                    }
                }
                // if this is a new entry
            } else {
                // perform a regular insert
                insertNewGenderID(content_id, gender_id);
            }
        } else {
            System.err.println("Error: " + gender_id + " is not in a valid range.");
        }
    }

    /**
     * Will add or update an education_id value for the supplied content_id.
     * Used only by demographics extractor.
     *
     * @param content_id The ID to insert for
     * @param education_id the education ID:
     * <p>
     * 1: No College<p>
     * 2: Some College<p>
     * 3: Graduate School<p>
     * 4: College<p>
     * @param bUpdate true if you want to update an existing row. (Will not
     * change the value of existing entry)
     * @throws SQLException
     */
    public void insertEducationID(long content_id, int education_id, boolean bUpdate) throws SQLException {
        // check existance for content_id
        int[] iExists = hasEntry(content_id);
        // if education_id is valid (exists in relevant table)
        if (validEducationIDs.contains(education_id)) {
            // if no entry for this content _ID
            if (utils.isEmptyRow(iExists)) {
                insertNewEducationID(education_id, content_id);
            } else {
                // if already exists
                // check if education exists in there
                boolean iEdu = (iExists[3] != 0);
                // if it has not an education entry.
                if (!iEdu) {
                    if (bUpdate) { // if user wants to update
                        // add it
                        updateEducationID(content_id, education_id, false); // update row, do not replace existing value
                    }
                }
            }
        }
    }

    /**
     * <b>NOT YET SUPPORTED</b>
     * <p>
     * Intended to be used by crawlers.
     *
     * @param content_id The content_id
     * @param education The education category.
     */
    public void insertEducation(long content_id, String education) {
        throw new UnsupportedOperationException("Not Yet Supported. use \'insertEducationID\'");
    }

    /**
     * Will look in location_description for the supplied location string, and
     * if it does not already exist, it will create a new entry in
     * location_description and location. Furthermore, it will check for
     * location_id entry for that content_id in demographics, and if it does not
     * exist it will insert it, else it will update it, if user wants to
     * <i>Used only by crawlers: youtube, facebook, twitter</i>
     *
     * @param content_id The ID to insert for
     * @param location The location to insert, represented by an auto generated
     * ID
     * @throws SQLException
     */
    public void insertLocation(long content_id, String location, boolean bUpdate) throws SQLException {
        // trim location
        location = location.trim();
        // check if location exists in location_description.
        int iLocationNameID = existsLocationInDescription(location.trim());
        if (iLocationNameID == -1) { // if not there
            // add new entry and take location_id
            iLocationNameID = insertLocationNameAndDescription(location, location, 2); //FIX ME (use country/region lists of some kind?)
        }
        // check if existing entry for that content_id
        int[] iExistID = hasEntry(content_id);
        // if not there
        if (utils.isEmptyRow(iExistID)) {
            // insert new row
            insertNewLocationID(iLocationNameID, content_id);
        } else {
            if (bUpdate) {
                // update with new location ID
                updateLocationID(content_id, iLocationNameID);
            } // else ignore
        }
    }

/////////////////////////////// PRIVATE MEMBERS //////////////////////////////////
    /**
     * Will return an int[] containing the values as they are stored in the
     * columns of the demographics table. The values will be : [location_id,
     * age_id, gender_id, education_id], or a zero length int array if no entry
     * is entered for the supplied content_id
     *
     * @param content_id The specific ID to look upon
     * @return the the int[] of values if there is an entry in demographics for
     * this ID
     * @throws SQLException
     */
    private int[] hasEntry(long content_id) throws SQLException {

        int[] iEntry = new int[]{0, 0, 0, 0};
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        // check if specific content row is already there
        try {

            String sql
                    = "SELECT * FROM demographics WHERE content_id = ? LIMIT 1;";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setLong(1, content_id);
            pStmt.execute();

            resultSet = pStmt.getResultSet();

            if (resultSet.next()) {
                iEntry[0] = resultSet.getInt("location_id");
                iEntry[1] = resultSet.getInt("age_id");
                iEntry[2] = resultSet.getInt("gender_id");
                iEntry[3] = resultSet.getInt("education_id");
            }

        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
        return iEntry;
    }

    private void insertNewAgeID(long content_id, int age_id) throws SQLException {

        Connection dbConnection = null;
        PreparedStatement pStmt = null;

        try {
            String sql = "INSERT INTO demographics(age_id, content_id) VALUES (?, ?);";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);

            pStmt.setInt(1, age_id);
            pStmt.setLong(2, content_id);

            pStmt.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    /**
     * Will update the age_id in the specified row
     *
     * @param content_id The demographicsid value
     * @param age_id the age to update
     * @param bReplaceValue true if you want the existing value replaced, or
     * false to perform an update
     * @throws SQLException
     */
    private void updateAgeID(long content_id, int age_id, boolean bReplaceValue) throws SQLException {

        PreparedStatement pStmt = null;
        Connection dbConnection = dataSource.getConnection();
        String sql;
        try {
            if (bReplaceValue) { // replace already existing value
                sql = "INSERT INTO demographics(age_id, content_id) VALUES (?, ?)"
                        + " ON DUPLICATE KEY UPDATE age_id=?;";
                pStmt = dbConnection.prepareStatement(sql);
                // insert values
                pStmt.setInt(1, age_id);
                pStmt.setLong(2, content_id);
                pStmt.setInt(3, age_id);
                pStmt.executeUpdate();
            } else { // update row (this value should not exist in this row)
                sql = "UPDATE demographics SET age_id = ? WHERE content_id = ?";
                pStmt = dbConnection.prepareStatement(sql);
                // insert values
                pStmt.setInt(1, age_id);
                pStmt.setLong(2, content_id);
                pStmt.executeUpdate();
            }
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    private void insertNewGenderID(long content_id, int gender_id) throws SQLException {

        Connection dbConnection = null;
        PreparedStatement pStmt = null;

        try {
            String sql = "INSERT INTO demographics(gender_id, content_id) VALUES (?, ?);";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);

            pStmt.setInt(1, gender_id);
            pStmt.setLong(2, content_id);

            pStmt.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    /**
     * Will update the gender_id in the specified row
     *
     * @param content_id The demographicsid value
     * @param gender_id the gender_id to update
     * @param bReplaceValue if true, will replace the existing value. If false,
     * will update the row with the value (the column should be empty)
     * @throws SQLException
     */
    private void updateGenderID(long content_id, int gender_id, boolean bReplaceValue) throws SQLException {
        PreparedStatement pStmt = null;
        Connection dbConnection = dataSource.getConnection();
        String sql;
        try {
            if (bReplaceValue) { // replace already existing value
                sql = "INSERT INTO demographics(gender_id, content_id) VALUES (?, ?)"
                        + " ON DUPLICATE KEY UPDATE gender_id=?;";
                pStmt = dbConnection.prepareStatement(sql);
                // insert values
                pStmt.setInt(1, gender_id);
                pStmt.setLong(2, content_id);
                pStmt.setInt(3, gender_id);
                pStmt.executeUpdate();
            } else { // update row (this value should not exist in this row)
                sql = "UPDATE demographics SET gender_id = ? WHERE content_id = ?";
                pStmt = dbConnection.prepareStatement(sql);
                // insert values
                pStmt.setInt(1, gender_id);
                pStmt.setLong(2, content_id);
                pStmt.executeUpdate();
            }
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    private void insertNewEducationID(int education_id, long content_id) throws SQLException {
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {

            String sql = "INSERT INTO demographics(education_id, content_id) VALUES (?, ?);";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);

            pStmt.setInt(1, education_id);
            pStmt.setLong(2, content_id);

            pStmt.executeUpdate();

        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }

    }

    private void updateEducationID(long content_id, int education_id, boolean bReplaceValue) throws SQLException {
        Connection dbConnection = dataSource.getConnection();
        PreparedStatement pStmt = null;
        String sql;
        try {
            if (bReplaceValue) { // replace already existing value
                sql = "INSERT INTO demographics(education_id, content_id) VALUES (?, ?)"
                        + " ON DUPLICATE KEY UPDATE education_id=?;";
                pStmt = dbConnection.prepareStatement(sql);
                // insert values
                pStmt.setInt(1, education_id);
                pStmt.setLong(2, content_id);
                pStmt.setInt(3, education_id);
                pStmt.executeUpdate();
            } else { // update row (this value should not exist in this row)
                sql = "UPDATE demographics SET education_id = ? WHERE content_id = ?";
                pStmt = dbConnection.prepareStatement(sql);
                // insert values
                pStmt.setInt(1, education_id);
                pStmt.setLong(2, content_id);
                pStmt.executeUpdate();
            }
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    /**
     *
     * @param regionName The string to look upon, e.g. "GR" or "EN", or
     * "Brooklyn" (?), etc.
     * @return the location_id of the location_description table, if the
     * regionName exists, else -1. The value returned can then be inserted into
     * demographics .
     */
    private int existsLocationInDescription(String regionName) throws SQLException {

        int iNameExists = -1;
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        try {

            String sql
                    = "SELECT location_id FROM location_description "
                    + "WHERE LOWER(region_name) LIKE LOWER(?) LIMIT 1;"; // ? = "GR" or "EN"
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);

            pStmt.setString(1, regionName);
            pStmt.execute();

            resultSet = pStmt.getResultSet();

            if (resultSet.next()) {
                iNameExists = resultSet.getInt("location_id");
            }

            return iNameExists;
        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
    }

    /**
     * Will insert a new location in location table, as well as a new
     * location_description entry.
     *
     * @param canonicalName The canonical name, e.g. 'Greece'
     * @param regionName The region name, e.g. "GR"
     * @param language_id The language_id, e.g. for English 2
     * @return The ID of the newly inserted row.
     * @throws SQLException
     */
    private int insertLocationNameAndDescription(String canonicalName, String regionName, int language_id)
            throws SQLException {
        int iLastID = insertLocationName(canonicalName);
        insertDescriptionLocationRelation(language_id, iLastID, regionName);
        return iLastID;
    }

    private int insertLocationName(String canonicalName) throws SQLException {
        int iLastID = -1;
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            String sql = "INSERT INTO location(canonical_string) VALUES (?);";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pStmt.setString(1, canonicalName);
            pStmt.executeUpdate();
            // get ID
            rs = pStmt.getGeneratedKeys();
            if (rs.next()) {
                iLastID = rs.getInt(1); // last insert ID
            }
            return iLastID;
        } finally {
            SQLUtils.release(dbConnection, pStmt, rs);
        }
    }

    private void insertDescriptionLocationRelation(int language_id, int iLastID, String regionName) throws SQLException {
        Connection dbConn = null;
        PreparedStatement insertToDescription = null;
        try {
            String sql = "INSERT INTO location_description(language_id, location_id, region_name) VALUES(?,?,?);";

            dbConn = dataSource.getConnection();
            insertToDescription = dbConn.prepareStatement(sql);
            insertToDescription.setInt(1, language_id);
            insertToDescription.setInt(2, iLastID);
            insertToDescription.setString(3, regionName);

            insertToDescription.executeUpdate();
        } finally {
            SQLUtils.release(dbConn, insertToDescription, null);
        }
    }

    /**
     * Will update an existing entry in demographics with the new value of
     * location_id
     *
     * @param id The demographics id value
     * @param location_id the id to update
     * @throws SQLException
     */
    private void updateLocationID(long content_id, int location_id) throws SQLException {

        Connection dbConnection = null;
        PreparedStatement pStmt = null;

        try {

            String sql = "INSERT INTO demographics(location_id, content_id) VALUES (?, ?) "
                    + "ON DUPLICATE KEY UPDATE location_id=?;";

            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            // insert values
            pStmt.setInt(1, location_id);
            pStmt.setLong(2, content_id);
            pStmt.setInt(3, location_id);
            pStmt.executeUpdate();

        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    /**
     *
     * @param location_id the id that represents a location (through the
     * location table)
     * @param content_id the content_id relevant.
     * @throws SQLException
     */
    private void insertNewLocationID(int location_id, long content_id) throws SQLException {
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {
            String sql = "INSERT INTO demographics(location_id, content_id) VALUES (?, ?);";

            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);

            pStmt.setInt(1, location_id);
            pStmt.setLong(2, content_id);

            pStmt.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }
}
