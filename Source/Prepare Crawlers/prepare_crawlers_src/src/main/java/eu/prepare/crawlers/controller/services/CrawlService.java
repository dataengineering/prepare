package eu.prepare.crawlers.controller.services;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.prepare.crawlers.controller.crawl.AbstractCrawlerController;
import eu.prepare.crawlers.controller.crawl.DefaultCrawlerController;
import eu.prepare.crawlers.storage.dba.CrawlerDBA;
import eu.prepare.crawlers.storage.dba.QueryDBA;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.utils.Configuration;
import eu.prepare.crawlers.utils.CrawlerUtils;
import eu.prepare.crawlers.utils.response.CrawlResult;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Servlet implementation class CrawlService
 *
 * <br>-----------------------------------------------------------------------
 * <br><b>EXAMPLE CALL: CRAWL</b>
 * <b>
 * http://localhost:8080/PrepareCrawlers/Crawl?bing=true&gplus=true&youtube=true&twitter=true&max_queries=5
 * <b>it will crawl for the latest 5 queries inserted in the db.query table
 * <br>if max_queries param is not supplied, it will get value of 100
 * <br>
 * <br>CrawlService with query list (QUERIES MUST EXIST IN DB)
 * <br>EXAMPLE CALL:
 * <br>
 * http://localhost:8080/PrepareCrawlers/Crawl?bing=true&gplus=true&youtube=true&twitter=true
 * &query_list=["Quantum entanglement", "CERN
 * Institute"]&crawl_name=CustomNameOfCrawl
 * <br>it will crawl for the queries passed and respond with : SEE END OF
 * CURRENT FILE.
 * <br><br>
 * NOTE: When invoking crawl with the queries list, note that the queries MUST
 * exist in DB. (see eu.prepare.crawlers.controller.services.InsertQuery)
 * <br> if crawl_name is a new one (i.e. a crawl setup does not exist in DB),
 * then the JSON response will have cumulative statistics for all the previous
 * crawlers. Only for the first run of the crawler, for consecutive runs the
 * response will hold values from the current run.
 * -----------------------------------------------------------------------
 *
 * @author George K. <gkiom@iit.demokritos.gr>
 */
@WebServlet(urlPatterns = {"/Crawl"})
public class CrawlService extends HttpServlet {

    public static final Logger LOGGER = Logger.getLogger(CrawlService.class.getName());

    private static final long serialVersionUID = 1L;
    private final String PROPERTIES = "crawler.properties";
    private QueryDBA query_storage;
    public static final int MAX_QUERIES = 100;
    private static volatile DataSource dataSource;

    /**
     * @param request the request to handle
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(final HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");

        DataSource ds = getDS();

        PrintStream out = null;
        // init configuration
        ServletContext servletContext = getServletContext();
        String workingDir = servletContext.getRealPath("/").endsWith("/") ? servletContext.getRealPath("/") + "WEB-INF/"
                : servletContext.getRealPath("/") + "/WEB-INF/";
        final Configuration configuration = new Configuration(workingDir + PROPERTIES);
        configuration.setWorkingDir(workingDir);
        // initialize utilities
        CrawlerUtils.initialiseUtils(workingDir);
        // get max queries for user
        String sMaxQueries = request.getParameter("max_queries");
        Integer iMaxQueries;
        if (sMaxQueries != null) {
            iMaxQueries = Integer.parseInt(sMaxQueries);
        } else {
            iMaxQueries = MAX_QUERIES;
        }
        // log start timestamp of crawl
        Date start = new Date();
        // get crawl_name, if there
        String crawl_name = request.getParameter("crawl_name");
        if (crawl_name == null) {
            crawl_name = "crawl_".concat(String.valueOf(start.getTime()));
        }
        final Set<PQuery> hsQueries;

        try {
            out = new PrintStream(response.getOutputStream());
            // init a storage instance
            query_storage = new QueryDBA(ds, iMaxQueries);
            String query_list = request.getParameter("query_list");
            // if not provided a query_list
            if (query_list == null) {
                // get the queries for the specified params
                hsQueries = query_storage.fetchQueries();
                // reset storage (no further use)
                query_storage = null;
            } else { // if user wants to crawl on specific queries
                hsQueries = query_storage.fetchQueriesFromList(query_list);
                query_storage = null;
            }
            int iQueryCnt = 1;
            int iTotal = hsQueries.size();
            if (iTotal == 0) {
                String no_queries = "{\"Error\": \"No queries passed for crawling... Aborting\"}";
                out.print(no_queries);
                out.close();
                throw new IllegalArgumentException("No queries passed for crawling... Aborting");
            }

            // get query crawl time limit from system configuration. 
            int query_time_limit = configuration.getQueryTimeLimit();
            // crawl results counter
            int iCrawlResults = 0;
            // register crawl
            long crawl_id = CrawlerDBA.crawlInitiated(ds, crawl_name, start);
            LOGGER.log(Level.INFO, "<CrawlID {0}> Initialized at {1}", new Object[]{crawl_id, start});
            //----- INITIALIZE CRAWLER CONTROLLER ----- //
            AbstractCrawlerController crawl_controller
                    = new DefaultCrawlerController(ds, crawl_name, crawl_id);
            // set needed crawl services to execute at runtime
            crawl_controller.setCrawlerImpls(request.getParameterMap(), configuration);
            // -----                            ----- //
            // check if this crawler runs for the first time
            boolean isFirstCrawl = CrawlerDBA.isFirstCrawl(ds, crawl_name);
            List<CrawlResult> previous_stats = new ArrayList<CrawlResult>();
            // if so, BEFORE invoking crawling, fetch previous results for crawler terms
            if (isFirstCrawl) {
                previous_stats = CrawlerDBA.getExistingStatistics(ds, hsQueries);
            }
            // for each query
            for (PQuery eachCand : hsQueries) {
                LOGGER.log(Level.INFO,
                        "<CrawlID {0}>: Starting Query: ''{1}'' ({2} of {3}) at {4}...",
                        new Object[]{crawl_id, eachCand.getQueryTerm(), iQueryCnt++, iTotal, new Date()});
                // perform crawl
                iCrawlResults
                        += crawl_controller
                        .executeCrawl(eachCand, query_time_limit);
            }
            // crawl finished
            Date ended = new Date();
            CrawlerDBA.crawlFinished(ds, crawl_id, ended, iCrawlResults);
            // get JSON report of the current crawl
            String verbose = CrawlerDBA.getJSONReport(ds, crawl_controller, previous_stats);
            // respond json
            out.print(verbose);
            LOGGER.log(Level.INFO, "<CrawlID {0}> Finalized at {1}, total items: {2}, total duration: {3} secs ",
                    new Object[]{
                        crawl_id,
                        ended,
                        iCrawlResults,
                        (TimeUnit.MILLISECONDS.toSeconds(ended.getTime()) - TimeUnit.MILLISECONDS.toSeconds(start.getTime()))
                    }
            );
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            String error = String.format("{\"Error\": \"%s\"}", ex.toString());
            if (out != null) {
                out.print(error);
            }
        } catch (IllegalArgumentException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            String error = String.format("{\"Error\": \"%s\"}", ex.toString());
            if (out != null) {
                out.print(error);
            }
        } finally {
            if (out != null) {
                out.close();
            }
            ds = null;
        }
    }

    public synchronized static DataSource getDS() {
        if (dataSource == null) {
            // inject datasource connection
            Context initContext;
            try {
                initContext = new InitialContext();
                Context envContext = (Context) initContext.lookup("java:/comp/env");
                dataSource = (DataSource) envContext.lookup("jdbc/prepare_crawl");
            } catch (NamingException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
            }
        }
        return dataSource;
    }
}
// JSON response
//{
//
//    "crawl_id": 33,
//    "crawl_response": {
//        "name": "crawl_test_1",
//        "crawl_results": [
//            {
//                "term": "Quantum entanglement",
//                "results": [
//                    {
//                        "crawler": "BingCrawler",
//                        "items": 5
//                    },
//                    {
//                        "crawler": "GplusCrawler",
//                        "items": 0
//                    },
//                    {
//                        "crawler": "YouTubeCrawler",
//                        "items": 9
//                    }
//                ]
//            },
//            {
//                "term": "CERN Institute",
//                "results": [
//                    {
//                        "crawler": "BingCrawler",
//                        "items": 6
//                    },
//                    {
//                        "crawler": "GplusCrawler",
//                        "items": 3
//                    },
//                    {
//                        "crawler": "YouTubeCrawler",
//                        "items": 3
//                    }
//                ]
//            }
//        ]
//    }
//
//}
