/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.utils.response;

import com.google.gson.Gson;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class InsertResponse implements IResponse {

    private String term;
    private String status;

    public InsertResponse(String term, String status) {
        this.term = term;
        this.status = status;
    }

    @Override
    public String toJSON() {
        return new Gson().toJson(this);
    }

}
