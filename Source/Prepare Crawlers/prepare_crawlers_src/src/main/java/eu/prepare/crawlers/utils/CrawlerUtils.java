package eu.prepare.crawlers.utils;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;
import de.l3s.boilerpipe.BoilerpipeExtractor;
import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.document.TextBlock;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.sax.BoilerpipeSAXInput;
import de.l3s.boilerpipe.sax.HTMLDocument;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author George K. <gkiom@iit.demokritos.gr>
 */
public class CrawlerUtils {

    static String workingDir = "";
    static int JAVA_VERSION_CODE = Integer.parseInt(System.getProperty("java.specification.version").substring(2, 3));

    private static HttpClient CRAWLER_HTTP_CLIENT;

    public static void initialiseUtils(String setWorkingDir) {
        workingDir = setWorkingDir;
    }

    public synchronized static String identifyLanguage(String text) {
        String language = "en";
        try {// detect the language of the given text
            DetectorFactory.loadProfile(workingDir + "profiles");
            Detector detector = DetectorFactory.create();
            detector.append(text);
            language = detector.detect();
        } catch (LangDetectException e) {
            if (!e.getMessage().equals("no features in text")) {
                e.printStackTrace();
                DetectorFactory.clear();
            }
        }
        DetectorFactory.clear();
        return language;
    }

    /**
     *
     * @param sContent The content to check whether it will be stored or not
     * @param sQuery The query to match against the content
     * @param iMaxWords the mininum word threshold that the content must be
     * comprised of
     * @return true if the content should be stored to the DB
     */
    public synchronized static boolean shouldStore(String sContent, String sQuery, int iMaxWords) {
        // if sContent null or empty, false
        if (sContent == null || sContent.trim().isEmpty()) {
            return false;
        }
        // if content shorter than max words, abort
        if (sContent.replaceAll("[;:&!,. ]", " ").split("\\s+").length < iMaxWords) {
            return false;
        }
        if (JAVA_VERSION_CODE == 6) {
            return containsQuery16(sContent, sQuery);
        } else {
            return containsQuery17(sContent, sQuery);
        }
    }

    /**
     *
     * @param text the text to check
     * @param query the query to compare against the text
     * @return true if the text contains the query
     * <b>Requires Java 1.6 (uses normalization)</b>
     */
    public static boolean containsQuery16(String text, String query) {

        // Split query into it's contents
        List<String> queryTokens = splitQuery(query);

        String normalizedText = Normalizer.normalize(text, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
                .toLowerCase()
                .trim();

        for (String queryToken : queryTokens) {
            if (!queryToken.isEmpty()) {
                String normalizedQueryToken = Normalizer.normalize(queryToken, Normalizer.Form.NFD)
                        .replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
                        .toLowerCase()
                        .trim();
                if (!normalizedText.contains(normalizedQueryToken)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     *
     * @param text the text to check
     * @param query the query to compare against the text
     * @return true if the text contains the query
     * <b>Requires Java 1.7 (uses unicode_character_class regex)</b>
     */
    public static boolean containsQuery17(String text, String query) {
        boolean contains = false;
        // Split query into it's contents
        List<String> queryTokens = splitQuery(query);
        for (String queryToken : queryTokens) {
            if (!queryToken.isEmpty()) {
                Pattern pattern = Pattern.compile(".*\\b" + queryToken + "\\b.*",
                        // unicode_character_class exists in java7. in 6 it does not match unicode queries
                        Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CHARACTER_CLASS);
                Matcher matcher = pattern.matcher(text.toLowerCase());

                if (matcher.matches()) {
                    contains = true;
                } else {
                    return false;
                }
                matcher.reset();
            }
        }
        return contains;
    }

    public static List<String> splitQuery(String sQuery) {
        String sRes = "";
        List<String> allMatches = new ArrayList<String>();
        // first match all " ", if any
        String sRegex = "(?i)([\"\'].*?[\"\'])";
        Matcher m = Pattern.compile(sRegex).matcher(sQuery);
        boolean bFind = false;
        String sToAdd = "";

        while (m.find()) {
            bFind = true;
            sToAdd = m.group();
            // clear brackets
            allMatches.add(sToAdd.replaceAll("[\"\']", ""));
        }
        if (bFind) {
            sRes = sQuery.replaceAll(sRegex, "");
        } else {
            sRes = sQuery;
        }
        if (sQuery.trim().length() > 0) {
            // remove unneeded chars
            sRes = sRes.replaceAll("[,/&;?%$]", " ");
            // split to " " and return
            allMatches.addAll(Arrays.asList(sRes.toLowerCase().split("\\s+")));
        }
        // remove empty elements
        allMatches.removeAll(Arrays.asList(""));
        return allMatches;
    }

    /**
     * Tries to extract clean content from a raw html string
     *
     * @param sRawHtml The string representing the html text
     * @param bpExtractor the boilerpipe instance
     * @return the clean content of the html
     */
    public synchronized static String getCleanContent(String sRawHtml, BoilerpipeExtractor bpExtractor) {
        String sOneLiner = null;
        try {
            // try the one liner first
            sOneLiner = bpExtractor.getText(sRawHtml);
        } catch (BoilerpipeProcessingException ex) {
            Logger.getAnonymousLogger().log(Level.WARNING, ex.getMessage(), ex);
        }

        // init a new StringBuilder
        StringBuilder content = new StringBuilder();
        HTMLDocument htmlDoc = null;
        // init new input source
        InputSource is = new InputSource();
//        // force UTF-8
//        is.setEncoding("UTF-8");

        BoilerpipeSAXInput boilerpipeSaxInput = null;

        try {
            htmlDoc = new HTMLDocument(sRawHtml);
            // transform to input Source
            is = htmlDoc.toInputSource();

            boilerpipeSaxInput = new BoilerpipeSAXInput(is);

            TextDocument doc;
            // parse the document into boilerpipe's internal data structure
            doc = boilerpipeSaxInput.getTextDocument();
            // perform the extraction/ classification process on "doc"
            bpExtractor.process(doc);
            // iterate over all blocks (= segments as "Extractor" sees them)
            for (TextBlock block : doc.getTextBlocks()) {
                // if extractor suggests that the block is content
                if (block.isContent()) {
                    // if content appears to be of some value (empiric approach)
                    if (block.getNumWords() > 10 && block.getTextDensity() > 10) {
                        // keep it
                        content.append(block.getText().replaceAll("\\s+", " ")).append('\n');
                    } else {
                        // append new line for clarity of reading
                        content.append('\n');
                    }
                }
            }
            String sContent = content.toString();

            // prefer oneLiner results, if existing or if larger content
            if (sOneLiner == null || sOneLiner.trim().isEmpty()) {

                return sContent;

            } else {

                return sOneLiner.split("\\s+").length >= sContent.split("\\s+").length
                        ? sOneLiner : sContent;

            }

        } catch (SAXException ex) {
            Logger.getAnonymousLogger().log(Level.WARNING, ex.getMessage(), ex);
        } catch (BoilerpipeProcessingException ex) {
            Logger.getAnonymousLogger().log(Level.WARNING, ex.getMessage(), ex);
        } catch (Exception ex) {
            Logger.getAnonymousLogger().log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }

    public static String stripHtmlTags(String content, boolean bStripAll) {
        // add newlines next to all br, div and p paragraphs
        String sRes = content.replaceAll("<br|<p|<div", "\n");

        if (bStripAll) {
            sRes = Jsoup.clean(sRes, Whitelist.none());
        } else {
            sRes = Jsoup.clean(sRes, Whitelist.basic());
//            Document dDoc = Jsoup.parse(content);
//            Element eCur = dDoc.body();
//            sRes = getNewlinedChildren(eCur);
        }
        sRes = StringEscapeUtils.unescapeHtml4(sRes);
        return sRes.replaceAll("\n{2,}", "\n");
    }

    public static String stripHtmlTagsWithNewLines(String content, boolean bStripAll) {
        // add newlines next to all br, div and p paragraphs
        String sMed = "";
        String sRes = "";
        String sNewLine = "\n";
        if (bStripAll) {
            sMed = Jsoup.clean(content, Whitelist.none().addTags("p").addTags("br"));
            sRes = sMed.replaceAll("<[/]*p>", sNewLine)
                    .replaceAll("<br\\s*[/]*\\s*>", sNewLine);
        } else {
            sMed = Jsoup.clean(content, Whitelist.basic().addTags("p").addTags("br"));
            sRes = sMed.replaceAll("<[/]*p>", sNewLine)
                    .replaceAll("<br\\s*[/]*\\s*>", sNewLine);
        }
        sRes = StringEscapeUtils.unescapeHtml4(sRes);
        return sRes.replaceAll("\n{2,}", "\n");
    }

    public static synchronized String stripAndNormalizeCompletely(String text) {
        text = StringUtils.deleteWhitespace(text);
        text = Normalizer.normalize(text, Normalizer.Form.NFD)
                .replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
                .toLowerCase();
        text = StringUtils.replaceChars(text, ";:&!,.?-_' ", "");
        return text;
    }

    public synchronized static HttpClient getHttpClient() {
        if (CRAWLER_HTTP_CLIENT == null) {
            HttpParams params = new BasicHttpParams();
            // set socket timeout
            params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 5000);
            // set connection timeout
            params.setIntParameter(
                    CoreConnectionPNames.CONNECTION_TIMEOUT, 5000);
            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            ClientConnectionManager cm = new PoolingClientConnectionManager(registry);
            CRAWLER_HTTP_CLIENT = new DefaultHttpClient(cm, params);
        }
        return CRAWLER_HTTP_CLIENT;
    }
}
