/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.controller.crawl;

import eu.prepare.crawlers.impl.Crawler;
import eu.prepare.crawlers.impl.ICrawler;
import eu.prepare.crawlers.impl.bing.BingCrawler;
import eu.prepare.crawlers.impl.gplus.GplusCrawler;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.impl.twitter.TwitterCrawler;
import eu.prepare.crawlers.utils.Configuration;
import eu.prepare.crawlers.impl.youtube.YoutubeCrawler;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public abstract class AbstractCrawlerController {

    protected static final Logger LOGGER = Logger.getLogger(AbstractCrawlerController.class.getName());

    protected DataSource dataSource;
    protected final String engine_name;
    protected final long engine_id;
    /**
     * a list of crawlers, based on the request parameters, configured with
     * keys, secrets and other members based on system configuration
     */
    protected List<ICrawler> crawler_impls;

    public AbstractCrawlerController(DataSource dataSource, String crawl_name, long crawl_id) {
        this.dataSource = dataSource;
        this.engine_name = crawl_name;
        this.engine_id = crawl_id;
        this.crawler_impls = new ArrayList<ICrawler>();
    }

    public String getEngineName() {
        return engine_name;
    }

    public long getEngineID() {
        return engine_id;
    }

    /**
     * crawls with the query and list of crawlers, and returns the number of
     * results found
     *
     * @param qQuery the query to crawl for
     * @param query_time_limit the time limit to limit the query crawl wait. In
     * minutes
     * @return
     */
    public abstract int executeCrawl(final PQuery qQuery, int query_time_limit) throws IllegalArgumentException;

    public abstract void executeSingleThreadCrawl(PQuery qQuery);

    /**
     * calls the monitor() implementation of each respective crawler, if the
     * crawler supports it.
     * <b> The sources provided MUST exist in each crawlers repository </b>
     *
     * @param sources the list of sources per crawler name
     * @return the number of new items fetched per monitor service, i.e.
     * (twitter=100, gplus=10, etc).
     * <br><b>Currently supported only by twitter</b>
     * @throws IllegalArgumentException
     */
    public abstract Map<String, Integer> executeMonitor(Map<String, Collection<String>> sources) throws IllegalArgumentException;

    /**
     * // TODO: check implementation // for each crawler // for each query //
     * submit a runnable, and fetch results (crawler-query) // need different
     * method for result aggregation but looks faster // if query or crawler
     * pass the time_limit, abort submission // public abstract void
     * run(Set<PQuery> queries, int query_time_limit); /
     *
     * /**
     *
     * @param params the parameter map
     * @param configuration the system configuration
     * @throws SQLException
     */
    public void setCrawlerImpls(Map<String, String[]> params,
            Configuration configuration)
            throws SQLException {
        // result list
        List<ICrawler> crawler_list = new ArrayList<ICrawler>();

        // for each crawl parameter passed to the request.
        if (params.containsKey(Crawler.BING.getDecl())) {
            String bing = params.get(Crawler.BING.getDecl())[0];
            if (bing != null && bing.equals(Boolean.toString(true))) {
                crawler_list.add(
                        new BingCrawler(
                                configuration.getBingAppID(),
                                dataSource,
                                configuration.getBingAbortURLs(),
                                configuration.getMaxCrawlingThreads(),
                                configuration.getMinTokens()
                        )
                );
            }
        }
        if (params.containsKey(Crawler.GPLUS.getDecl())) {
            String gplus = params.get(Crawler.GPLUS.getDecl())[0];
            if (gplus != null && gplus.equals(Boolean.toString(true))) {
                crawler_list.add(
                        new GplusCrawler(
                                dataSource,
                                configuration
                        )
                );
            }
        }
        if (params.containsKey(Crawler.YOUTUBE.getDecl())) {
            String youtube = params.get(Crawler.YOUTUBE.getDecl())[0];
            if (youtube != null && youtube.equals(Boolean.toString(true))) {
                crawler_list.add(
                        new YoutubeCrawler(
                                configuration.getYouTubeAppName(),
                                configuration.getYouTubeDevID(),
                                dataSource,
                                configuration.getMinTokens()
                        )
                );
            }
        }
        if (params.containsKey(Crawler.TWITTER.getDecl())) {
            String twitter = params.get(Crawler.TWITTER.getDecl())[0];
            if (twitter != null && twitter.equals(Boolean.toString(true))) {
                crawler_list.add(
                        new TwitterCrawler(
                                configuration,
                                dataSource
                        )
                );
            }
        }
        this.crawler_impls = new ArrayList(crawler_list);
    }

    /**
     *
     * @return the amount of the crawler implementations assigned to this
     * crawler the AbstractCrawlerControllers MUST have been set with {@link #setCrawlerImpls(javax.servlet.http.HttpServletRequest, javax.sql.DataSource, eu.prepare.crawlers.utils.Configuration)
     * }
     */
    public int getCrawlersSize() {
        return this.crawler_impls.size();
    }
}
