/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.utils.response;

import eu.prepare.crawlers.utils.response.CrawlResult.Pair;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class CrawlResultList extends ArrayList<CrawlResult> {

    private Map<String, Map<String, Integer>> results;

    public CrawlResultList(int initialCapacity) {
        super(initialCapacity);
        this.results = new HashMap<String, Map<String, Integer>>();
    }

    public CrawlResultList() {
        this.results = new HashMap<String, Map<String, Integer>>();
    }

    public CrawlResultList(Collection<? extends CrawlResult> c) {
        super(c);
        this.results = new HashMap<String, Map<String, Integer>>();
        for (CrawlResult crawlResult : c) {
            Map<String, Integer> actual = new HashMap<String, Integer>();
            for (Pair p : crawlResult.getResults()) {
                if (actual.containsKey(p.getCrawler())) {
                    actual.put(p.getCrawler(), actual.get(p.getCrawler()) + p.getResults());
                } else {
                    actual.put(p.getCrawler(), p.getResults());
                }
            }
            results.put(crawlResult.getTerm(), actual);
        }
    }

    @Override
    public boolean add(CrawlResult e) {
        boolean added = super.add(e);
        if (results.containsKey(e.getTerm())) {
            Map<String, Integer> exist = results.get(e.getTerm());
            for (Pair p : e.getResults()) {
                if (exist.containsKey(p.getCrawler())) {
                    exist.put(p.getCrawler(), exist.get(p.getCrawler()) + p.getResults());
                } else {
                    exist.put(p.getCrawler(), p.getResults());
                }
            }
            results.put(e.getTerm(), exist);
        } else {
            Map<String, Integer> notexist = new HashMap<String, Integer>();
            for (Pair p : e.getResults()) {
                notexist.put(p.getCrawler(), p.getResults());
            }
            results.put(e.getTerm(), notexist);
        }
        return added;
    }

    public boolean containsCrawlName(String crawl_name) {
        return results.containsKey(crawl_name);
    }

    /**
     *
     * @return the calculated crawlResult list
     */
    public List<CrawlResult> getSyncedResults() {
        List<CrawlResult> res = new ArrayList<CrawlResult>();
        for (Map.Entry<String, Map<String, Integer>> entry : results.entrySet()) {
            String term = entry.getKey();
            Map<String, Integer> term_results_per_crawler = entry.getValue();
            CrawlResult tmp = new CrawlResult(term);
            for (Map.Entry<String, Integer> entry1 : term_results_per_crawler.entrySet()) {
                String crawler_name = entry1.getKey();
                Integer cr_res = entry1.getValue();
                tmp.appendResult(new CrawlResult().new Pair(crawler_name, cr_res));
            }
            res.add(tmp);
        }
        return res;
    }
}
