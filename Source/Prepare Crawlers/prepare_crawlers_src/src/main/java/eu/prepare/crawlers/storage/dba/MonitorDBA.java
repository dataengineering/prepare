/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.storage.dba;

import static eu.prepare.crawlers.storage.dba.CrawlerDBA.DATE_FORMAT;
import eu.prepare.crawlers.storage.util.SQLUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class MonitorDBA {

    /**
     * adds an entry to the crawl_log table, with date initiated
     *
     * @param dataSource
     * @param monitor_name the name of the crawl. Can be Null
     * @param now
     * @return the id of the crawl
     */
    public synchronized static long monitorInitiated(DataSource dataSource, String monitor_name, Date now) {
        long last_insert_id = -1l;
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Connection dbCon = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            dbCon = dataSource.getConnection();
            String sql = "INSERT INTO monitor_log(monitor_name, start_date) VALUES(?,?);";
            pStmt = dbCon.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            if (monitor_name != null && !monitor_name.trim().isEmpty()) {
                pStmt.setString(1, monitor_name);
            } else {
                pStmt.setNull(1, Types.VARCHAR);
            }
            pStmt.setString(2, sdf.format(now));
            pStmt.executeUpdate();
            rs = pStmt.getGeneratedKeys();
            if (rs.next()) {
                last_insert_id = rs.getLong(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MonitorDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbCon, pStmt, rs);
        }
        return last_insert_id;
    }

    public synchronized static void monitorFinished(DataSource dataSource, long monitor_id, Date end_date, int num_docs) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

        String sql = "UPDATE monitor_log SET end_date=?, num_docs=?  WHERE monitor_id = ?;";

        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            dbConnection = dataSource.getConnection();
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, sdf.format(end_date));
            preparedStatement.setInt(2, num_docs);
            preparedStatement.setLong(3, monitor_id);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MonitorDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, null);
        }
    }

    public synchronized static int numDocsFound(DataSource dataSource, int engine_type_id, long monitor_id) {
        int res = 0;
        String sql = "SELECT count(id) FROM twitter_post WHERE engine_type_id = ? and engine_id = ?;";

        Connection dbCon = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            dbCon = dataSource.getConnection();
            pStmt = dbCon.prepareStatement(sql);
            pStmt.setInt(1, engine_type_id);
            pStmt.setLong(2, monitor_id);
            rs = pStmt.executeQuery();
            if (rs.next()) {
                res = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MonitorDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbCon, pStmt, rs);
        }
        return res;
    }
}
