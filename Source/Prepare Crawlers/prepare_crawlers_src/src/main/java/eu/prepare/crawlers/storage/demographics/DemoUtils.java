/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.storage.demographics;

import eu.prepare.crawlers.storage.util.SQLUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import javax.sql.DataSource;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class DemoUtils {

    private DataSource dataSource;

    public DemoUtils(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Will look into nomad.age and compare the value provided against the
     * ranges in the table.
     *
     * @param age The age to check and get it's range ID
     * @return The ID for the range that the age belongs to, if it is in an
     * accepted range
     */
    protected int translatetoAgeID(int age) {
        int age_id = -1;
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        try {
            String sql
                    = "SELECT id FROM age WHERE ? >= minAge AND ? <= maxAge;";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setLong(1, age);
            pStmt.setLong(2, age);
            pStmt.executeQuery();

            resultSet = pStmt.getResultSet();

            if (resultSet.next()) {
                age_id = resultSet.getInt("id");
            }

        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
        return age_id;
    }

    protected Set<Integer> getValidAgeIDs() {
        Set<Integer> Ages = new HashSet<Integer>();
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        try {
            String sql
                    = "SELECT id FROM age;";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.executeQuery();

            resultSet = pStmt.getResultSet();

            while (resultSet.next()) {
                Ages.add(resultSet.getInt("id"));
            }
            return Ages;
        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
            return null;
        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
    }

    protected Set<Integer> getValidGenderIDs() {
        Set<Integer> Genders = new HashSet<Integer>();
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        try {
            String sql
                    = "SELECT id FROM gender;";

            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.executeQuery();

            resultSet = pStmt.getResultSet();

            while (resultSet.next()) {
                Genders.add(resultSet.getInt("id"));
            }
            return Genders;
        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
            return null;
        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
    }

    protected Set<Integer> getValidEducationIDs() {
        Set<Integer> EducationIDs = new HashSet<Integer>();

        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        try {
            String sql
                    = "SELECT id FROM education;";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.executeQuery();

            resultSet = pStmt.getResultSet();

            while (resultSet.next()) {
                EducationIDs.add(resultSet.getInt("id"));
            }
            return EducationIDs;
        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
            return null;
        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
    }

    protected int translatetoGenderID(String gender) {
        int iGenderID = -1;
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        try {

            String sql
                    = "SELECT id FROM gender WHERE LOWER(canonical_string) LIKE TRIM(LOWER(?));";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, gender.trim());
            pStmt.executeQuery();

            resultSet = pStmt.getResultSet();

            if (resultSet.next()) {
                iGenderID = resultSet.getInt("id");
            }

        } catch (SQLException ex) {
            System.err.println("Error: " + ex.getMessage());
        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
        return iGenderID;
    }

    protected boolean isEmptyRow(int[] iEntry) {
        // empty row means all values are zero
        for (int i : iEntry) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }

}
