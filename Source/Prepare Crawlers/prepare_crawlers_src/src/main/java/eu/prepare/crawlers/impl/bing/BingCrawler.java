package eu.prepare.crawlers.impl.bing;

import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import de.l3s.boilerpipe.extractors.ArticleExtractor;
import de.l3s.boilerpipe.extractors.DefaultExtractor;
import eu.prepare.crawlers.impl.AbstractCrawler;
import eu.prepare.crawlers.impl.ICrawler;
import eu.prepare.crawlers.impl.bing.storage.BingSQLstorage;
import eu.prepare.crawlers.storage.dba.CrawlerDBA;
import eu.prepare.crawlers.storage.model.PQuery;

import eu.prepare.crawlers.utils.CrawlerUtils;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

import net.billylieurance.azuresearch.AbstractAzureSearchResult;
import net.billylieurance.azuresearch.AzureSearchCompositeQuery;
import net.billylieurance.azuresearch.AzureSearchNewsResult;
import net.billylieurance.azuresearch.AzureSearchResultSet;
import net.billylieurance.azuresearch.AzureSearchWebResult;
import net.billylieurance.azuresearch.AbstractAzureSearchQuery.AZURESEARCH_QUERYTYPE;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author George K. <gkiom@iit.demokritos.gr>
 */
public class BingCrawler extends AbstractCrawler implements ICrawler {

    private final String appid;
    private long queryId;
    private final String AZURE_NO_QUERY_ALTERATIONS = "DisableQueryAlterations";
    private final String AZURE_FILE_TYPE = "Html";
    private final String[] ABORT_URLS;

    private BingSQLstorage bingstorage;
    private HttpClient client;

    private int max_threads;

    public enum Market {

        EN_GB("en-GB"),
        DE_DE("de-DE"),
        EL_GR("el-GR"),
        FR_FR("fr-FR");

        private final String market;

        private Market(String market) {
            this.market = market;
        }
    }
    private volatile int intIns; // represents the items inserted in bing internal storage

    /**
     * Will use hardcoded abort_urls
     *
     * @param appid The Bing AppID to use for crawling
     * @param dataSource the dataSource layer
     */
    public BingCrawler(String appid, DataSource dataSource) {
        super(dataSource);
        this.appid = appid;
        this.ABORT_URLS
                = new String[]{
                    "youtube", "facebook", "twitter", "google", "scribd", "linguee", "slideshare"
                };

        this.bingstorage = new BingSQLstorage(dataSource);
        this.client = CrawlerUtils.getHttpClient();
        this.max_threads = 1;
    }

    /**
     *
     * @param appid The Bing AppID to use for crawling
     * @param dataSource
     * @param ABORT_URLS parent domain URLs to abort.
     * @param max_threads the max number of threads to call on the crawler
     * @param min_tokens a document must contain tokens more than this to be
     * stored
     */
    public BingCrawler(String appid,
            DataSource dataSource,
            String[] ABORT_URLS,
            int max_threads,
            int min_tokens) {
        super(dataSource, min_tokens);
        this.appid = appid;
        this.ABORT_URLS = ABORT_URLS;
        this.bingstorage = new BingSQLstorage(dataSource);
        this.client = CrawlerUtils.getHttpClient();
        this.max_threads = max_threads;
    }

    @Override
    public int crawl(long crawl_id, PQuery qQuery) {
        return crawl(qQuery.getQueryTerm(), qQuery.getId(), qQuery.getLanguageID(), crawl_id);
    }

    protected int crawl(String query, long queryId, int iLangID, long crawl_id) {
        int cntRes = 0;
        try {
            this.queryId = queryId;
            String sLang = CrawlerDBA.getLanguageKey(dataSource, iLangID);
            if (sLang.equalsIgnoreCase("en")) {
                crawlWithMarket(query, Market.EN_GB);
            } else if (sLang.equalsIgnoreCase("el")) {
                crawlWithMarket(query, Market.EL_GR);
            } else if (sLang.equalsIgnoreCase("de")) {
                crawlWithMarket(query, Market.DE_DE);
            } else if (sLang.equalsIgnoreCase("fr")) {
                crawlWithMarket(query, Market.FR_FR);
            } else {
                throw new IllegalArgumentException(String.format("%s does not support %s language. Abort.%n", toString(), sLang));
            }
            cntRes = bingstorage.moveContent(crawl_id, queryId, intIns);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cntRes;
    }

    private void crawlWithMarket(final String query, final Market market) throws SQLException {
        AzureSearchCompositeQuery compositeQuery = new AzureSearchCompositeQuery();
        compositeQuery.setAppid(appid);
        compositeQuery.setQuery(query);
        compositeQuery.setSources(new AZURESEARCH_QUERYTYPE[]{
            AZURESEARCH_QUERYTYPE.WEB,
            AZURESEARCH_QUERYTYPE.NEWS
        });
        compositeQuery.setMarket(market.market);

        compositeQuery.setPerPage(50);
        // to not fetch other file formats (pdf, ppt, etc) - default is 'fetch all formats'
        compositeQuery.setWebFileType(AZURE_FILE_TYPE);
        // disable query alterations (is on by default)
        compositeQuery.setWebSearchOptions(AZURE_NO_QUERY_ALTERATIONS);
        compositeQuery.doQuery();

        // get results by engine
        AzureSearchResultSet<AbstractAzureSearchResult> ars = compositeQuery.getQueryResult();

        ExecutorService es;
        if (max_threads == 1) {
            es = Executors.newSingleThreadExecutor();
        } else {
            es = Executors.newFixedThreadPool(max_threads);
        }
        for (final AbstractAzureSearchResult anr : ars) {

            // try multithreaded 
            es.submit(new Runnable() {

                @Override
                public void run() {
                    intIns += fetchResult(anr, query, market);
                }
            });
        }
        // wait for completion
        es.shutdown();
        try {
            es.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            Logger.getLogger(BingCrawler.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    public int fetchResult(AbstractAzureSearchResult anr,
            String query,
            Market market) {
        boolean bProcess = true;
        int res = 0;
        try {
            String html = null;
            String title = null;
            String url = null;
            String urlHash = null;
            String date = null;
            String cleanedHtml = null;
            String cleanedHtmlHash;

            if (anr instanceof AzureSearchWebResult) {//fetch web results
                AzureSearchWebResult web = (AzureSearchWebResult) anr;
                title = web.getTitle();
                url = web.getUrl();
                for (String each : ABORT_URLS) {
                    if (url.contains(each)) {
                        bProcess = false;
                        break;
                    }
                }
                if (bProcess) {
                    urlHash = DigestUtils.md5Hex(url);
                    // get the raw html from the url fetched
                    html = getHtml(url);
                    if (html != null) {
                        // get clean html from boilerpipe, using input source
                        cleanedHtml = CrawlerUtils.getCleanContent(html, DefaultExtractor.INSTANCE);
                    }
                }

            } else { //fetch news results
                AzureSearchNewsResult news = (AzureSearchNewsResult) anr;
                title = news.getTitle();
                url = news.getUrl();
                for (String each : ABORT_URLS) {
                    if (url.contains(each)) {
                        bProcess = false;
                        break;
                    }
                }
                if (bProcess) {
                    date = news.getDate();
                    date = date.replace("T", " ");
                    date = date.replace("Z", "");
                    //countNews++;
                    urlHash = DigestUtils.md5Hex(url);
                    // get the raw html from the url fetched
                    html = getHtml(url);
                    if (html != null) {
                        // get clean html from boilerpipe, using input source
                        cleanedHtml = CrawlerUtils.getCleanContent(html, ArticleExtractor.INSTANCE);
                    }
                }
            }

            // if the retrieved content is not empty, and it indeed contains the query
            if (bProcess
                    && CrawlerUtils.shouldStore(cleanedHtml, query, min_tokens)) {

                cleanedHtmlHash = DigestUtils.md5Hex(cleanedHtml);

                Long id = bingstorage.retrieveMostRecentID(urlHash, queryId);

                if (id == null || id == 0) {

                    res = bingstorage.insertResult(query, url, urlHash,
                            title, html, cleanedHtml,
                            cleanedHtmlHash, market.market, date, queryId);

                } else {

                    if (date == null) {
                        if (!bingstorage.contentHashIsEqual(id, cleanedHtmlHash)) {
                            res = bingstorage.insertResult(query, url, urlHash,
                                    title, html, cleanedHtml,
                                    cleanedHtmlHash, market.market, date, queryId);
                        }
                    } else {

                        Date dateFormated
                                = new SimpleDateFormat(CrawlerDBA.DATE_FORMAT).parse(date);
                        Date dbDate = bingstorage.retrieveLatestDateFromDB(id);
                        if (dbDate != null) {
                            if (dateFormated.after(dbDate)) {
                                res = bingstorage.insertResult(query, url, urlHash,
                                        title, html, cleanedHtml,
                                        cleanedHtmlHash, market.market, date, queryId);
                            }
                        }
                    }
                }
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    private String getHtml(String url) throws IOException {
        // debug
//        System.out.println("Parsing url: " + url);
//        System.out.println(new Random().nextInt(10) + ": Parsing url: " + url);
        // debug
        HttpGet request = null;

        try {

            // init a http request
            request = new HttpGet(url);
            // create a response handler
            ResponseHandler<String> responseHandler = new ResponseHandler() {
                @Override
                public String handleResponse(final HttpResponse response)
                        throws HttpResponseException, IOException {

                    StatusLine statusLine = response.getStatusLine();
                    if (statusLine.getStatusCode() >= 300) {
                        throw new HttpResponseException(statusLine.getStatusCode(),
                                statusLine.getReasonPhrase());
                    }

                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        // parse with jSoup to avoid encoding issues
                        String sMed = EntityUtils.toString(entity, Charset.forName("UTF-8"));
                        // check for diamond question marks, and if so, abort text.
                        if (StringUtils.countMatches(sMed, "\uFFFD") < 4) {
                            // if unrecognized chars do not exist (or exist few) in text, save it, else abort
//                        if (sMed.indexOf("\uFFFD") == -1) { 
                            // parse html with jsoup
                            Document dHTMLDoc = Jsoup.parse(sMed);

                            return dHTMLDoc.outerHtml();
                        } else {
//                            System.out.println("Error: Wrong encoding declaration detected");
                            return null;
                        }
                    } else {
                        return null;
                    }
                }
            };
            // get the response
            String response = client.execute(request, responseHandler);

            return response;
        } catch (Exception ex) {
//            System.out.println("Error: " + ex.getMessage());
            return null;
        } finally {
//            if (client != null) {
//                // shut down the connection manager to ensure
//                // immediate deallocation of all system resources
//                client.getConnectionManager().shutdown();
//            }
            if (request != null) {
                request.releaseConnection();
            }
        }
    }

    @Override
    public int monitor(long monitor_id, Collection<String> sources) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
