/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl;

import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class AbstractCrawler {

    public static final Logger LOGGER = Logger.getLogger(AbstractCrawler.class.getName());

    protected DataSource dataSource;
    protected int min_tokens;

    public AbstractCrawler(DataSource dataSource) {
        this.dataSource = dataSource;
        this.min_tokens = 5; // a document must just contain 5 tokens to be accepted
    }

    public AbstractCrawler(DataSource dataSource, int min_tokens) {
        this.dataSource = dataSource;
        this.min_tokens = min_tokens;
    }

}
