/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class MonitorUtils {
    private static final Logger LOGGER = Logger.getLogger(MonitorUtils.class.getName());

    public static FileLock acquireFileLock(String workingDir, String caller) {
        String fname = workingDir.concat(caller).concat(".lock");
        File file = new File(fname);
        boolean isLocked = true;
        FileLock lock = null;
        try {
            FileChannel channel = new RandomAccessFile(file, "rw").getChannel();

            while (isLocked) {
                try {
                    lock = channel.tryLock();
                    isLocked = false;
                } catch (OverlappingFileLockException e) {
                    // File is already locked in this thread or virtual machine
//                    LOGGER.warning("Lock exists, waiting");
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MonitorUtils.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(MonitorUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MonitorUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lock;
    }

    public static void releaseLock(FileLock lock) {
        if (lock != null) {
            try {
                lock.release();
            } catch (IOException ex) {
                Logger.getLogger(MonitorUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static int sum(Collection<Integer> items) {
        int sum = 0;
        if (items == null || items.isEmpty()) {
            return sum;
        }
        for (Integer each : items) {
            sum += each;
        }
        return sum;
    }
}
