/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.controller.services;

import com.google.gson.Gson;
import eu.prepare.crawlers.controller.crawl.AbstractCrawlerController;
import eu.prepare.crawlers.controller.crawl.DefaultCrawlerController;
import static eu.prepare.crawlers.controller.services.CrawlService.LOGGER;
import eu.prepare.crawlers.impl.Crawler;
import eu.prepare.crawlers.storage.dba.MonitorDBA;
import eu.prepare.crawlers.utils.Configuration;
import eu.prepare.crawlers.utils.CrawlerUtils;
import eu.prepare.crawlers.utils.MonitorUtils;
import eu.prepare.crawlers.utils.response.MonitorResponse;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.channels.FileLock;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class MonitorService
 * <br>------------------------------------------------------------
 * <br> EXAMPLE CALL: MONITOR
 * <br> http://localhost:8080/PrepareCrawler/Monitor?twitter=true
 <br> it will call the crawlers declared that actively implement the monitor
 * method and crawl the sources that they are assigned to.
 * <br>
 http://localhost:8080/PrepareCrawler/Monitor?twitter=true&twitter_sources=["account1",
 * "account2"]&monitor_name=mon1
 * <br> it will call twitter with the two accounts specified
 * <br> The monitored sources <b>must already preexist</b> in the DB
 * <br> Currently monitor() is supported only by TwitterCrawler implementation
 * <br> For response string, see end of current file.
 * <br>------------------------------------------------------------
 *
 * @author George K. <gkiom@iit.demokritos.gr>
 */
@WebServlet(name = "MonitorService", urlPatterns = {"/Monitor"})
public class MonitorService extends HttpServlet {

    private static final long serialVersionUID = 51L;
    private final String PROPERTIES = "crawler.properties";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json;charset=UTF-8");
        DataSource ds = CrawlService.getDS();
        final PrintStream out = new PrintStream(response.getOutputStream());
        // init configuration
        ServletContext servletContext = getServletContext();
        String workingDir = servletContext.getRealPath("/").endsWith("/") ? servletContext.getRealPath("/") + "WEB-INF/"
                : servletContext.getRealPath("/") + "/WEB-INF/";
        final Configuration configuration = new Configuration(workingDir + PROPERTIES);
        configuration.setWorkingDir(workingDir);
        // initialize utilities
        CrawlerUtils.initialiseUtils(workingDir);
        // block thread until finalized
        FileLock curLock = MonitorUtils.acquireFileLock(workingDir, "monitor");
        // log start date
        Date start = new Date();
        // get parameters
        String monitor_name = request.getParameter("monitor_name");
        if (monitor_name == null) {
            monitor_name = "monitor_".concat(start.toString());
        }
        // inject monitor started
        long monitor_id = MonitorDBA.monitorInitiated(ds, monitor_name, start);
        // init monitor controller //
        AbstractCrawlerController monitor_controller = new DefaultCrawlerController(ds, monitor_name, monitor_id);
        // verbose
        LOGGER.log(Level.INFO, "<MonitorID {0}> Initialized at {1} ", new Object[]{monitor_id, start});
        // init monitor response
        MonitorResponse monitor_response = new MonitorResponse(monitor_id, monitor_name, start);
        // get Sources required for monitoring
        Map<String, Collection<String>> sources = getSources(request);

        try {
            monitor_controller.
                    // get needed services at runtime
                    setCrawlerImpls(request.getParameterMap(), configuration);

            // perform monitor 
            Map<String, Integer> monitor_results = monitor_controller.executeMonitor(sources);
            // log end date
            Date end = new Date();
            monitor_response.setEnded(end);
            monitor_response.setFetched(monitor_results);
            out.print(monitor_response.toJSON());
            int num_docs = MonitorUtils.sum(monitor_results.values());
            // monitor finalized
            MonitorDBA.monitorFinished(ds, monitor_id, end, num_docs);
            // verbose
            LOGGER.log(Level.INFO, "<MonitorID {0}> Finalized at {1}, total items: {2} ", new Object[]{monitor_id, end, num_docs});
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } finally {
            out.close();
            ds = null;
            MonitorUtils.releaseLock(curLock);
        }
    }

    private Map<String, Collection<String>> getSources(HttpServletRequest request) {
        Enumeration<String> params = request.getParameterNames();
        // (crawler.toString, Sources) map
        Map<String, Collection<String>> sources = new LinkedHashMap();
        while (params.hasMoreElements()) {
            String nextParam = params.nextElement();
            if (nextParam.contains("_sources")) {
                String insert = request.getParameter(nextParam);
                List<String> expList = new Gson().fromJson(insert, List.class);
                sources.put(
                        Crawler.crawler_declarations.get(Crawler.valueOf(nextParam.split("_")[0].toUpperCase())),
                        expList);
            }
        }
        return sources;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

// response json 
//{
//
//    "monitor_id": 8,
//    "monitor_name": "mon_test_8",
//    "started": "Jun 24, 2015 4:29:08 PM",
//    "ended": "Jun 24, 2015 4:29:11 PM",
//    "results": 
//      {
//        "TwitterCrawler": 4
//      }
//
//}
