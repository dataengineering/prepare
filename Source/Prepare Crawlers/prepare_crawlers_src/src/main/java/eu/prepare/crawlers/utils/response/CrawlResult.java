/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.utils.response;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class CrawlResult {

    private String term;
    private List<Pair> results;

    public CrawlResult(String term, List<Pair> results) {
        this.term = term;
        this.results = results;
    }

    public CrawlResult() {
        this.term = "";
        this.results = new ArrayList<Pair>();
    }

    public CrawlResult(String term) {
        this.term = term;
        this.results = new ArrayList<Pair>();
    }

    public List<Pair> getResults() {
        return results;
    }

    public void setResults(List<Pair> results) {
        this.results = results;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public void appendResult(Pair result) {
        this.results.add(result);
    }

    public boolean containsCrawlerName(String crawler) {
        for (Pair pair : results) {
            if (pair.getCrawler().equalsIgnoreCase(crawler)) {
                return true;
            }
        }
        return false;
    }

    public Integer getCrawlerResults(String crawler) {
        for (Pair pair : results) {
            String name = pair.getCrawler();
            if (name.equalsIgnoreCase(crawler)) {
                return pair.getResults();
            }
        }
        return 0;
    }

    @Override
    public String toString() {
        return "CrawlResult{" + "term=" + term + ", results=" + results + '}';
    }

    public class Pair {

        private String crawler;
        private Integer items;

        public Pair(String crawler, Integer results) {
            this.crawler = crawler;
            this.items = results;
        }

        public String getCrawler() {
            return crawler;
        }

        public void setCrawler(String crawler) {
            this.crawler = crawler;
        }

        public Integer getResults() {
            return items;
        }

        public void setResults(Integer results) {
            this.items = results;
        }

        @Override
        public String toString() {
            return "Pair{" + "crawler=" + crawler + ", items=" + items + '}';
        }
    }
}
