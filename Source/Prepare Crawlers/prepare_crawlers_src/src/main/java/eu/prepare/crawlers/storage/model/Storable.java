/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.storage.model;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class Storable {

    /**
     * basic items of the storable item in the content table
     */
    final private String url, clean_text, timestamp, language, sourceTable;
    /**
     * the query ID that this storable originated from
     */
    final private long queryId;

    public Storable(String url, String clean_text, String timestamp, String language, String sourceTable, long queryId) {
        this.url = url;
        this.clean_text = clean_text;
        this.timestamp = timestamp;
        this.language = language;
        this.sourceTable = sourceTable;
        this.queryId = queryId;
    }

    public String getUrl() {
        return url;
    }

    public String getCleanText() {
        return clean_text;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getLanguage() {
        return language;
    }

    public String getSourceTable() {
        return sourceTable;
    }

    public long getQueryId() {
        return queryId;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.url != null ? this.url.hashCode() : 0);
        hash = 79 * hash + (this.timestamp != null ? this.timestamp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Storable other = (Storable) obj;
        if ((this.url == null) ? (other.url != null) : !this.url.equals(other.url)) {
            return false;
        }
        if ((this.timestamp == null) ? (other.timestamp != null) : !this.timestamp.equals(other.timestamp)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Storable{" + "url=" + url + ", clean_text=" + clean_text + ", timestamp=" + timestamp + ", language=" + language + ", sourceTable=" + sourceTable + ", queryId=" + queryId + '}';
    }

}
