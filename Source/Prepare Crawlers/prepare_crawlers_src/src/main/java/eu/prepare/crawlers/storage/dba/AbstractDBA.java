/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.storage.dba;

import eu.prepare.crawlers.impl.Crawler;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public abstract class AbstractDBA {

    protected final DataSource datasource;

    public static final String RES_OK = "OK";
    public static final String RES_EXISTS = "Omitted (already exists)";
    public static final String RES_UPDATED_ACTIVE_STATE = "Updated active ";
    public static final String RES_ERROR = "Error: ";

    public AbstractDBA(DataSource datasource) {
        this.datasource = datasource;
    }

    public abstract String insertQueries(List<Map<Object, Object>> queries);

    public abstract String insertSources(EnumMap<Crawler, List<Map<Object, Object>>> sources);

}
