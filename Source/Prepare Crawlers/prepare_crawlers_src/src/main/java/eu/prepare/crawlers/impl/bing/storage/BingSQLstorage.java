/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl.bing.storage;

import eu.prepare.crawlers.impl.bing.BingCrawler;
import eu.prepare.crawlers.storage.dba.CrawlerDBA;
import eu.prepare.crawlers.utils.CrawlerUtils;
import eu.prepare.crawlers.storage.util.SQLUtils;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class BingSQLstorage {

    private DataSource dataSource;

    public static final String BING_TABLE = "bing_result";

    public BingSQLstorage(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * loads the latest #maxtop items fetched for the #query_id, validates them,
     * and stores the results to content table.
     *
     * @param crawl_id the unique ID of the crawl
     * @param queryId the query_id that the content was retrieved for
     * @param maxtop the amount of items fetched for this query_id in the
     * current execution
     * @return
     * @throws SQLException
     * @throws ParseException
     */
    public int moveContent(long crawl_id, long queryId, int maxtop) throws SQLException, ParseException {
        int cntRes = 0;
        String sql = "SELECT id, url, clean_content, datetime, language "
                + "FROM bing_result "
                + "WHERE query_id = ? ORDER BY id DESC LIMIT ?;";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection dbConnection = null;
        try {
            dbConnection = dataSource.getConnection();
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setLong(1, queryId);
            preparedStatement.setInt(2, maxtop);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {

                String url = resultSet.getString("url");
                String clean_text = resultSet.getString("clean_content");
                String timestamp = resultSet.getString("datetime");
                String language = resultSet.getString("language");

                cntRes += CrawlerDBA.storeContent(
                        dataSource, url, clean_text, timestamp, language,
                        BING_TABLE, queryId, crawl_id);
            }
            return cntRes;
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, resultSet);
        }
    }

    public synchronized Long retrieveMostRecentID(String urlHash, long query_id) throws SQLException {
        Long id = null;
        String sql = "SELECT id FROM bing_result "
                + "WHERE url_hash = ? AND query_id = ? "
                + "ORDER BY id DESC LIMIT 1;";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection dbConnection = null;
        try {
            dbConnection = dataSource.getConnection();
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, urlHash);
            preparedStatement.setLong(2, query_id);
            preparedStatement.execute();
            resultSet = preparedStatement.getResultSet();
            if (resultSet.next()) {
                id = resultSet.getLong(1);
            }
            return id;
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, resultSet);
        }
    }

    public synchronized boolean contentHashIsEqual(long id, String cleanedHtmlHash) throws SQLException {
        boolean isEqual = false;
        String sql = "SELECT clean_content_hash, id FROM bing_result WHERE id = ? ORDER BY id DESC LIMIT 1;";
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection dbConnection = null;
        try {
            dbConnection = dataSource.getConnection();
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            resultSet = preparedStatement.getResultSet();
            if (resultSet.next()) {
                String dbHash = resultSet.getString(1);
                return dbHash.equals(cleanedHtmlHash);
            }
            return isEqual;
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, resultSet);
        }
    }

    public synchronized Date retrieveLatestDateFromDB(Long id) throws SQLException {
        String sql = "SELECT datetime FROM bing_result WHERE id = ?;";
        Date date = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Connection dbConnection = null;
        try {
            dbConnection = dataSource.getConnection();
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setLong(1, id);
            preparedStatement.execute();
            resultSet = preparedStatement.getResultSet();
            if (resultSet.next()) {
                date = resultSet.getTimestamp("datetime");
            }
            return date;
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, resultSet);
        }
    }

    public synchronized int insertResult(
            String query,
            String url,
            String urlHash,
            String title,
            String html,
            String cleanedHtml,
            String cleanedHtmlHash,
            String market,
            String date,
            long query_id
    ) throws SQLException {
        String sql = "INSERT INTO bing_result(query, url, url_hash, title, "
                + "html_content, clean_content, clean_content_hash, market, "
                + "language, datetime, query_id)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStatement = null;
        Connection dbConnection = null;
        try {
            dbConnection = dataSource.getConnection();
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, query);
            preparedStatement.setString(2, url);
            preparedStatement.setString(3, urlHash);
            preparedStatement.setString(4, title);
            preparedStatement.setBinaryStream(5, IOUtils.toInputStream(html));
            preparedStatement.setString(6, cleanedHtml);
            preparedStatement.setString(7, cleanedHtmlHash);
            preparedStatement.setString(8, market);
            String sLang = CrawlerUtils.identifyLanguage(cleanedHtml);
            preparedStatement.setString(9, sLang);
            if (date == null) {
                date = retrieveDateFromUrl(url);
            }
            if (date == null) {
                preparedStatement.setNull(10, Types.VARCHAR);
            } else {
                preparedStatement.setString(10, date);
            }
            preparedStatement.setLong(11, query_id);
            return preparedStatement.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, null);
        }
    }

    private String retrieveDateFromUrl(String url) {
        // get last modified date from URL
        URL uURL;
        HttpURLConnection httpCon = null;
        try {
            uURL = new URL(url);
            httpCon = (HttpURLConnection) uURL.openConnection();
            long date = httpCon.getLastModified();
            if (date == 0) {
                return null;
            } else {
                SimpleDateFormat sdf = new SimpleDateFormat(CrawlerDBA.DATE_FORMAT);
                return sdf.format(date);
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(BingCrawler.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (IOException ex) {
            Logger.getLogger(BingCrawler.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } finally {
            if (httpCon != null) {
                httpCon.disconnect();
            }
        }
        return null;
    }

}
