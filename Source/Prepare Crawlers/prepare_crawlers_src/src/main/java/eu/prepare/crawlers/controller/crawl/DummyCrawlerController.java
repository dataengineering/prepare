/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.controller.crawl;

import eu.prepare.crawlers.impl.Crawler;
import eu.prepare.crawlers.impl.ICrawler;
import eu.prepare.crawlers.impl.bing.DummyBingCrawler;
import eu.prepare.crawlers.impl.gplus.DummyGplusCrawler;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.storage.dba.CrawlerDBA;
import eu.prepare.crawlers.storage.dba.QueryDBA;
import eu.prepare.crawlers.impl.twitter.DummyTwitterCrawler;
import eu.prepare.crawlers.utils.Configuration;
import eu.prepare.crawlers.impl.youtube.DummyYouTubeCrawler;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import javax.sql.DataSource;

/**
 * for testing of inputs/outputs/responses purposes
 *
 * @author George K. <gkiom@scify.org>
 */
public class DummyCrawlerController extends AbstractCrawlerController {

    public DummyCrawlerController(DataSource dataSource, String crawl_name, long crawl_id) {
        super(dataSource, crawl_name, crawl_id);
    }

    /**
     * simulates a crawl with the query and list of crawlers , and returns the
     * number of results found
     *
     * @param qQuery the query crawled
     * @param time_limit the amount of time to wait for results (in hours)
     * @return
     * @deprecated
     */
    @Override
    public int executeCrawl(final PQuery qQuery, int time_limit) throws IllegalArgumentException {
        final AtomicInteger res = new AtomicInteger();
        final Map<String, Integer> crawl_resCnt
                = Collections.synchronizedMap(new HashMap<String, Integer>());
        // perform multithreaded crawl. Each query at a time, all services
        // serve this query. Time of query loop = time of the slowest service.
        // init executor service
        int iThreads = crawler_impls.size();
        ExecutorService es;
        if (iThreads <= 0) {
            throw new IllegalArgumentException("No Crawlers have been specified to run");
        } else if (iThreads == 1) {
            es = Executors.newSingleThreadExecutor();
        } else {
            es = // init a thread for each crawler
                    Executors.newFixedThreadPool(iThreads);
        }
        // log start time in seconds
        long start_time = TimeUnit.MILLISECONDS.toSeconds(new Date().getTime());
        // for each service needed, pass query.
        for (final ICrawler crawler : crawler_impls) {
            es.submit(new Runnable() {
                @Override
                public void run() {
                    String sName = crawler.toString();
                    int iResults = crawler.crawl(engine_id, qQuery);
                    synchronized (crawl_resCnt) {
                        crawl_resCnt.put(sName, iResults);
                        res.addAndGet(iResults);
                    }
                }
            });
        }
        // Await completion
        es.shutdown();
        try {
            boolean terminatedNormally = es.awaitTermination(time_limit, TimeUnit.MINUTES);
            if (!terminatedNormally) {
                LOGGER.log(Level.INFO, "terminated execution in {0} minutes", time_limit);
            }
        } catch (InterruptedException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        // update query execution times
        QueryDBA.updateQueryExecTimes(dataSource, qQuery.getId());
        // update query data fetched
        CrawlerDBA.updateQueryInfo(dataSource, engine_id, qQuery.getId(), crawl_resCnt);
        // log end time
        long end_time = TimeUnit.MILLISECONDS.toSeconds(new Date().getTime());
        // verbose
        LOGGER.log(Level.INFO, "<CrawlID {0}>: Query: ''{1}'' finished at {2} with results: {3} - Sum: {4} - duration: {5} secs",
                new Object[]{engine_id, qQuery.getQueryTerm(), new Date(), crawl_resCnt.toString(), res.intValue(), (end_time - start_time)});
        return res.intValue();
    }

    /**
     * @throws java.sql.SQLException
     * @deprecated
     */
    @Override
    public void setCrawlerImpls(Map<String, String[]> params,
            Configuration configuration)
            throws SQLException {
        // result list
        List<ICrawler> lsCrawl = new ArrayList<ICrawler>();
        // for each crawl parameter 
        if (params.containsKey(Crawler.BING.getDecl())) {
            String bing = params.get(Crawler.BING.getDecl())[0];
            if (bing != null && bing.equals(Boolean.toString(true))) {
                lsCrawl.add(new DummyBingCrawler(dataSource));
            }
        }
        if (params.containsKey(Crawler.GPLUS.getDecl())) {
            String gplus = params.get(Crawler.GPLUS.getDecl())[0];
            if (gplus != null && gplus.equals(Boolean.toString(true))) {
                lsCrawl.add(new DummyGplusCrawler(dataSource));
            }
        }
        if (params.containsKey(Crawler.YOUTUBE.getDecl())) {
            String ytub = params.get(Crawler.YOUTUBE.getDecl())[0];
            if (ytub != null && ytub.equals(Boolean.toString(true))) {
                lsCrawl.add(new DummyYouTubeCrawler(dataSource));
            }
        }
        if (params.containsKey(Crawler.TWITTER.getDecl())) {
            String twiit = params.get(Crawler.TWITTER.getDecl())[0];
            if (twiit != null && twiit.equals(Boolean.toString(true))) {
                lsCrawl.add(new DummyTwitterCrawler(dataSource));
            }
        }
        this.crawler_impls = new ArrayList(lsCrawl);
    }

    /**
     *
     * @param qQuery
     * @deprecated
     */
    @Override
    public void executeSingleThreadCrawl(PQuery qQuery) {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    public Map<String, Integer> executeMonitor(Map<String, Collection<String>> sources) throws IllegalArgumentException {
        throw new UnsupportedOperationException("Not supported");
    }

}
