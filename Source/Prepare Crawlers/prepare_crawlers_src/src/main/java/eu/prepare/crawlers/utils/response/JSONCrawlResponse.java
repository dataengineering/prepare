/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.utils.response;

import com.google.gson.Gson;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class JSONCrawlResponse extends CrawlResponse implements IResponse {

    public static String FIELD_CRAWL_ID = "crawl_id";
    public static String FIELD_CRAWL_RESPONSE = "crawl_response";

    public JSONCrawlResponse(long crawl_id, MapCrawlResult crawl_results) {
        super(crawl_id, crawl_results);
    }

    public JSONCrawlResponse(long crawl_id) {
        super(crawl_id);
    }

    public MapCrawlResult getCrawlResponse() {
        return crawl_response;
    }

    @Override
    public String toJSON() {
        return new Gson().toJson(this, JSONCrawlResponse.class);
    }
//
//    /**
//     * Helper class to serialize as needed in the API
//     */
//    class JSONCrawlResponseSerializer implements JsonSerializer<JSONCrawlResponse> {
//
//        @Override
//        public JsonElement serialize(JSONCrawlResponse t, Type type, JsonSerializationContext jsc) {
//            final JsonObject jsonObject = new JsonObject();
//            jsonObject.addProperty(FIELD_CRAWL_ID, t.getCrawlID());
//            jsonObject.addProperty(FIELD_CRAWL_RESPONSE, t.getCrawlResponse());
//            jsonObject.addProperty(IASummary.FIELD_DATE, t.getDate());
//
//            final JsonElement jsonSummary = jsc.serialize(t.getSummary());
//            jsonObject.add(IASummary.FIELD_SUMMARY, jsonSummary);
//
//            jsonObject.addProperty(IASummary.FIELD_LANG, t.getLang());
//            jsonObject.addProperty(IASummary.FIELD_LAST_UPDATED, t.getSavedDate());
//            jsonObject.addProperty(IASummary.FIELD_CATEGORY, t.getCategory());
//            // add hashtags
//            final JsonArray jsonHashtags = new JsonArray();
//            for (final String each_hashtag : t.getHashtags()) {
////            for (final String each_hashtag : t.getHashtagEntities()) {
//                JsonElement je = jsc.serialize(each_hashtag);
//                jsonHashtags.add(je);
//            }
//            jsonObject.add(FIELD_HASHTAGS, jsonHashtags);
//            // add named_entities
//            final JsonArray json_nes = new JsonArray();
//            for (final String each_ne : t.getNamedEntities()) {
////            for (final String each_hashtag : t.getHashtagEntities()) {
//                JsonElement je = jsc.serialize(each_ne);
//                json_nes.add(je);
//            }
//            jsonObject.add(FIELD_NAMED_ENTITIES, json_nes);
//            // add slugline
//            jsonObject.addProperty(FIELD_SLUGLINE, t.getNESlugline());
//            // add related tweets
//            final JsonArray jsonRelatedTweets = new JsonArray();
//            for (final IATweet tweet : t.getRelatedTweets()) {
//                final JsonElement jsonTweet = jsc.serialize(tweet);
//                jsonRelatedTweets.add(jsonTweet);
//            }
//            jsonObject.add(IASummary.FIELD_RELATED_TWEETS, jsonRelatedTweets);
//            // add related fb_posts
//            final JsonArray jsonRelatedFB = new JsonArray();
//            for (final FBPost post : t.getRelatedFBPosts()) {
//                final JsonElement jsonFB = post.toJSONElement();
//                jsonRelatedFB.add(jsonFB);
//            }
//            jsonObject.add(IASummary.FIELD_RELATED_FB_POSTS, jsonRelatedFB);
//            return jsonObject;
//        }
//    }
}
