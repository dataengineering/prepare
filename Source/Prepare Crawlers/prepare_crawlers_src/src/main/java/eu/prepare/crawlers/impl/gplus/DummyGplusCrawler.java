/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl.gplus;

import eu.prepare.crawlers.impl.AbstractCrawler;
import eu.prepare.crawlers.impl.ICrawler;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.impl.youtube.DummyYouTubeCrawler;
import java.util.Collection;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 * For tests
 *
 * @author George K. <gkiom@scify.org>
 */
public class DummyGplusCrawler extends AbstractCrawler implements ICrawler {

    public DummyGplusCrawler(DataSource dataSource) {
        super(dataSource);
    }

    @Override
    public int crawl(long crawl_id, PQuery query) {
        try {
            Thread.sleep(1000l);
        } catch (InterruptedException ex) {
            Logger.getLogger(DummyYouTubeCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new Random().nextInt(10);
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    @Override
    public int monitor(long monitor_id, Collection<String> sources) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
