/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.storage.dba;

import eu.prepare.crawlers.controller.crawl.AbstractCrawlerController;
import eu.prepare.crawlers.storage.util.SQLUtils;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.utils.CrawlerUtils;
import eu.prepare.crawlers.utils.response.CrawlResult;
import eu.prepare.crawlers.utils.response.CrawlResult.Pair;
import eu.prepare.crawlers.utils.response.CrawlResultList;
import eu.prepare.crawlers.utils.response.JSONCrawlResponse;
import eu.prepare.crawlers.utils.response.MapCrawlResult;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class CrawlerDBA {

    public static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final Map<Integer, String> lang_keys = Collections.synchronizedMap(new HashMap<Integer, String>());
    private static final Map<Engine, Integer> engines = Collections.synchronizedMap(new EnumMap<Engine, Integer>(Engine.class));

    /**
     *
     * @param dataSource
     * @param url The URL to store
     * @param clean_text The clean content to store
     * @param timestamp The timestamp the document was created
     * @param language The language the document is in
     * @param sourceTable The source table that the document comes from
     * @param query_id The query id that the document was retrieved from
     * @param crawl_id the crawl ID executed
     * @return The number of inserts performed
     * @throws SQLException
     * @throws ParseException
     */
    public synchronized static int storeContent(
            DataSource dataSource,
            String url, String clean_text,
            String timestamp, String language,
            String sourceTable, long query_id, long crawl_id)
            throws SQLException, ParseException {

        // Init result count returned
        int cntRes = 0;

        // If nothing to add
        if (clean_text == null || clean_text.trim().isEmpty()) {
            // return
            return 0;
        }

        String SQL = "select id, clean_text, timestamp from content "
                + "where query_id=? and url=? and source_table=? "
                + "order by id desc limit 1;";

        Connection dbConnection = null;
        PreparedStatement pStatement = null;
        ResultSet rset = null;
        Date dNew;
        Date dOld;
        try {
            dbConnection = dataSource.getConnection();
            // Init statement
            pStatement = dbConnection.prepareStatement(SQL);
            pStatement.setLong(1, query_id);
            pStatement.setString(2, url);
            pStatement.setString(3, sourceTable);
            // Execute query
            rset = pStatement.executeQuery();
            // If there exists such a record
            if (rset.next()) {
                String existing_text = rset.getString("clean_text");
                existing_text = CrawlerUtils.stripAndNormalizeCompletely(existing_text);
                String dbHash = DigestUtils.md5Hex(existing_text);
                // if timestamp retrieved and passed
                String newDocHash = DigestUtils.md5Hex(CrawlerUtils.stripAndNormalizeCompletely(clean_text));
                if (timestamp != null) {
                    dNew = new SimpleDateFormat(DATE_FORMAT).parse(timestamp);
                    dOld = rset.getTimestamp("timestamp");
                    if (dOld != null) {
                        // If the MD5 is the same with the current text
                        // AND the timestamp is before stored date or the same
                        if (dbHash.equals(newDocHash) && (dNew.equals(dOld) || dNew.before(dOld))) {
                            // ignore new insert and return
                            return 0;
                        }
                    }
                } else { // if null timestamp
                    dOld = rset.getTimestamp("timestamp");
                    // if already stored as NULL, and text same, ignore
                    if (dOld == null
                            && dbHash.equals(newDocHash)) {
                        return 0;
                    }
                }
            }
            // if such a record does not exist or has an updated text
            // Insert new record
            cntRes
                    = simpleStoreContent(dbConnection, url, clean_text, timestamp, language, sourceTable, query_id, crawl_id);
        } finally {
            SQLUtils.release(dbConnection, pStatement, rset);
        }
        return cntRes;
    }

    private synchronized static int simpleStoreContent(
            Connection dbConnection,
            String url,
            String clean_text,
            String timestamp,
            String language,
            String sourceTable,
            long query_id,
            long crawl_id)
            throws SQLException {

        String sql = "INSERT INTO "
                + "content (url, clean_text, timestamp, crawl_timestamp, language, source_table, query_id, crawl_id) "
                + "VALUES "
                + "(?, ?, ?, ?, ?, ?, ?, ?);";
        int countResults = 0;
        PreparedStatement pStmt = null;
        try {
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, url.trim());
            pStmt.setString(2, clean_text);
            // insert NULL timestamp if not defined
            if (timestamp == null) {
                pStmt.setNull(3, Types.DATE);
            } else {
                pStmt.setString(3, timestamp);
            }
            // always insert current crawl time
            pStmt.setString(4, getCurrentTimeStamp());
            pStmt.setString(5, language);
            pStmt.setString(6, sourceTable.trim());
            pStmt.setLong(7, query_id);
            pStmt.setLong(8, crawl_id);
            countResults = pStmt.executeUpdate();
        } finally {
            SQLUtils.release(null, pStmt, null);
        }
        return countResults;
    }

    public synchronized static long getContentID(DataSource dataSource, String url, String timestamp, String sourceTable, long queryId)
            throws SQLException {
        long id = 0L;
        String sql = "SELECT id FROM content WHERE url = ? AND timestamp=? AND source_table = ? AND query_id=? LIMIT 1;";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, url);
            pStmt.setString(2, timestamp);
            pStmt.setString(3, sourceTable);
            pStmt.setLong(4, queryId);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            if (rSet.next()) {
                id = rSet.getLong("id");
            }
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
        return id;
    }

    /**
     * adds an entry to the crawl_log table, with date initiated
     *
     * @param dataSource
     * @param crawl_name the name of the crawl. Can be Null
     * @param start_date
     * @return the id of the crawl
     */
    public synchronized static long crawlInitiated(DataSource dataSource, String crawl_name, Date start_date) {
        long last_insert_id = -1l;
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Connection dbCon = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            dbCon = dataSource.getConnection();
            String sql = "INSERT INTO crawl_log(crawl_name, start_date) VALUES(?,?);";
            pStmt = dbCon.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            if (crawl_name != null && !crawl_name.trim().isEmpty()) {
                pStmt.setString(1, crawl_name);
            } else {
                pStmt.setNull(1, Types.VARCHAR);
            }
            pStmt.setString(2, sdf.format(start_date));
            pStmt.executeUpdate();
            rs = pStmt.getGeneratedKeys();
            if (rs.next()) {
                last_insert_id = rs.getLong(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CrawlerDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbCon, pStmt, rs);
        }
        return last_insert_id;
    }

    public synchronized static void crawlFinished(DataSource dataSource, long crawl_id, Date date, int num_docs) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

        String sql = "UPDATE crawl_log SET end_date=?, num_docs=?  WHERE crawl_id = ?;";

        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            dbConnection = dataSource.getConnection();
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setString(1, sdf.format(date));
            preparedStatement.setInt(2, num_docs);
            preparedStatement.setLong(3, crawl_id);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CrawlerDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, null);
        }
    }

    /**
     * updates the crawl_query_lkp table with per query statistics of fetched
     * items
     *
     * @param dataSource
     * @param crawl_id
     * @param query_id
     * @param crawl_resCnt
     */
    public synchronized static void updateQueryInfo(DataSource dataSource,
            long crawl_id,
            long query_id,
            Map<String, Integer> crawl_resCnt) {
        Connection dbCon = null;
        PreparedStatement pStmt = null;
        String sql;
        try {
            dbCon = dataSource.getConnection();
            for (Map.Entry<String, Integer> entry : crawl_resCnt.entrySet()) {
                String crawler_name = entry.getKey();
                Integer num_docs = entry.getValue();
                sql = "INSERT INTO crawl_query_lkp(crawl_id, query_id, crawler_name, num_docs) VALUES(?,?,?,?);";
                pStmt = dbCon.prepareStatement(sql);
                pStmt.setLong(1, crawl_id);
                pStmt.setLong(2, query_id);
                pStmt.setString(3, crawler_name);
                pStmt.setInt(4, num_docs);
                pStmt.executeUpdate();
            }
        } catch (NullPointerException ex) {
            Logger.getLogger(CrawlerDBA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(CrawlerDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbCon, pStmt, null);
        }
    }

    /**
     *
     * @param dataSource
     * @param crawler the crawler used for this crawl
     * @param previous the previous results for these terms, if this crawlID is
     * the first crawl (for this crawler name)
     * @return
     * @throws SQLException
     */
    public synchronized static String getJSONReport(
            DataSource dataSource,
            AbstractCrawlerController crawler,
            List<CrawlResult> previous
    ) throws SQLException {

        MapCrawlResult tmpMap = new MapCrawlResult(crawler.getEngineName());
        CrawlResult.Pair tmp_pair;
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        String SQL_SELECT = "select query_term as term, "
                + "crawl_query_lkp.crawler_name  as crawler, "
                + "crawl_query_lkp.num_docs as items "
                + "FROM crawl_query_lkp "
                + "INNER JOIN query ON crawl_query_lkp.query_id=query.id "
                + "where crawl_id = ?;";
        CrawlResult tmpRes = new CrawlResult();
        // initialize list containing all previous results, if any
        CrawlResultList crawlRes = new CrawlResultList(previous);
        // update log table with previous_docs field
        updateCrawlLogWithPreviousStats(dataSource, crawler.getEngineID(), crawlRes);
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(SQL_SELECT);
            pStmt.setLong(1, crawler.getEngineID());
            pStmt.execute();
            rSet = pStmt.getResultSet();
            String termPrev = CrawlerDBA.class.getName().trim();
            while (rSet.next()) {
                String term = rSet.getString(1);
                if (!term.equals(termPrev)) {
                    tmpRes = new CrawlResult(term);
                }
                String crawler_name = rSet.getString(2);
                int items = rSet.getInt(3);
                tmp_pair = new CrawlResult().new Pair(crawler_name, items);
                List<CrawlResult.Pair> res = tmpRes.getResults();
                if (!res.contains(tmp_pair)) {
                    tmpRes.appendResult(tmp_pair);
                }
                termPrev = term;
                if (tmpRes.getResults().size() == crawler.getCrawlersSize()) {
                    crawlRes.add(tmpRes);
                    tmpRes = new CrawlResult();
                }
            }
        } catch (SQLException ex) {
            Logger.getAnonymousLogger().severe(ex.toString());
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }

        JSONCrawlResponse response = new JSONCrawlResponse(crawler.getEngineID());
        tmpMap.setCrawlResults(crawlRes.getSyncedResults());
        response.setCrawlerResults(tmpMap);
        return response.toJSON();
    }

    private static String getCurrentTimeStamp() {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        return sdf.format(now);
    }

    /**
     * updates crawl_query_lkp table with information on fetched amounts for the
     * query id specified
     *
     * @param dbCon
     * @param crawl_id
     * @param query_id
     * @param crawl_resCnt
     */
    public static void updateQueryInfo(Connection dbCon,
            int crawl_id,
            long query_id,
            Map<String, Integer> crawl_resCnt) {
        PreparedStatement preparedStatement = null;
        String sql;
        System.out.println(crawl_resCnt.toString());
        try {
            for (Map.Entry<String, Integer> entry : crawl_resCnt.entrySet()) {
                String crawler_name = entry.getKey();
                Integer num_docs = entry.getValue();
                sql = "INSERT INTO crawl_query_lkp(crawl_id, query_id, crawler_name, num_docs) VALUES(?,?,?,?);";

                preparedStatement = dbCon.prepareStatement(sql);
                preparedStatement.setInt(1, crawl_id);
                preparedStatement.setLong(2, query_id);
                preparedStatement.setString(3, crawler_name);
                preparedStatement.setInt(4, num_docs);
                preparedStatement.addBatch();

            }

            preparedStatement.executeBatch();
            preparedStatement.clearBatch();

        } catch (NullPointerException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbCon, preparedStatement, null);
        }
    }

    /**
     * returns the language key (en,el,de,etc) by the language ID provided,
     * using the 'language' lookup table
     *
     * @param dataSource
     * @param iLangID
     * @return
     */
    public synchronized static String getLanguageKey(DataSource dataSource, int iLangID) {
        if (lang_keys.containsKey(iLangID)) {
            return lang_keys.get(iLangID);
        }
        String lang_key = "en";
        Connection dbCon = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        String SELECT = "SELECT iso_code FROM language WHERE id = ? LIMIT 1;";
        try {
            dbCon = dataSource.getConnection();
            pStmt = dbCon.prepareStatement(SELECT);
            pStmt.setInt(1, iLangID);
            rSet = pStmt.executeQuery();
            if (rSet.next()) {
                lang_key = rSet.getString(1);
                lang_keys.put(iLangID, lang_key);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CrawlerDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbCon, pStmt, rSet);
        }
        return lang_key;
    }

    /**
     * return true if it exists only one entry in crawl_log for the specified
     * crawl name
     *
     * @param dataSource
     * @param crawl_name the crawl name to check
     * @return
     */
    public static boolean isFirstCrawl(DataSource dataSource, String crawl_name) {
        boolean first = false;
        Connection dbCon = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        String SELECT = "SELECT COUNT(crawl_name) FROM crawl_log WHERE crawl_name = ? ;";
        try {
            dbCon = dataSource.getConnection();
            pStmt = dbCon.prepareStatement(SELECT);
            pStmt.setString(1, crawl_name);
            rSet = pStmt.executeQuery();
            if (rSet.next()) {
                first = rSet.getInt(1) == 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CrawlerDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbCon, pStmt, rSet);
        }
        return first;
    }

    public synchronized static List<CrawlResult> getExistingStatistics(DataSource dataSource, Set<PQuery> hsQueries) {
        List<CrawlResult> res = new ArrayList();

        Connection dbCon = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        for (PQuery query : hsQueries) {
            try {
                dbCon = dataSource.getConnection();
                // init new crawl result for this term
                String query_term = query.getQueryTerm();
                CrawlResult tmp = new CrawlResult(query_term);
                // get id, lang and execute query
                long query_id = query.getId();

                String select = "SELECT crawler_name, SUM(num_docs) as items "
                        + "FROM crawl_query_lkp "
                        + "WHERE query_id=? "
                        + "GROUP BY crawler_name ORDER BY items DESC";

                stmt = dbCon.prepareStatement(select);
                stmt.setLong(1, query_id);
                rs = stmt.executeQuery();
                // for eah source table
                while (rs.next()) {
                    CrawlResult.Pair crawler_items = new CrawlResult().new Pair(rs.getString(1), rs.getInt(2));
                    tmp.appendResult(crawler_items);
                }
                res.add(tmp);
            } catch (SQLException ex) {
                Logger.getAnonymousLogger().severe(ex.toString());
            } finally {
                SQLUtils.release(dbCon, stmt, rs);
            }
        }
        return res;
    }

    private static void updateCrawlLogWithPreviousStats(DataSource dataSource, long crawl_id, CrawlResultList crawlRes) {

        String sql = "UPDATE crawl_log SET previous_docs_sum=?  WHERE crawl_id = ?;";
        int sum_res = 0;
        for (CrawlResult crawlResult : crawlRes) {
            for (Pair p : crawlResult.getResults()) {
                sum_res += p.getResults();
            }
        }
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;
        try {
            dbConnection = dataSource.getConnection();
            preparedStatement = dbConnection.prepareStatement(sql);
            preparedStatement.setInt(1, sum_res);
            preparedStatement.setLong(2, crawl_id);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CrawlerDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbConnection, preparedStatement, null);
        }
    }

    public static int getEngineTypeId(DataSource dataSource, Engine engine) {
        synchronized (engines) {
            if (engines.containsKey(engine)) {
                return engines.get(engine);
            }
        }
        int engtype = -1;
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

        String sql = "SELECT id FROM engine_type WHERE engine_type = ? LIMIT 1;";

        Connection dbCon = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            dbCon = dataSource.getConnection();
            pStmt = dbCon.prepareStatement(sql);
            pStmt.setString(1, engine.getName());
            rs = pStmt.executeQuery();
            if (rs.next()) {
                engtype = rs.getInt(1);
                synchronized (engines) {
                    engines.put(engine, engtype);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(MonitorDBA.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbCon, pStmt, rs);
        }
        return engtype;
    }

    public enum Engine {

        CRAWL("crawl"), MONITOR("monitor");
        private String engine;

        private Engine(String engine) {
            this.engine = engine;
        }

        public String getName() {
            return engine;
        }

    }
}
