/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl.twitter.storage;

import eu.prepare.crawlers.storage.dba.CrawlerDBA;
import eu.prepare.crawlers.storage.util.SQLUtils;
import eu.prepare.crawlers.storage.demographics.DemographicsDB;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.storage.model.Storable;
import eu.prepare.crawlers.utils.CrawlerUtils;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import twitter4j.GeoLocation;
import twitter4j.HashtagEntity;
import twitter4j.Place;
import twitter4j.Status;
import twitter4j.User;

/**
 *
 * @author George K.<gkiom@iit.demokritos.gr>
 */
public class MySQLRepository extends AbstractRepository implements IRepository {

    private DataSource dataSource;
    private DemographicsDB demog;

    /**
     * on searching twitter posts, how many to fetch (avoid SQL overflows)
     */
    public static final int SEARCH_PER_QUERY_LIMIT = 1000;

    public MySQLRepository(DataSource datasource, DemographicsDB demog) {
        super();
        this.dataSource = datasource;
        this.demog = demog;
    }

    @Override
    public long insertSourceAccount(String accountName, boolean active) {
        long srcAccID = -1l;
        String SQL_INSERT = "INSERT IGNORE INTO twitter_source(account_name, active) VALUES(?, ?);";
        Connection dbConnection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            dbConnection = dataSource.getConnection();
            stmt = dbConnection.prepareStatement(SQL_INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
            stmt.setString(1, accountName);
            stmt.setInt(2, active ? 1 : 0);
            stmt.executeUpdate();
            rs = stmt.getGeneratedKeys();
            if (rs.next()) {
                srcAccID = rs.getLong(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, stmt, null);
        }
        return srcAccID;
    }

    @Override
    public Map<Long, String> getAccounts(boolean fetch_all) {
        Map<Long, String> mRes = new HashMap<Long, String>();
        Connection dbConnection = null;
        PreparedStatement stmt = null;
        ResultSet rSet = null;
        String SQL_SELECT
                = fetch_all
                ? "SELECT id, account_name FROM twitter_source;"
                : "SELECT id, account_name FROM twitter_source WHERE active = 1;";
        try {
            dbConnection = dataSource.getConnection();
            stmt = dbConnection.prepareStatement(SQL_SELECT);
            // get accounts to crawl from the database
            rSet = stmt.executeQuery();
            while (rSet.next()) {
                Long id = rSet.getLong(1);
                String name = rSet.getString(2);
                mRes.put(id, name);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, stmt, rSet);
        }
        return mRes;
    }

    @Override
    public Map<Long, String> getAccountsFromSpecifiedSources(Collection<String> sources) {
        Map<Long, String> res = new LinkedHashMap();
        long candSrcID;
        for (String candSrc : sources) {
            if (existSource(candSrc)) {
                candSrcID = getSourceAccountID(candSrc);
                if (!isActiveAccount(candSrc)) {
                    switchAccountStatus(candSrc);
                }
                res.put(candSrcID, candSrc);
            }
        }
        return res;
    }

    @Override
    public void switchAccountStatus(String account) {
        if (!existSource(account)) {
            return;
        }
        int activestatus = getAccountStatus(account);
        // switch status
        setAccountStatus(account, activestatus == 0 ? 1 : 0);
    }

    private int getAccountStatus(String account) {
        int active = 0;
        Connection dbConnection = null;
        PreparedStatement stmt = null;
        ResultSet rSet = null;
        String SQL_SELECT = "SELECT active FROM twitter_source WHERE account_name = ?;";
        try {
            dbConnection = dataSource.getConnection();
            stmt = dbConnection.prepareStatement(SQL_SELECT);
            stmt.setString(1, account);
            rSet = stmt.executeQuery();
            if (rSet.next()) {
                active = rSet.getInt(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, stmt, rSet);
        }
        return active;
    }

    private void setAccountStatus(String account, int active) {
        Connection dbConnection = null;
        PreparedStatement stmt = null;
        String SQL_SELECT = "UPDATE twitter_source SET active = ? WHERE account_name = ?;";
        try {
            dbConnection = dataSource.getConnection();
            stmt = dbConnection.prepareStatement(SQL_SELECT);
            stmt.setInt(1, active);
            stmt.setString(2, account);
            stmt.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, stmt, null);
        }
    }

    @Override
    public boolean isActiveAccount(String account) {
        return getAccountStatus(account) == 1;
    }

    @Override
    public boolean existSource(String sourceAcc) {
        boolean exists = false;
        Connection dbConnection = null;
        PreparedStatement stmt = null;
        ResultSet rSet = null;
        String SQL_SELECT = "SELECT id FROM twitter_source WHERE account_name = ?;";
        try {
            dbConnection = dataSource.getConnection();
            stmt = dbConnection.prepareStatement(SQL_SELECT);
            stmt.setString(1, sourceAcc);
            rSet = stmt.executeQuery();
            while (rSet.next()) {
                exists = (rSet.getLong(1) > 0);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, stmt, rSet);
        }
        return exists;
    }

    @Override
    public long getSourceAccountID(String sourceAcc) {
        long accID = -1l;
        Connection dbConnection = null;
        PreparedStatement stmt = null;
        ResultSet rSet = null;
        String SQL_SELECT = "SELECT id FROM twitter_source WHERE account_name = ?;";
        try {
            dbConnection = dataSource.getConnection();
            stmt = dbConnection.prepareStatement(SQL_SELECT);
            stmt.setString(1, sourceAcc);
            rSet = stmt.executeQuery();
            while (rSet.next()) {
                accID = rSet.getLong(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, stmt, rSet);
        }
        return accID;
    }

    /**
     *
     * @param user the {@link User} object to insert
     * @return the user ID generated
     */
    @Override
    public long insertUser(User user) {
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;
        ResultSet generatedKeysSet = null;
        try {
            dbConnection = dataSource.getConnection();
            prepStmt
                    = dbConnection.prepareStatement("INSERT INTO twitter_user "
                            + "(`user_id`, `followers_count`, `friends_count`, "
                            + "`listed_count`, `name`, `screen_name`, `location`, `statuses_count`, `timezone`) "
                            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prepStmt.setLong(1, user.getId());
            prepStmt.setInt(2, user.getFollowersCount());
            prepStmt.setInt(3, user.getFriendsCount());
            prepStmt.setInt(4, user.getListedCount());
            prepStmt.setString(5, user.getName());
            prepStmt.setString(6, user.getScreenName());
            prepStmt.setString(7, user.getLocation());
            prepStmt.setInt(8, user.getStatusesCount());
            prepStmt.setString(9, user.getTimeZone());
            prepStmt.executeUpdate();
            generatedKeysSet = prepStmt.getGeneratedKeys();
            generatedKeysSet.next();
            long generatedKey = generatedKeysSet.getLong(1);
            return generatedKey;
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        } finally {
            SQLUtils.release(dbConnection, prepStmt, generatedKeysSet);
        }
    }

    @Override
    public void updateUser(User user) {
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;
        try {
            dbConnection = dataSource.getConnection();
            prepStmt
                    = dbConnection.prepareStatement("UPDATE twitter_user SET followers_count = ?, "
                            + "friends_count=?, listed_count=?, location=?, statuses_count=?, "
                            + "timezone=? WHERE user_id = ?;");
            prepStmt.setInt(1, user.getFollowersCount());
            prepStmt.setInt(2, user.getListedCount());
            prepStmt.setInt(3, user.getListedCount());
            prepStmt.setString(4, user.getLocation());
            prepStmt.setInt(5, user.getStatusesCount());
            prepStmt.setString(6, user.getTimeZone());
            prepStmt.setLong(7, user.getId());
            prepStmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, prepStmt, null);
        }
    }

    @Override
    public void insertPost(Status post, long userKey, long sourceID, int followersWhenPublished, int engine_type_id, long engine_id) {
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;
        ResultSet generatedKeysSet = null;
        try {
            String sTweet = post.getText();
            // if nothing there (not really likely)
            if (sTweet == null || sTweet.trim().isEmpty()) {
                return;
            }
            Long postID = post.getId();

            dbConnection = dataSource.getConnection();
            prepStmt = dbConnection.prepareStatement(
                    "INSERT INTO twitter_post (post_id, created_at, coordinates, place, "
                    + "retweet_count, followers_when_published, text, language, url, "
                    + "twitter_user_id, twitter_source_id, engine_type_id, engine_id) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prepStmt.setLong(1, post.getId());
            Date createdAt = post.getCreatedAt();
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString = sdf.format(createdAt);
            prepStmt.setString(2, dateString);
            GeoLocation geoLocation = post.getGeoLocation();
            if (geoLocation == null) {
                prepStmt.setNull(3, Types.VARCHAR);
            } else {
                prepStmt.setString(3, post.getGeoLocation().toString());
            }
            Place place = post.getPlace();
            if (place != null) {
                String sFullName = place.getFullName();
                String sCountry = place.getCountry();
                if (sFullName != null) {
                    prepStmt.setString(4, sFullName);
                } else if (sCountry != null) {
                    prepStmt.setString(4, sCountry);
                }
            } else {
                prepStmt.setNull(4, Types.VARCHAR);
            }
            prepStmt.setLong(5, post.getRetweetCount());
            prepStmt.setInt(6, followersWhenPublished);
            prepStmt.setString(7, sTweet);
            prepStmt.setString(8, CrawlerUtils.identifyLanguage(sTweet));
            String url = "https://twitter.com/" + post.getUser().getScreenName() + "/status/" + postID;
            prepStmt.setString(9, url);
            prepStmt.setLong(10, userKey);
            prepStmt.setLong(11, sourceID);
            prepStmt.setInt(12, engine_type_id);
            prepStmt.setLong(13, engine_id);
            prepStmt.execute();
            generatedKeysSet = prepStmt.getGeneratedKeys();
            generatedKeysSet.next();
            // get post ID
            long generatedKey = generatedKeysSet.getLong(1);
            // insert hashtags in database
            for (HashtagEntity hashtagEntities : post.getHashtagEntities()) {
                insertHashtag(generatedKey, hashtagEntities.getText());
            }
            // get URL links, if there 
            List<String> lsURLs = extractor.extractURLs(sTweet);
            // unshorten URLs
            lsURLs = unshortenURLs(lsURLs);
            // insert them in the DB
            insertExternalURLs(generatedKey, lsURLs);
        } catch (SQLException e) {
//            e.printStackTrace();
            System.err.println(e.toString());
        } finally {
            SQLUtils.release(dbConnection, prepStmt, generatedKeysSet);
        }
    }

    /**
     *
     * @param postID the post ID to update
     * @param retweetCount the current retweet count
     */
    @Override
    public void updatePost(long postID, long retweetCount) {
        Connection dbConnection = null;
        PreparedStatement prepStmt = null;
        try {
            dbConnection = dataSource.getConnection();
            prepStmt
                    = dbConnection
                    .prepareStatement("UPDATE twitter_post SET retweet_count = ? WHERE post_id = ?;");
            prepStmt.setLong(1, retweetCount);
            prepStmt.setLong(2, postID);
            prepStmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, prepStmt, null);
        }
    }

    /**
     *
     * @param generatedKey the post ID
     * @param hashtag the hashtag contained in the tweet
     */
    @Override
    public void insertHashtag(long generatedKey, String hashtag) {
        Connection dbConnection = null;
        PreparedStatement insertHashtag = null;
        try {
            dbConnection = dataSource.getConnection();
            insertHashtag
                    = dbConnection.prepareStatement("INSERT IGNORE INTO twitter_hashtag (hashtag) VALUES (?);");
            insertHashtag.setString(1, hashtag);
            insertHashtag.execute();
            insertHashtag.close();
            PreparedStatement insertPostToHashtag
                    = dbConnection.prepareStatement("INSERT IGNORE INTO twitter_post_has_hashtag "
                            + "(`twitter_post_id`, `twitter_hashtag_id`) "
                            + "SELECT twitter_post.id, twitter_hashtag.id "
                            + "FROM twitter_post, twitter_hashtag "
                            + "WHERE twitter_post.id = ? AND twitter_hashtag.hashtag = ?;");
            insertPostToHashtag.setLong(1, generatedKey);
            insertPostToHashtag.setString(2, hashtag);
            insertPostToHashtag.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, insertHashtag, null);
        }
    }

    /**
     *
     * @param generatedID the ID of the tweet in twitter_post table
     * @param lsURLs the URLs that it contains
     */
    @Override
    public void insertExternalURLs(long generatedID, List<String> lsURLs) {
        if (lsURLs == null || lsURLs.isEmpty()) {
            return; // nothing to add
        }
        Connection dbConnection = null;
        PreparedStatement insStmt = null;
        try {
            dbConnection = dataSource.getConnection();
            insStmt
                    = dbConnection.prepareStatement(
                            // url, post_id are UNIQUE pair in DB
                            "INSERT IGNORE INTO twitter_external_link (url, post_id) VALUES (?,?);"
                    );
            for (String extURL : lsURLs) {
                insStmt.setString(1, extURL);
                insStmt.setLong(2, generatedID);
                // add batch to statement
                insStmt.addBatch();
            }
            insStmt.executeBatch();
        } catch (BatchUpdateException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, insStmt, null);
        }
    }

    /**
     *
     * @return total retweet count, name for each user in the DB
     */
    @Override
    public Map<Integer, String> getTotalRetweets() {

        Map<Integer, String> lRes = new LinkedHashMap<Integer, String>();

        String SQL = "select twitter_user.name, sum(twitter_post.retweet_count) as total_retweets from twitter_user "
                + "inner join "
                + "twitter_post on twitter_post.twitter_user_id = twitter_user.id "
                + "group by twitter_user.name "
                + "order by total_retweets desc;";
        Connection dbConnection = null;
        PreparedStatement selStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            selStmt
                    = dbConnection.prepareStatement(SQL);
            rSet = selStmt.executeQuery();
            while (rSet.next()) {
                Integer total = rSet.getInt(2);
                String name = rSet.getString(1);
                lRes.put(total, name);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, selStmt, rSet);
        }
        return lRes;
    }

    @Override
    public boolean existsPost(long postID) {
        boolean exists = false;
        String SELECT = "SELECT id FROM twitter_post WHERE post_id = ? LIMIT 1;";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(SELECT);
            pStmt.setLong(1, postID);
            resultSet = pStmt.executeQuery();
            if (resultSet.next()) {
                exists = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
        return exists;
    }

    @Override
    public long existsUser(long userID) {
        long exists = -1;
        String SELECT = "SELECT id FROM twitter_user WHERE user_id = ?;";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(SELECT);
            pStmt.setLong(1, userID);
            resultSet = pStmt.executeQuery();
            if (resultSet.next()) {
                exists = resultSet.getLong(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
        return exists;
    }

    @Override
    public int updateContent(long crawl_id, PQuery query, Status status) {
        int res = 0;

        String url = "https://twitter.com/" + status.getUser().getScreenName() + "/status/" + status.getId();
        Date createdAt = status.getCreatedAt();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = sdf.format(createdAt);
        String content = status.getText();
        String lang = CrawlerDBA.getLanguageKey(dataSource, query.getLanguageID());

        if (CrawlerUtils.shouldStore(content, query.getQueryTerm(), 5)) {
            try {
                res += CrawlerDBA.storeContent(
                        dataSource, url, content, timestamp, lang, TBL_TWITTER_POST, query.getId(), crawl_id);
                // store user info as well
                storeTwitterUserInfo(status.getUser().getId(), url, timestamp, TBL_TWITTER_POST, query.getId());
            } catch (ParseException e) {
                Logger.getAnonymousLogger().log(Level.WARNING, e.getMessage(), e);
            } catch (SQLException e) {
//                Logger.getAnonymousLogger().log(Level.WARNING, e.getMessage(), e);
                System.out.println(e.toString());
            }
        }

        return res;
    }

    @Override
    public void storeTwitterUserInfo(long userId, String url, String timestamp, String sourceTable, long queryId)
            throws SQLException {

        long contentID = CrawlerDBA.getContentID(dataSource,
                url, timestamp, sourceTable, queryId);
        if (contentID != 0) {
            String location = null;
            String sql = "SELECT location FROM "
                    + TBL_TWITTER_USER
                    + " WHERE id = ?;";
            Connection dbConnection = null;
            PreparedStatement pStmt = null;
            ResultSet rSet = null;
            try {
                dbConnection = dataSource.getConnection();
                pStmt = dbConnection.prepareStatement(sql);
                pStmt.setLong(1, userId);
                pStmt.execute();
                rSet = pStmt.getResultSet();
                if (rSet.next()) {
                    location = rSet.getString(1);
                }
                if (location != null) {
                    demog.insertLocation(contentID, location.trim(), Boolean.TRUE);
                }
            } finally {
                SQLUtils.release(dbConnection, pStmt, rSet);
            }
        }
    }

    @Override
    public Set<Storable> searchContent(PQuery query) {
        Set<Storable> storables = new HashSet();
        String lang = CrawlerDBA.getLanguageKey(dataSource, query.getLanguageID());
        String term = query.getQueryTerm();
        String[] all_terms = term.split("\\s+");
        long query_id = query.getId();
        String SELECT;
        boolean bLike = false;
        if (all_terms.length > 1) {
            bLike = true;
            SELECT = "SELECT "
                    + "id, url, text, created_at "
                    + "FROM twitter_post "
                    + "WHERE language = ? AND text LIKE ? "
                    + "ORDER BY id DESC LIMIT ?;";
        } else {
            SELECT = "SELECT "
                    + "id, url, text, created_at "
                    + "FROM twitter_post "
                    + "WHERE language = ? AND text REGEXP ? "
                    + "ORDER BY id DESC LIMIT ?;";
        }
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet resultSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(SELECT);
            pStmt.setString(1, lang);
            pStmt.setString(
                    2,
                    bLike
                    ? "%" + term + "%"
                    : "[[:<:]]" + term + "[[:>:]]"
            );
            pStmt.setInt(3, SEARCH_PER_QUERY_LIMIT);
            resultSet = pStmt.executeQuery();
            while (resultSet.next()) {
                String url = resultSet.getString(2);
                String clean_text = resultSet.getString(3);
                String timestamp = resultSet.getString(4);
                storables.add(new Storable(url, clean_text, timestamp, lang, TBL_TWITTER_POST, query_id));
            }
        } catch (SQLException ex) {
            Logger.getLogger(MySQLRepository.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            SQLUtils.release(dbConnection, pStmt, resultSet);
        }
        return storables;
    }

    @Override
    public int updateContent(long crawl_id, Set<Storable> to_store, String term, int min_token_cnt) {
        // for each storable, insert content
        int res = 0;
        for (Storable storable : to_store) {
            if (CrawlerUtils.shouldStore(storable.getCleanText(), term, min_token_cnt)) {
                try {

                    res += CrawlerDBA.storeContent(dataSource, storable.getUrl(), storable.getCleanText(),
                            storable.getTimestamp(), storable.getLanguage(), storable.getSourceTable(), storable.getQueryId(), crawl_id);
                } catch (SQLException ex) {
                    Logger.getLogger(MySQLRepository.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ParseException ex) {
                    Logger.getLogger(MySQLRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return res;
    }
}
