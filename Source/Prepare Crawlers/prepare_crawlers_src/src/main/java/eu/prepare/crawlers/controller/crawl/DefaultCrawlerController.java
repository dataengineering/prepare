/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.controller.crawl;

import eu.prepare.crawlers.impl.ICrawler;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.storage.dba.CrawlerDBA;
import eu.prepare.crawlers.storage.dba.QueryDBA;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import javax.sql.DataSource;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class DefaultCrawlerController extends AbstractCrawlerController {

    public DefaultCrawlerController(DataSource dataSource, String engine_name, long engine_id) {
        super(dataSource, engine_name, engine_id);
    }

    @Override
    public int executeCrawl(final PQuery qQuery, int query_time_limit) throws IllegalArgumentException {
        final AtomicInteger res = new AtomicInteger();

        final Map<String, Integer> crawl_resCnt
                = Collections.synchronizedMap(new HashMap<String, Integer>());
        // perform multithreaded crawl. Each query at a time, all services
        // serve this query. Time of query loop = time of the slowest service.
        // init executor service
        int iThreads = crawler_impls.size();
        ExecutorService es;
        if (iThreads <= 0) {
            throw new IllegalArgumentException("No Crawlers have been specified to run");
        } else if (iThreads == 1) {
            es = Executors.newSingleThreadExecutor();
        } else {
            es = // init a thread for each crawler
                    Executors.newFixedThreadPool(iThreads);
        }
        // log start time in seconds
        long start_time = TimeUnit.MILLISECONDS.toSeconds(new Date().getTime());
        // for each service needed, pass query.
        for (final ICrawler crawler : crawler_impls) {
            es.submit(new Runnable() {
                @Override
                public void run() {
                    String sName = crawler.toString();
                    int iResults = crawler.crawl(engine_id, qQuery);
                    synchronized (crawl_resCnt) {
                        crawl_resCnt.put(sName, iResults);
                        res.addAndGet(iResults);
                    }
                    // debug
//                    LOGGER.log(Level.INFO, "finished {0} for query id {1}", new Object[]{sName, qQuery.getId()});
                }
            });
        }
        // Await completion
        es.shutdown();
        try {
            boolean terminatedNormally = es.awaitTermination(query_time_limit, TimeUnit.MINUTES);
            if (!terminatedNormally) {
                LOGGER.log(Level.INFO, "terminated execution in {0} minutes", query_time_limit);
            }
        } catch (InterruptedException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        // update query execution times
        QueryDBA.updateQueryExecTimes(dataSource, qQuery.getId());
        // update query data fetched
        CrawlerDBA.updateQueryInfo(dataSource, engine_id, qQuery.getId(), crawl_resCnt);
        // log end time
        long end_time = TimeUnit.MILLISECONDS.toSeconds(new Date().getTime());
        // verbose
        LOGGER.log(Level.INFO, "<CrawlID {0}>: Query: ''{1}'' finished at {2} with results: {3} - Sum: {4} - duration: {5} secs",
                new Object[]{engine_id, qQuery.getQueryTerm(), new Date(), crawl_resCnt.toString(), res.intValue(), (end_time - start_time)});
        return res.intValue();
    }

    @Override
    public Map<String, Integer> executeMonitor(final Map<String, Collection<String>> sources) throws IllegalArgumentException {
        final Map<String, Integer> results = Collections.synchronizedMap(new TreeMap());
        int iThreads = crawler_impls.size();
        ExecutorService es;
        if (iThreads <= 0) {
            throw new IllegalArgumentException("No Crawlers have been specified to run");
        } else if (iThreads == 1) {
            es = Executors.newSingleThreadExecutor();
        } else {
            es = // init a thread for each crawler
                    Executors.newFixedThreadPool(iThreads);
        }
        for (final ICrawler crawler : crawler_impls) {
            es.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        // monitor
                        int ires = crawler.monitor(engine_id, sources.get(crawler.toString()));
                        synchronized (results) {
                            results.put(crawler.toString(), ires);
                        }
                    } catch (UnsupportedOperationException ex) {
                        // pass
                    }
                }
            });
        }
        // Await completion
        es.shutdown();
        try {
            es.awaitTermination(2, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
        }
        return results;
    }

    /**
     * backup and emergency purposes only. Will execute all crawlers serially
     *
     * @param qQuery
     */
    @Override
    public void executeSingleThreadCrawl(PQuery qQuery) {
        final Map<String, Integer> crawl_resCnt = new HashMap<String, Integer>();
        // if we want to ignore exact matching
        String sQueryTerm = qQuery.getQueryTerm();
        LOGGER.log(Level.INFO, "query: ''{0}'' started at {1}", new Object[]{sQueryTerm, new Date()});
        //////////////////////
        // for each service needed, pass query.
        for (final ICrawler crawler : crawler_impls) {
            LOGGER.log(Level.INFO, "Currently Running {0}", crawler.toString());
            String sName = crawler.toString();
            int iResults = crawler.crawl(engine_id, qQuery);
            crawl_resCnt.put(sName, iResults);
        }
        // update query execution times.
        QueryDBA.updateQueryExecTimes(dataSource, qQuery.getId());
        // update query data fetched
        CrawlerDBA.updateQueryInfo(dataSource, engine_id, qQuery.getId(), crawl_resCnt);
        LOGGER.log(Level.INFO, "query: ''{0}'' finished at {1}", new Object[]{sQueryTerm, new Date()});
    }

//    @Override
//    public void run(Set<PQuery> queries, int engine_id, int query_time_limit) {
//        int iThreads = crawler_impls.size();
//        ExecutorService es;
//        if (iThreads <= 0) {
//            throw new IllegalArgumentException("No Crawlers have been specified to run");
//        } else if (iThreads == 1) {
//            es = Executors.newSingleThreadExecutor();
//        } else {
//            es = // init a thread for each crawler
//                    Executors.newFixedThreadPool(iThreads);
//        }
//
//        final Map<String, Integer> crawl_resCnt
//                = Collections.synchronizedMap(new HashMap<String, Integer>());
//        // for each crawler
//        for (final ICrawler crawler : crawler_impls) {
//            // for each query
//            for (final PQuery qQuery : queries) {
//                // crawl the query with the crawler
//                es.submit(new Runnable() {
//                    @Override
//                    public void run() {
//                        String sName = crawler.toString();
//                        int iResults = crawler.crawl(qQuery);
//                        synchronized (crawl_resCnt) {
//                            crawl_resCnt.put(sName, iResults);
//                            res.addAndGet(iResults);
//                        }
//                    }
//                });
//            }
//        }
//    }
}
