package eu.prepare.crawlers.impl.youtube;


import com.google.gdata.client.youtube.*;
import com.google.gdata.data.youtube.*;
import com.google.gdata.util.*;
import eu.prepare.crawlers.impl.AbstractCrawler;
import eu.prepare.crawlers.impl.ICrawler;
import eu.prepare.crawlers.storage.model.PQuery;
import eu.prepare.crawlers.utils.CrawlerUtils;

import eu.prepare.crawlers.impl.youtube.storage.YTSQLStorage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.sql.DataSource;

//
//import com.google.api.client.googleapis.json.GoogleJsonResponseException;
//import com.google.api.client.http.HttpRequest;
//import com.google.api.client.http.HttpRequestInitializer;
//import com.google.api.client.http.javanet.NetHttpTransport;


public class YoutubeCrawler extends AbstractCrawler implements ICrawler {

    private YouTubeService service;
    private YTSQLStorage storage;

    private int MAX_VIDEOS = 20;
    private int MAX_COMMENTS_PER_VIDEO = 20;

    public YoutubeCrawler(String youtubeAppName, String youtubeDevID,
            DataSource dataSource, int min_tokens) {
        super(dataSource, min_tokens);
        this.service = new YouTubeService(youtubeAppName, youtubeDevID);
        // init demographics storage
        this.storage = new YTSQLStorage(dataSource, min_tokens);
    }

    @Override
    public int crawl(long crawl_id, PQuery qQuery) {
        return crawl(qQuery.getQueryTerm(), qQuery.getId(), crawl_id);
    }
//    private static YouTube youtube;
//
//    protected int test(String query) {
//        try {
//            // This object is used to make YouTube Data API requests. The last
//            // argument is required, but since we don't need anything
//            // initialized when the HttpRequest is initialized, we override
//            // the interface and provide a no-op function.
//            youtube = new YouTube.Builder(new NetHttpTransport(), , new HttpRequestInitializer() {
//                public void initialize(HttpRequest request) throws IOException {
//                }
//            }).setApplicationName("prepare_youtube_search").build();
//
//            // Prompt the user to enter a query term.
//            String queryTerm = query;
//
//            // Define the API request for retrieving search results.
//            YouTube.Search.List search = youtube.search().list("id,snippet");
//
//            // Set your developer key from the Google Developers Console for
//            // non-authenticated requests. See:
//            // https://console.developers.google.com/
//            String apiKey = properties.getProperty("youtube.apikey");
//            search.setKey(apiKey);
//            search.setQ(queryTerm);
//
//            // Restrict the search results to only include videos. See:
//            // https://developers.google.com/youtube/v3/docs/search/list#type
//            search.setType("video");
//
//            // To increase efficiency, only retrieve the fields that the
//            // application uses.
//            search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url)");
//            search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);
//
//            // Call the API and print results.
//            SearchListResponse searchResponse = search.execute();
//            List<SearchResult> searchResultList = searchResponse.getItems();
//            if (searchResultList != null) {
//                prettyPrint(searchResultList.iterator(), queryTerm);
//            }
//        } catch (GoogleJsonResponseException e) {
//            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
//                    + e.getDetails().getMessage());
//        } catch (IOException e) {
//            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
//        } catch (Throwable t) {
//            t.printStackTrace();
//        }
//        return 4;
//    }

    protected int crawl(String query, long queryID, long crawl_id) {
        int res = 0;
        try {
            YouTubeQuery youtubeQuery = new YouTubeQuery(new URL("http://gdata.youtube.com/feeds/api/videos"));

            youtubeQuery.setFullTextQuery(query);
            youtubeQuery.setSafeSearch(YouTubeQuery.SafeSearch.NONE);

            byte videoLoops = 0;
            List<Long> newCommentIDs = new ArrayList<Long>();
            VideoFeed videoFeed = service.query(youtubeQuery, VideoFeed.class);
            do {
                for (VideoEntry videoEntry : videoFeed.getEntries()) {

                    try {
                        YouTubeMediaGroup mediaGroup = videoEntry.getMediaGroup();
                        String videoId = mediaGroup.getVideoId();
                        // get User 
                        String userID = mediaGroup.getUploader();
                        // get user profile entry
                        UserProfileEntry user
                                = service.getEntry(new URL("http://gdata.youtube.com/feeds/api/users/" + userID),
                                        UserProfileEntry.class);
                        // insert user
                        storage.insertUser(user);
                        // get user Key
                        long userKey = storage.getUserKey(userID);
                        // get video key if exists
                        Long videoKey = storage.getVideoKey(videoId, queryID);
                        // if exists, update, else insert
                        if (videoKey != null) {
                            storage.updateVideo(videoKey, mediaGroup.getDescription(),
                                    videoEntry.getRating(), videoEntry.getStatistics(),
                                    videoEntry.getUpdated());
                        } else {
                            storage.insertVideo(videoEntry, queryID, userKey);
                        }
                        //get comments
                        try { // if comments are disabled or 0 (unchecked) -> throws NullPointerException
                            byte commentLoops = 0;
                            String commentsUrl = videoEntry.getComments().getFeedLink().getHref();
                            CommentFeed commentFeed = service.getFeed(new URL(commentsUrl), CommentFeed.class);
                            //String videoID = mediaGroup.getVideoId();
                            //long videoKey = getVideoKey(videoId);
                            do {
                                for (CommentEntry comment : commentFeed.getEntries()) {
                                    // if comment not spam
                                    if (!comment.hasSpamHint()) {
                                        Long commentKey = storage.getCommentKey(comment.getId(), queryID);
                                        if (commentKey != null) {
                                            storage.updateComment(commentKey, comment);
                                        } else {
                                            // get comment author
                                            if (CrawlerUtils.shouldStore(comment.getPlainTextContent(), query, min_tokens)) {
                                                UserProfileEntry commentAuthor
                                                        = service.getEntry(new URL(comment.getAuthors().get(0).getUri()),
                                                                UserProfileEntry.class);
                                                storage.insertUser(commentAuthor);
                                                String commentAuthorID = commentAuthor.getUsername();
                                                long commentAuthorKey = storage.getUserKey(commentAuthorID);
                                                // store if OK
                                                newCommentIDs.add(storage.insertComment(comment, videoId, commentAuthorKey, queryID));
                                            }
                                        }
                                    }
                                }
                                if (commentFeed.getNextLink() != null) {
                                    commentFeed = service.getFeed(new URL(commentFeed.getNextLink().getHref()), CommentFeed.class);
                                } else {
                                    commentFeed = null;
                                }
                                commentLoops++;
                                // get at most max comments per video
                            } while (commentFeed != null && commentLoops <= MAX_COMMENTS_PER_VIDEO);

                        } catch (NullPointerException e) {
                            // System.out.println("no comments");
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (ResourceNotFoundException e) {
                            // user not found
                        } catch (ServiceForbiddenException e) {
                            // User account suspended
                        } catch (ServiceException e) {
                            e.printStackTrace();
                        } // finished comments
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ServiceException e) {
                        e.printStackTrace();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                if (videoFeed.getNextLink() != null) {
                    videoFeed = service.getFeed(new URL(videoFeed.getNextLink().getHref()), VideoFeed.class);
                } else {
                    videoFeed = null;
                }
                videoLoops++;
            } while (videoFeed != null && videoLoops <= MAX_VIDEOS); // get at most max videos
            // move comment entries to content table
            res += storage.moveContent(crawl_id, queryID, query, newCommentIDs.size());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    @Override
    public int monitor(long monitor_id, Collection<String> sources) {
        throw new UnsupportedOperationException("Not supported");
    }
}
