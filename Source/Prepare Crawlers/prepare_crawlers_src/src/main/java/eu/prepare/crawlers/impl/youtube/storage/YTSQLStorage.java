/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl.youtube.storage;

import com.google.gdata.data.DateTime;
import com.google.gdata.data.extensions.Rating;
import com.google.gdata.data.media.mediarss.MediaCategory;
import com.google.gdata.data.media.mediarss.MediaDescription;
import com.google.gdata.data.media.mediarss.MediaTitle;
import com.google.gdata.data.youtube.CommentEntry;
import com.google.gdata.data.youtube.UserProfileEntry;
import com.google.gdata.data.youtube.VideoEntry;
import com.google.gdata.data.youtube.YouTubeMediaGroup;
import com.google.gdata.data.youtube.YtStatistics;
import eu.prepare.crawlers.storage.dba.CrawlerDBA;
import eu.prepare.crawlers.utils.CrawlerUtils;
import eu.prepare.crawlers.storage.util.SQLUtils;
import eu.prepare.crawlers.storage.demographics.DemographicsDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public class YTSQLStorage {

    private DataSource dataSource;
    private DemographicsDB demogStore;
    private int min_tokens;

    public YTSQLStorage(DataSource dataSource, int min_tokens) {
        this.dataSource = dataSource;
        this.min_tokens = min_tokens;
        this.demogStore = new DemographicsDB(dataSource);
    }

    public int moveContent(long crawl_id, long queryId, String query, int maxtop) throws SQLException {
        int cntRes = 0;
        String sourceTable = "youtube_comment";
        String sql = "SELECT id, url, content, date, language, youtube_user_id "
                + "FROM youtube_comment "
                + "WHERE query_id = ? "
                + "ORDER BY id DESC "
                + "LIMIT ?;";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setLong(1, queryId);
            pStmt.setInt(2, maxtop);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            while (rSet.next()) {
                //long sourceId = resultSet.getLong("id");
                String url = rSet.getString("url");
                String clean_text = rSet.getString("content");
                String timestamp = rSet.getString("date");
                String language = rSet.getString("language");
                long userId = rSet.getLong("youtube_user_id");

                if (CrawlerUtils.shouldStore(clean_text, query, min_tokens)) {

                    try {
                        cntRes
                                += CrawlerDBA.storeContent(dataSource, url,
                                        clean_text, timestamp, language, sourceTable, queryId, crawl_id);
                        storeYoutubeUserInfo(userId, url, timestamp, sourceTable, queryId);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            return cntRes;
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
    }

    public void storeYoutubeUserInfo(long userId, String url, String timestamp,
            String sourceTable, long queryId)
            throws SQLException {

        long nomadContentId
                = CrawlerDBA.getContentID(dataSource, url, timestamp, sourceTable, queryId);
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        if (nomadContentId != 0) {
            String sql = "SELECT age, gender, location "
                    + "FROM youtube_user "
                    + "WHERE id = ?;";
            try {
                dbConnection = dataSource.getConnection();
                pStmt = dbConnection.prepareStatement(sql);
                pStmt.setLong(1, userId);
                pStmt.execute();
                rSet = pStmt.getResultSet();
                if (rSet.next()) {

                    Integer age = rSet.getInt("age");
                    String gender = rSet.getString("gender");
                    String location = rSet.getString("location");

                    if (age != null && age != 0) {
                        demogStore.insertAge(nomadContentId, age, Boolean.TRUE);
                    }

                    if (gender != null) {
                        demogStore.insertGender(nomadContentId, gender, Boolean.TRUE);
                    }

                    if (location != null) {
                        demogStore.insertLocation(nomadContentId, location.trim(), Boolean.TRUE);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                SQLUtils.release(dbConnection, pStmt, rSet);
            }
        }
    }

    public void insertVideo(VideoEntry videoEntry, long queryID, long userKey) throws SQLException {
        /*try {*/
        YouTubeMediaGroup mediaGroup = videoEntry.getMediaGroup();

        String videoID = mediaGroup.getVideoId();

        String sql = "INSERT INTO youtube_video "
                + "(video_id, title, description, average_rating, view_count, favorite_count, location, publiced, recorded, updated, youtube_user_id, query_id) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
                + "ON DUPLICATE KEY UPDATE description=?, average_rating=?, view_count=?, favorite_count=?, updated=?;";

        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);

            pStmt.setString(1, videoID);
            MediaTitle mediaTitle = mediaGroup.getTitle();
            if (mediaTitle != null) {
                pStmt.setString(2, mediaTitle.getPlainTextContent());
            } else {
                pStmt.setNull(2, Types.VARCHAR);
            }
            if (mediaGroup.getDescription() != null) {
                pStmt.setString(3, mediaGroup.getDescription().getPlainTextContent());
                pStmt.setString(13, mediaGroup.getDescription().getPlainTextContent());
            } else {
                pStmt.setNull(3, Types.VARCHAR);
                pStmt.setNull(13, Types.VARCHAR);
            }
            if (videoEntry.getRating() != null) {
                pStmt.setFloat(4, videoEntry.getRating().getAverage());
                pStmt.setFloat(14, videoEntry.getRating().getAverage());
            } else {
                pStmt.setNull(4, Types.FLOAT);
                pStmt.setNull(14, Types.FLOAT);
            }
            Long viewCount = null;
            Long favoriteCount = null;
            YtStatistics stats = videoEntry.getStatistics();
            if (stats != null) {
                viewCount = stats.getViewCount();
                favoriteCount = stats.getFavoriteCount();
            }
            if (viewCount != null) {
                pStmt.setLong(5, viewCount);
                pStmt.setLong(15, viewCount);
            } else {
                pStmt.setNull(5, Types.BIGINT);
                pStmt.setNull(15, Types.BIGINT);
            }
            if (favoriteCount != null) {
                pStmt.setLong(6, favoriteCount);
                pStmt.setLong(16, favoriteCount);
            } else {
                pStmt.setNull(6, Types.BIGINT);
                pStmt.setNull(16, Types.BIGINT);
            }
            pStmt.setString(7, videoEntry.getLocation());
            pStmt.setString(8, videoEntry.getPublished().toUiString());
            if (videoEntry.getRecorded() != null) {
                pStmt.setString(9, videoEntry.getRecorded().toUiString());
            } else {
                pStmt.setNull(9, Types.VARCHAR);
            }
            if (videoEntry.getUpdated() != null) {
                pStmt.setString(10, videoEntry.getUpdated().toUiString());
                pStmt.setString(17, videoEntry.getUpdated().toUiString());
            } else {
                pStmt.setNull(10, Types.VARCHAR);
                pStmt.setNull(17, Types.VARCHAR);
            }
            pStmt.setLong(11, userKey);
            pStmt.setLong(12, queryID);
            pStmt.executeUpdate();
            rSet = pStmt.getGeneratedKeys();
            if (rSet.next()) {
                long VKey = rSet.getLong(1);
                //insert categories
                List<MediaCategory> categories = mediaGroup.getCategories();
                if (mediaGroup.getCategories() != null) {
                    for (MediaCategory category : categories) {
                        String label = category.getLabel();
                        if (label != null) {
                            insertCategory(label, VKey);
                            insertVideoToCategoryRelation(label, VKey);
                        }
                    }
                }
            }
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
    }

    public Long insertComment(CommentEntry comment, String videoId, long userKey, long queryID) throws SQLException {
        Long id = null;
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rs = null;
        try {
            // get Plain text from comment
            String content = comment.getPlainTextContent();

            Integer totalRating = comment.getTotalRating();
            String language = CrawlerUtils.identifyLanguage(content);

            String sql = "INSERT INTO "
                    + "youtube_comment (comment_id, url, content, date, rating, language, youtube_video_id, youtube_user_id, query_id)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) "
                    + "ON DUPLICATE KEY UPDATE content=?, rating=?, language=?;";

            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            pStmt.setString(1, comment.getId());
            pStmt.setString(2, comment.getHtmlLink().getHref()); // contains the URL of the video that the comment belongs to. 
            // There is no persistent link to a youtube comment.
//            pStmt.setString(2, comment.getSelfLink().getHref()); // is not a URL link... but a gdata feed item. Contains the actual comment though
            //String content = comment.getPlainTextContent();
            pStmt.setString(3, content);
            pStmt.setString(4, comment.getPublished().toUiString());
            //Integer totalRating = comment.getTotalRating();
            if (totalRating != null) {
                pStmt.setInt(5, totalRating);
            } else {
                pStmt.setNull(5, Types.INTEGER);
            }
            //String language = CrawlerUtils.identifyLanguage(content);
            pStmt.setString(6, language);
            pStmt.setLong(7, getVideoKey(videoId, queryID));
            pStmt.setLong(8, userKey);
            pStmt.setLong(9, queryID);

            pStmt.setString(10, content);
            if (totalRating != null) {
                pStmt.setInt(11, totalRating);
            } else {
                pStmt.setNull(11, Types.INTEGER);
            }
            pStmt.setString(12, language);
            pStmt.executeUpdate();
            rs = pStmt.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getLong(1); // last insert ID
            }
        } finally {
            SQLUtils.release(dbConnection, pStmt, rs);
        }
        return id;
    }

    public void insertUser(UserProfileEntry user) throws SQLException {
        String userID = user.getUsername();
        Short age = null;
        if (user.getAge() != null) {
            age = user.getAge().shortValue();
        }
        String gender = null;
        if (user.getGender() != null) {
            gender = user.getGender().toString();
        }
        String hometown = user.getHometown();
        String location = user.getLocation();
        String occupation = user.getOccupation();
        String relationship = null;
        if (user.getRelationship() != null) {
            relationship = user.getRelationship().toString();
        }
        String school = user.getSchool();

        String sql = "INSERT INTO youtube_user "
                + "(user_id, age, gender, hometown, location, occupation, relationship, school)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?) "
                + "ON DUPLICATE KEY UPDATE age=?, gender=?, hometown=?, location=?, occupation=?, relationship=?, school=?";

        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);

            pStmt.setString(1, userID);
            if (age == null) {
                pStmt.setNull(2, Types.SMALLINT);
            } else {
                pStmt.setShort(2, age);
            }
            pStmt.setString(3, gender);
            pStmt.setString(4, hometown);
            pStmt.setString(5, location);
            pStmt.setString(6, occupation);
            pStmt.setString(7, relationship);
            pStmt.setString(8, school);

            if (age == null) {
                pStmt.setNull(9, Types.SMALLINT);
            } else {
                pStmt.setShort(9, age);
            }
            pStmt.setString(10, gender);
            pStmt.setString(11, hometown);
            pStmt.setString(12, location);
            pStmt.setString(13, occupation);
            pStmt.setString(14, relationship);
            pStmt.setString(15, school);

            pStmt.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
    }

    public void insertCategory(String category, long videoKey) {
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {
            String sql = "INSERT IGNORE INTO youtube_category(category) VALUES (?)";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, category);
            pStmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    public void insertVideoToCategoryRelation(String category, long videoKey) {
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {
            long commentKey = getCategoryKey(category);
            dbConnection = dataSource.getConnection();
            String sql2 = "INSERT IGNORE INTO youtube_video_has_category "
                    + "(youtube_video_id, youtube_category_id) "
                    + "VALUES (?, ?);";
            pStmt = dbConnection.prepareStatement(sql2);
            pStmt.setLong(1, videoKey);
            pStmt.setLong(2, commentKey);
            pStmt.executeUpdate();
        } catch (SQLException ee) {
            ee.printStackTrace();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    public Long getCategoryKey(String category) throws SQLException {
        PreparedStatement pStmt = null;
        Connection dbConnection = null;
        ResultSet rSet = null;
        Long id = null;
        try {
            String sql = "SELECT id FROM youtube_category WHERE category = ?";
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, category);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            rSet.next();
            id = rSet.getLong("id");
            return id;
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
    }

    public void updateVideo(long id, MediaDescription description, Rating rating, YtStatistics stats, DateTime updated) throws SQLException {
        String sql = "UPDATE youtube_video SET description=?, average_rating=?, view_count=?, favorite_count=?, updated=? WHERE id = ?";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            if (description != null) {
                pStmt.setString(1, description.getPlainTextContent());
            } else {
                pStmt.setNull(1, Types.VARCHAR);
            }
            if (rating != null) {
                pStmt.setFloat(2, rating.getAverage());
            } else {
                pStmt.setNull(2, Types.FLOAT);
            }
            Long viewCount = null;
            Long favoriteCount = null;
            if (stats != null) {
                viewCount = stats.getViewCount();
                favoriteCount = stats.getFavoriteCount();
            }
            if (viewCount != null) {
                pStmt.setLong(3, viewCount);
            } else {
                pStmt.setNull(3, Types.BIGINT);
            }
            if (favoriteCount != null) {
                pStmt.setLong(4, favoriteCount);
            } else {
                pStmt.setNull(4, Types.BIGINT);
            }
            if (updated != null) {
                pStmt.setString(5, updated.toUiString());
            } else {
                pStmt.setNull(5, Types.VARCHAR);
            }
            pStmt.setLong(6, id);
            pStmt.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    public void updateComment(long id, CommentEntry comment) throws SQLException {
        // get Plain text from comment
        String content = comment.getPlainTextContent();
        Integer totalRating = comment.getTotalRating();
        String language = CrawlerUtils.identifyLanguage(content);

        String sql = "UPDATE youtube_comment SET content=?, rating=?, language=? WHERE id = ?";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;

        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, content);
            if (totalRating != null) {
                pStmt.setInt(2, totalRating);
            } else {
                pStmt.setNull(2, Types.INTEGER);
            }
            pStmt.setString(3, language);
            pStmt.setLong(4, id);
            pStmt.executeUpdate();
        } finally {
            SQLUtils.release(dbConnection, pStmt, null);
        }
    }

    public Long getUserKey(String userID) throws SQLException {
        Long id = null;
        String sql = "SELECT id FROM youtube_user WHERE user_id = ?";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, userID);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            rSet.next();
            id = rSet.getLong(1);
            return id;
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
    }

    public Long getVideoKey(String videoID, long queryID) throws SQLException {
        Long id = null;
        String sql = "SELECT id FROM youtube_video WHERE video_id = ? AND query_id = ?";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, videoID);
            pStmt.setLong(2, queryID);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            if (rSet.next()) {
                id = rSet.getLong("id");
            }
            return id;
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
    }

    public Long getCommentKey(String commentID, long queryID) throws SQLException {
        Long id = null;
        String sql = "SELECT id FROM youtube_comment WHERE comment_id = ? AND query_id = ?";
        Connection dbConnection = null;
        PreparedStatement pStmt = null;
        ResultSet rSet = null;
        try {
            dbConnection = dataSource.getConnection();
            pStmt = dbConnection.prepareStatement(sql);
            pStmt.setString(1, commentID);
            pStmt.setLong(2, queryID);
            pStmt.execute();
            rSet = pStmt.getResultSet();
            if (rSet.next()) {
                id = rSet.getLong("id");
            }
            return id;
        } finally {
            SQLUtils.release(dbConnection, pStmt, rSet);
        }
    }

}
