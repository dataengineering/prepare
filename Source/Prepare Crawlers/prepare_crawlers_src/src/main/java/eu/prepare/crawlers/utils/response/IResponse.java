/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package eu.prepare.crawlers.utils.response;

/**
 *
 * @author George K. <gkiom@scify.org>
 */
public interface IResponse {

    String toJSON();
    
}
