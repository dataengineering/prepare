/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.prepare.crawlers.impl;

import eu.prepare.crawlers.storage.model.PQuery;
import java.util.Collection;

/**
 *
 * @author George K. <gkiom@iit.demokritos.gr>
 */
public interface ICrawler {

    /**
     *
     * @param crawl_id the unique id of the crawl execution
     * @param query The {@link PQuery} to crawl.
     * @return The number of results fetched for that query, by the respective
     * crawler.
     */
    int crawl(long crawl_id, PQuery query);

    /**
     * will monitor from specified sources, if any, or from all sources that are
     * marked as active
     *
     * @param moninor_id
     * @param sources the sources to monitor, may be null to monitor all active
     * sources
     * @return the number of items fetched at the current run
     */
    int monitor(long moninor_id, Collection<String> sources);
}
