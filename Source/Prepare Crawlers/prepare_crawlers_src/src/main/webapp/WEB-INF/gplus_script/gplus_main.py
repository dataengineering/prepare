# -*- coding: utf-8 -*-

"""Command-line script to retrieve Google+ data.

Command-line script that given a database name containing a table with query
strings it retrieves user posts in Google+ that contain the query strings and
the comments for each retrieved post (along with information such as the user
id, the created and updated dates, etc.).

Usage:
  $ python gplus_main.py --db_host=localhost --db_user=root --db_pwd=nomad123 \
  --query_db_name=QueriesDB --query_table=Queries --output_file=data.txt \
  --max_activities=20 --max_comments=100

If only
  $ python gplus_main.py
is used then the default values for the flags are used.

To run the script a number of packages need to be installed:
  $ sudo easy_install python-gflags
    $ sudo easy_install httplib2
  $ sudo easy_install MySQL-python
  $ sudo easy_install google-api-python-client

If sudo easy_install MySQL-python fails with a message of
  mysql_config: command not found
a possible solution may come by,
  export PATH=$PATH:/usr/local/mysql/bin/

In MacOS, if sudo easy_install MySQL-python fails with a message of
  unable to execute clang: No such file or directory
Install latest Xcode version and, then:
  - Open Xcode
  - Go to "Preferences"
  - Select "Download" tab
  - Install Command Line Tools 


For the boilerpipe:

Install the jcc package included in the official repositories:
  $ sudo apt-get install jcc

Download the most recent binary tarball (for example, version 1.2.0).

Extract the downloaded file:

  $ tar xvzf boilerpipe-*.tar.gz
  $ cd boilerpipe-*

Compile the jar file into a Python module:

  $ python -m jcc \
    --jar boilerpipe-1.2.0.jar \
    --classpath lib/nekohtml-1.9.13.jar \
    --classpath lib/xerces-2.9.1.jar \
    --package java.net \
    java.net.URL \
    --python boilerpipe --build --install
"""

__author__ = 'ekanou@gmail.com (Evangelos Kanoulas)'

import gflags
import httplib2
import logging
import os
import sys
import codecs
import json
# import MySQLdb as mdb

from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import AccessTokenRefreshError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.tools import run

import gplus as gplus

reload(sys)
sys.setdefaultencoding('utf-8')
UTF8Writer = codecs.getwriter('utf-8')
sys.stdout = UTF8Writer(sys.stdout)

FLAGS = gflags.FLAGS

# The gflags module makes defining command-line options easy for
# applications. Run this program with the '--help' argument to see
# all the flags that it understands.
gflags.DEFINE_enum('logging_level',
                    #'WARNING',
                     'INFO',
                   ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                   'Set the level of logging detail.')

gflags.DEFINE_string('output_file',
                     'data.txt',
                     'The file to save the acquired data.')

gflags.DEFINE_integer('max_activities',
                      5,
                      'max number of returned Google+ posts, multiples of 20')

gflags.DEFINE_integer('max_comments',
                      5,
                      'max number of comments returned on Google+ posts,' 
                      'multiples of 20')

gflags.DEFINE_bool('get_attachment',
                   False,
                   'get any attached to the post article')

gflags.DEFINE_bool('get_user',
                   True,
                   'get user data')

gflags.DEFINE_string('db_host',
                     'localhost',
                     'The host where the MySQL database is located.')

gflags.DEFINE_string('db_user',
                     'root',
                     'The database user name.')

gflags.DEFINE_string('db_pwd',
                     'nomad123',
                     'The user''s account password.')

gflags.DEFINE_string('query_db_name',
                     'QueriesDB',
                     'The database name.')
                    
gflags.DEFINE_string('query_table',
                     'Queries',
                     'The name of the table that contains the query strings.')

# Added 'query_string' flag, in order to pass it as a parameter from caller
                    
gflags.DEFINE_string('query_string',
                     'Offene Daten',
                     'The name of the search query.')
# the API dev Key
gflags.DEFINE_string('gplus_API_Key',
        		      '123',
        		      'The developer key')

def main(argv):

    # Let the gflags module process the command-line arguments
    argv = FLAGS(argv)
    
    try:
        argv = FLAGS(argv)
    except gflags.FlagsError as e:
        print('%s\\nUsage: %s ARGS\\n%s' % (e, argv[0], FLAGS))
        sys.exit(1)

    # Set the logging according to the command-line flag
    logging.getLogger().setLevel(getattr(logging, FLAGS.logging_level))
    
    # Obtain developer Key
    devKey = FLAGS.gplus_API_Key
    if devKey.strip() == "": # Start with oAuth
        gpluscrawler = gplus.PlusDataRetriever(FLAGS.max_activities, FLAGS.max_comments, FLAGS.get_attachment, FLAGS.get_user)
    else: # start with API Key
        # print 'API KEY: %s' %(devKey)
        gpluscrawler = gplus.PlusDataRetriever(FLAGS.max_activities, FLAGS.max_comments, FLAGS.get_attachment, FLAGS.get_user, devKey)

    # get working Dir
    WORKINGDIR = os.path.dirname(__file__)
#     con = None
# 
#     try:
#         # The parameters are: (a) host where the MySQL database is located,
#         # (b) the database user name, (c) the user's account password, and
#         # (d) the database name. E.g.
#         # con = mdb.connect('localhost', 'root', 'nomad123', 'QueriesDB');
#         con = mdb.connect(FLAGS.db_host, FLAGS.db_user,
#                             FLAGS.db_pwd, FLAGS.query_db_name,
#                             charset="utf8", use_unicode=True)
# 
#         # query_term is the table column we are interested in.
#         # FLAGS.query_table is the database table.
#         cur = con.cursor()
#         cur.execute("SELECT query_term FROM " + FLAGS.query_table)
#         
# #         queries = cur.fetchall()
#         # debug
#         queries = cur.fetchmany(2)
#         # debug
#         for q in queries: print queries.index(q), " : ", q
#         # debug
#     except mdb.Error as e:
#         print("Error %d: %s" % (e.args[0], e.args[1]))
#         sys.exit(1)
#     finally:	
#         if con:	
#             con.close()

    try:
        output = []
        # for each query in the table (coming from caller of script)
        query_line = FLAGS.query_string
        if not isinstance(query_line, str):
            query = query_line[0]
        else:
            query = query_line
        # print "Searching for term \'%s\' in gplus activities..." %query
#         with codecs.open(FLAGS.output_file, 'w', encoding='utf-8') as fout:
        with codecs.open(os.path.join(os.path.dirname(__file__), FLAGS.output_file), 'w', encoding='utf-8') as fOut:
            results = {'query': query}
            activities = gpluscrawler.GetActivitiesByTermSearch(query_term=query)
            results['results'] = activities
            output.append(results)
            # Write to file
            fOut.write(json.dumps(output, ensure_ascii=False))
    except AccessTokenRefreshError:
        print "The credentials have been revoked or expired, please re-run the application to re-authorize"

if __name__ == '__main__':
    main(sys.argv)
