# -*- coding: utf-8 -*-

"""Library that defines a class to collected Google+ posts.

The class PlusDataRetriever is used to query the Google+ API.
A simple usage example is:
  gpluscrawler = gplus.PlusDataRetriever(max_activities, \
                                         max_comments, \
                                         get_attachment)
"""
from apiclient.errors import Error

__author__ = 'ekanou@google.com (Evangelos Kanoulas)'

import httplib2
import os
import sys
import re
import traceback

from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import AccessTokenRefreshError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.tools import run

import boilerpipe
from apiclient.errors import HttpError
import time, random


def strip_tags(html):
    """Method to strip html from html tags.
    
    Args:
      html: html text. 
    """
    p = re.compile(r'<.*?>')
    #     html = p.sub('', html)
    #     p = re.compile(r'\n')
    #     html = p.sub('', html)
    #     p = re.compile(r'\r')

    return p.sub('', html)


class PlusDataRetriever:
    def __init__(self, max_activities, max_comments, get_attachment, get_user, devKey=None):
        self.max_activities = max_activities
        self.max_comments = max_comments
        self.get_attachment = get_attachment
        self.get_user = get_user
        self.api_key = devKey
        self.SetupService()

    def SetupService(self):
        # print "setup service..."
        if self.api_key is not None:
            # trying with simple API Key
            self.service = build("plus", "v1", developerKey=self.api_key)
        else:
            # CLIENT_SECRETS, name of a file containing the OAuth 2.0 information
            # for this application, including client_id and client_secret, which are
            # found on the API Access tab on the Google APIs
            # Console <http://code.google.com/apis/console>

            # changed cause of eclipse, probably need to revert # gk
            #     CLIENT_SECRETS = 'client_secrets.json'
            CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), 'client_secrets.json')

            # Helpful message to display in the browser if the CLIENT_SECRETS file
            # is missing.
            MISSING_CLIENT_SECRETS_MESSAGE = """
                WARNING: Please configure OAuth 2.0

                To make this sample run you will need to populate the client_secrets.json
                file found at:

                   %s

                with information from the APIs Console
                <https://code.google.com/apis/console>.

            """ % os.path.join(os.path.dirname(__file__), CLIENT_SECRETS)

            # Set up a Flow object to be used if we need to authenticate.
            FLOW = flow_from_clientsecrets(
                CLIENT_SECRETS,
                scope='https://www.googleapis.com/auth/plus.me',
                message=MISSING_CLIENT_SECRETS_MESSAGE)

            # If the Credentials don't exist or are invalid run through the native
            # client flow. The Storage object will ensure that if successful the good
            # Credentials will get written back to a file.
            storage = Storage('plus.dat')
            credentials = storage.get()
            # print "credentials: ", credentials
            if credentials is None or credentials.invalid:
                credentials = run(FLOW, storage)

            # Create an httplib2.Http object to handle our HTTP requests and authorize
            # it with our good Credentials.
            http = httplib2.Http()
            http.add_credentials('prepareanapla', 'prepareAnaPla2014!')
            http = credentials.authorize(http)

            self.service = build("plus", "v1", http=http)

        # Init boilerpipe VM # 
        jars = ':'.join((os.path.join(os.path.dirname(__file__), 'lib/nekohtml-1.9.13.jar'), \
                         os.path.join(os.path.dirname(__file__), 'lib/xerces-2.9.1.jar')))

        boilerpipe.initVM(boilerpipe.CLASSPATH + ':' + jars)
        
        # setup extractor
        self.extractor = boilerpipe.ArticleExtractor.getInstance()

    def _GetAllPages(self, resource, request, max_number_activities):
        """Method to iterate over a set of pages of results and return all items.
        
        A function that iterates over a number of pages of results. Each page
        contains up to the maximum number of possible results (20). The number of
        pages to be processed is controlled by the maximum number of activities
        (100 here).
        
        Args:
          resource: the google service.
          request: the request for results.
        """
        all_items = []
        number_activities = 0
        count = 0
        while (request is not None) \
                and (number_activities < max_number_activities) \
                and (count < 4):
            try:
                activities_page = request.execute()
                number_activities += len(activities_page['items'])
                for item in activities_page['items']:
                    all_items.append(item)
                request = resource.list_next(request, activities_page)
            except HttpError, error:
                if error.resp.reason in ['userRateLimitExceeded', 'quotaExceeded']:
                    print "Got 'Rate Limit Error', sleeping for some seconds..."
                to_sleep = 5 ** count + random.random()
                # print 'sleeping for %d seconds' %to_sleep
                time.sleep(to_sleep)
                count += 1
                # print 'Exception: %s :\n %s' % (error, traceback.format_exc())
            except Exception, e:
                print 'Exception: %s :\n %s' % (e, traceback.format_exc())
                count += 1
        return all_items

    def _GetAttachmentText(self, item):
        """Method to obtain the text from any attached to the post article.
        
        A function that goes over the attachments of a post, checks whether
        they are articles, gets the url of the attachments, calls boilerpipe
        and returns the clean text.
        
        Args:
          item: the item returned by a search on activities.
        """
        try:
            attachments_text = []
            for attachment in item['object']['attachments']:
                if attachment['objectType'] == "article":
                    url = boilerpipe.URL(attachment['url'])
                    attachments_text.append(self.extractor.getText(url))
                    # maybe try get Url and then parse from java with jSoup
            return '\n'.join(attachments_text)
        except Exception, e:
            print 'Exception: %s :\n %s' % (e, traceback.format_exc())

    def _GetUserById(self,
                     user_id):
        """Method to return information about a user.
        
        Args:
          user_id: the user id.
        """
        user_resource = self.service.people()
        user_document = user_resource.get(userId=user_id).execute()
        return user_document

    def _GetCommentFields(self, item):
        """Method to return a dictionary with fields of interest from comments.
        
        Args:
          item: the item returned by getting or listing comments.
        """
        CommentsDict = {'comment id': item.get('id'),
                        'published': item.get('published'),
                        'updated': item.get('updated'),
                        'comment selfLink': item.get('selfLink'),
                        'user displayName': item.get('actor').get('displayName'),
                        'user id': item.get('actor').get('id'),
                        'user url': item.get('actor').get('url'),
                        'activity': item.get('object').get('objectType'),
                        'content': item.get('object').get('content'),
                        'number of plusoners': item.get('plusoners').get('totalItems')
        }

        if self.get_user:
            user_item = self._GetUserById(item.get('actor').get('id'))
            if user_item.get('name'):
                CommentsDict['user familyName'] = user_item.get('name').get('familyName')
                CommentsDict['user givenName'] = user_item.get('name').get('givenName')
                CommentsDict['user currentLocation'] = user_item.get('name').get('currentLocation')
                CommentsDict['user relationshipStatus'] = user_item.get('name').get('relationshipStatus')
            CommentsDict['user birthday'] = user_item.get('birthday')
            CommentsDict['user gender'] = user_item.get('gender')
            # URL in DB is text, should contain only one URl. We do not need others
        #             # get the list of urls for this guy
        #             lsUrls = user_item.get('urls')
        #             if lsUrls and (not (not lsUrls)):
        #                 for url in lsUrls:
        #                     CommentsDict['user url'].append(url.get('value'))

        return CommentsDict

    def _GetCommentsByActivityID(self,
                                 activity_id,
                                 max_results=20,
                                 max_number_comments=100):
        """Method to return comments on a certain activity (post).
        
        Args:
          activity_id: the id of the activity on which comments are posted.
          max_results: the maximum number of results returned by a single action.
          max_number_comments: the total max number of comments to be collected.
        """
        comments_resource = self.service.comments()
        comments_request = comments_resource.list(maxResults=max_results,
                                                  activityId=activity_id)
        all_comments = self._GetAllPages(comments_resource,
                                         comments_request,
                                         max_number_comments)
        comments = []
        for comment in all_comments:
            comments.append(self._GetCommentFields(comment))
        return comments

    def _GetActivityFields(self, item, query_string):
        """Method to return a dictionary with fields of interest from activities.
        
        Apart from activity (post) fields of interest returned, the method also
        gets the attached article (if there is an attachment and it is some html
        article, calls boilerpipe to remove the boilerplate of the web page and
        returns the clean text of the article. Further, it calls a method to
        return comments associated by the activity (post).
        
        Args:
          item: the item returned by getting or listing comments.
        """
        if self.get_attachment:
            attachment_text = self._GetAttachmentText(item)
        else:
            attachment_text = ''
        comments_dict = self._GetCommentsByActivityID(
            item.get('id'),
            max_number_comments=self.max_comments)

        ActivitiesDict = {
            'query string': query_string,
            'title': strip_tags(item.get('title')),
            'published': item.get('published'),
            'updated': item.get('updated'),
            'activity id': item.get('id'),
            'activity url': item.get('url'),
            'user displayName': item.get('actor').get('displayName'),
            'user id': item.get('actor').get('id'),
            'user url': item.get('actor').get('url'),
            'content': item.get('object').get('content'),
            'content url': item.get('object').get('url'),
            'attachment': attachment_text,
            'number of plusoners': item.get('object').get('plusoners').get('totalItems'),
            'number of reshares': item.get('object').get('resharers').get('totalItems'),
            'comments': comments_dict
        }

        if self.get_user:
            user_item = self._GetUserById(item.get('actor').get('id'))
            if user_item.get('name'):
                ActivitiesDict['user familyName'] = user_item.get('name').get('familyName'),
                ActivitiesDict['user givenName'] = user_item.get('name').get('givenName'),
                ActivitiesDict['user currentLocation'] = user_item.get('name').get('currentLocation')
                ActivitiesDict['user relationshipStatus'] = user_item.get('name').get('relationshipStatus')
            ActivitiesDict['user birthday'] = user_item.get('birthday')
            ActivitiesDict['user gender'] = user_item.get('gender')
            # URL in DB is text, should contain only one URl. We do not need others            
        #             # get the list of urls for this guy
        #             lsUrls = user_item.get('urls')
        #             if lsUrls and (not (not lsUrls)):
        #                 for url in lsUrls:
        #                     ActivitiesDict['user url'].append(url.get('value'))

        return ActivitiesDict

    def GetActivitiesByTermSearch(self,
                                  query_term=None,
                                  max_results=20):
        """Method to obtain activities (posts), comments and attached articles.
        
        This method queries the Google+ API by a query term and obtains a predifined
        number of activities, comments on these activities and any attached article.
        
        Args:
          query_term: a term that will be used to query the API and retrieve all
          relevant posts.
          max_results: the maximum number of results returned by each request.
        """
        # debug
        # print "call service.activities()"
        activities_resource = self.service.activities()
        # debug
        # print "search for ", query_term
        activities_request = activities_resource.search(maxResults=max_results,
                                                        orderBy='best',
                                                        query=query_term)
        # print "get all items"
        all_items = self._GetAllPages(activities_resource,
                                      activities_request,
                                      self.max_activities)
        # print "Found %d posts" % len(all_items)
        all_activities = []
        for item in all_items:
            all_activities.append(self._GetActivityFields(item, query_term))
        return all_activities
