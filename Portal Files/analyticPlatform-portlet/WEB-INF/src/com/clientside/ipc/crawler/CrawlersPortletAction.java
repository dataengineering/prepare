/*
 @create on	:	    1/04/2014
 
 @author	:		Konstantinos Pechlivanis
 
 @research program: PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 
 @project	:		Analytic Platform (Work package 2)
  		-Task 2.1:  Development of Scientific means 
  		-Task 2.3:  Technical Platform
 
 @document: 		CrawlerPortletAction.java from file /WEB-INF/src/com/clientside/ipc/crawler/CrawlerPortletAction.java
 
 @summary	:		This class handles the items and the name of crawler that are received from manage_crawler portlet and send them to receiver portlet.
 					Depend on selection of crawler (this class) send to the receiver portlet the number of crawled documents the date and time of activation/
 					inactivation that read them from the database
*/

package com.clientside.ipc.crawler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Properties;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.log4j.Logger;

import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class CrawlersPortletAction extends MVCPortlet {

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String crawler = ParamUtil.getString(resourceRequest,"termCrawl");
		Logger myLog = Logger.getLogger(getClass().getName());
		
		PrintWriter pw = resourceResponse.getWriter();
		JSONObject juser = JSONFactoryUtil.createJSONObject();
		
		if (crawler.contains(",left")) {// left click
			crawler = crawler.replace(",left", "").trim();
			JSONArray array = JSONFactoryUtil.createJSONArray();
			JSONArray arrayFig = JSONFactoryUtil.createJSONArray();
			JSONArray arrayFigXX = JSONFactoryUtil.createJSONArray();
			
			try {
				File file = new File("/home/"+System.getProperty("user.name") + "/.PrepareData" + "/crawler_" + crawler + ".txt");
				File fileCron = new File("/home/"+System.getProperty("user.name") + "/.PrepareData"+"/crawler_crontab.txt");
				array.put(crawler);		// add in the first position of array the name of selected crawler
				
//-------------------------------------------------Activate/Inactive time and num_docs--------------------------------------------------------------------				
				Properties properties = new Properties();	// set properties of database
			    properties.put("user","searchuser");
			    properties.put("password","searchUser_@!");
			    properties.put("characterEncoding", "UTF-8");
			    
			    try {
			    // Class.forName(xxx) loads the jdbc classes and creates a driver manager class factory
			    Class.forName("com.mysql.jdbc.Driver");
			    
			    } catch (ClassNotFoundException e) {
			    	myLog.info("Where is your MySQL JDBC Driver?");
					e.printStackTrace();
					return;
				}
			    Connection conn = null;
			    
			    try {
					conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/prepare_crawler_db",properties);
				} catch (SQLException e) {
					myLog.info("Connection Failed! Check output console");
					e.printStackTrace();
					return;
				}
				if (conn == null)
					myLog.info("Failed to make connection!");

				Statement st = conn.createStatement(), st1 = conn.createStatement();
				ResultSet rs= null, rs1= null;
				String endDate="", startDate="", crawl_id="0";
				int bing=0, gplus=0, twitter=0, num_docs = 0, total_num_docs=0;
				
				if (!crawler.contains("_monitor")){ // case for searching crawler
					
					rs = st.executeQuery("SELECT start_date, end_date, date(end_date) FROM crawl_log where crawl_name = '" + crawler + "' group by date(end_date) order by start_date asc");// get the different execution days of crawler
				    while(rs.next()){
				    	startDate = rs.getString(1); 	// the date of crawler end
				    	endDate = rs.getString(2);		// the timestamp of crawler end
				    	
				    	num_docs=0;total_num_docs=0; 	// crawled data for a specific day
				    	rs1 = st1.executeQuery("SELECT crawl_id, num_docs, previous_docs_sum FROM crawl_log where crawl_name = '" + crawler + "' AND date(end_date) = '" + rs.getDate(1) + "'");// get crawl_id, num_docs and previous_docs_sum by day
				    	while(rs1.next()){
				    		crawl_id = rs1.getString(1);
				    		num_docs = rs1.getInt(2);
				    		total_num_docs += (rs1.getInt(2) + rs1.getInt(3));
				    	}
				    	arrayFig.put(total_num_docs);
				    	arrayFigXX.put(rs.getString(3));
				    }
				    
				    rs = st.executeQuery("select crawler_name, num_docs from crawl_query_lkp where crawl_id='" + crawl_id + "'");// get from crawler_name, num_docs the last crawling for each NoMad crawler
				    while(rs.next()){
				    	if (rs.getString(1).equals("BingCrawler"))
				    		bing += rs.getInt(2);
				    	else if (rs.getString(1).equals("GplusCrawler"))
				    		gplus += rs.getInt(2);
				    	else if (rs.getString(1).equals("TwitterCrawler"))
				    		twitter += rs.getInt(2);
				    }
				    st1.close();st.close();conn.close();
	
				    if (startDate=="") {
				    	array.put("Not activate");array.put("Not activate");array.put("Not activate");array.put("Not activate");array.put("Not activate");array.put("Not activate");
				    } else {
				    	array.put("Date: " + startDate.split(" ")[0] + " Time:" + startDate.split(" ")[1]);
				    	if (endDate!=null) {
					    	array.put("Date: " + endDate.split(" ")[0] + " Time:" + endDate.split(" ")[1]);
					    }else
					    	array.put("Not available yet");
				    	array.put("Bing: " + bing);
						array.put("Google Plus: " + gplus);
						array.put("Twitter: " + twitter);
						array.put("Total documents: " + num_docs);
				    }
					
				} else if (crawler.contains("_monitor")) {
					Hashtable<String, Integer > figureOccurHash = new Hashtable<String, Integer >();
					
					rs = st.executeQuery("SELECT start_date, end_date, num_docs, monitor_id FROM monitor_log WHERE monitor_name='" + crawler + "' ORDER BY end_date asc");
					while(rs.next()){
						startDate = rs.getString(1);
						endDate = rs.getString(2);
						num_docs = rs.getInt(3);

				    	rs1 = st1.executeQuery("SELECT count(*), date(created_at) FROM twitter_post "
					    			+ "WHERE engine_id='" + rs.getString(4) + "' and engine_type_id='2' group by date(created_at) order by date(created_at) asc");
				    	while(rs1.next()){
				    		if (!figureOccurHash.containsKey(rs1.getString(2)))
				    			figureOccurHash.put(rs1.getString(2), 0);
				    		figureOccurHash.put(rs1.getString(2), figureOccurHash.get(rs1.getString(2)) + rs1.getInt(1));
				    	}
					}
				  		
			  		ArrayList<String> dates = new ArrayList<String>(figureOccurHash.keySet());
			  	    Collections.sort(dates);	// sort hash-table according to the date of twitter post

			  	    for (String str : dates) {
			  			arrayFig.put(figureOccurHash.get(str));
			  			arrayFigXX.put(str);
			  	    }
				  	st1.close();st.close();

					if (startDate.equals("") || startDate==null) {
				    	array.put("Not activate");array.put("Not activate");array.put("Not activate");array.put("Not activate");array.put("Not activate");array.put("Not activate");
				    } else {
				    	array.put("Date: " + startDate.split(" ")[0] + " Time:" + startDate.split(" ")[1]);
				    	if (endDate!=null) {
					    	array.put("Date: " + endDate.split(" ")[0] + " Time:" + endDate.split(" ")[1]);
					    }else
					    	array.put("Not available yet");
				    	array.put("Bing: " + bing);array.put("Google Plus: " + gplus);array.put("Twitter: " + twitter);
						array.put("Total monitored documents: " + num_docs);
				    }
				}
				
				//-------------------------------------------------CRONTAB--------------------------------------------------------------------//
				if (fileCron.exists()) {// if file exists
					BufferedReader bf = new BufferedReader(new FileReader(fileCron.getAbsoluteFile()));	// create a buffer for a file
			  		String term;
			  		while ((term = bf.readLine()) != null ){// while there are terms in configuration file
			  			if (term.split("-->")[0].equals(crawler)){
			  				array.put(term.split("-->")[1]);		// add in first position of array the name of selected crawler and the date of activate
			  				break;
			  			}
			  		}bf.close();
			  		
			  		if (array.length()==7)	// if select crawler that is not activate yet
			  			array.put("* * * * *");
				} else
					array.put("* * * * *");

				//-------------------------------------------------Inserted terms--------------------------------------------------------------//
				if (file.exists()) {// if file exists
					BufferedReader bf = new BufferedReader(new FileReader(file.getAbsoluteFile()));	// create a buffer for a file
			  		String term; int id_lang;
			  		while ((term = bf.readLine()) != null ){				// while there are terms in configuration file
			  			id_lang = Integer.parseInt(term.split("#")[1])/2-1;	// find the language
			  			term = term.split("#")[0].split(",")[id_lang];		// take the inserted term
			  			array.put(term);									// add a term in the array
			  		}
			  		bf.close();
				}
				
				
		  	} catch (IOException e) {myLog.error("File does not exist!");
		  	} catch (SQLException ex) { // handle any errors
		  		myLog.error("SQLException: " + ex.getMessage());
		  		myLog.error("SQLState: " + ex.getSQLState());
		  		myLog.error("VendorError: " + ex.getErrorCode());}

			juser.put("jsCrawlerTerm", array);
			juser.put("numDocFig", arrayFig);
			juser.put("numDocFigXX", arrayFigXX);
			pw.println(juser.toString());

		}else if (crawler.contains("Remove:")){// right click for removing
			juser.put("RgClickCr", crawler);
			pw.println(juser);
		}else if(crawler.contains("Term:")){
			juser.put("TermAccount", crawler.replaceFirst("Term:", ""));
			pw.println(juser);			
		}
			
	}
}
