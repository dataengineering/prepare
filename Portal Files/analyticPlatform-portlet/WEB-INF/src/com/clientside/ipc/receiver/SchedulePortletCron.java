package com.clientside.ipc.receiver;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import porterStemmer.Stemmer;

import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;

public class SchedulePortletCron implements MessageListener {
	
	@Override  
	public void receive(Message arg0) throws MessageListenerException {
		Logger myLog = Logger.getLogger(getClass().getName());
		myLog.info("\nListener for CRON is awake!!!");
		
		try {
			//-------------------------------------FIND FOR EXTRA INSERTED CRAWLED DATA IN DATABASED----------------------------------------------//
			Properties properties = new Properties();	// set properties of database
		    properties.put("user","searchuser");
		    properties.put("password","searchUser_@!");
		    properties.put("characterEncoding", "UTF-8");
			try {
			    Class.forName("com.mysql.jdbc.Driver");	//loads the jdbc classes and creates a driver manager class factory

			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return;
			}
			Connection conn = null;
			try {
				conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/prepare_crawler_db",properties);
			} catch (SQLException e) {
				e.printStackTrace();
				return;
			}
			if (conn != null)
				myLog.info("You made it, take control your database now!");

			Statement st1 = conn.createStatement();
			Statement st2 = conn.createStatement();
			Stemmer myStemmer = new Stemmer();

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");			// set format of date
			dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Athens")); 		//set time zone of Athens
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss"); 	// set format of current time
			SimpleDateFormat timeFormatDb = new SimpleDateFormat("HH:mm:ss");	// set format of time of database
			timeFormat.setTimeZone(TimeZone.getTimeZone("Europe/Athens"));		//set time zone of Athens
			Date currDate = new Date();	// get current date
			Date currTime = new Date(); // get current time
			String crawl_name="";

			ResultSet rs = st1.executeQuery("select crawl_name, end_date, crawl_id from (select * from crawl_log order by end_date desc) x group by crawl_name union(select monitor_name, end_date, monitor_id from (select * from monitor_log order by end_date desc) x group by monitor_name)");// get the end time of last execution for each crawler
			while(rs.next()){// save the distinct dates of each crawling, not time
				Date lastDateIns = null;Date lastTimeIns = null;
				
				if (!(rs.getDate(2)==null)){
					crawl_name = rs.getString(1);
					lastDateIns = rs.getDate(2);
					lastTimeIns = rs.getTime(2);
				}else
					continue;

				if (dateFormat.format(currDate).equals(dateFormat.format(lastDateIns))){
					int diffhours = Integer.parseInt(timeFormat.format(currTime).split(":")[0])-Integer.parseInt(timeFormatDb.format(lastTimeIns).split(":")[0]);
					int diffmin = Integer.parseInt(timeFormat.format(currTime).split(":")[1])-Integer.parseInt(timeFormatDb.format(lastTimeIns).split(":")[1]);
					if ( (diffhours==0) || (diffhours==1 && diffmin<=0) || (diffhours==-23 && diffmin<=0) ){
						myLog.info("There is new insertion");

						StandardAnalyzer analyzer2 = new StandardAnalyzer();	// Specify the analyzer for tokenizing text.
						Directory index2 = FSDirectory.open(new File("/home/" + System.getProperty("user.name") + "/.PrepareData/index_data_" + crawl_name).toPath()); // store data to the specific folder
						IndexWriterConfig config2 = new IndexWriterConfig(analyzer2);
						IndexWriter w2 = new IndexWriter(index2, config2);
						
						String content="";ResultSet rsIndexContent;
						if(!crawl_name.contains("_monitor"))
							rsIndexContent = st2.executeQuery("select clean_text from content where crawl_id='" + rs.getString(3) + "'");// get title, clear_contenr, language and crawler_datatime from bing database
						else
							rsIndexContent = st2.executeQuery("select text from twitter_post where engine_id='" + rs.getString(3) + "' and engine_type_id='2'");// get title, clear_contenr, language and crawler_datatime from bing database
						while(rsIndexContent.next()){
							content = content + " " + rsIndexContent.getString(1);
						}

						String [] contentTokens = content.replaceAll("[^\\dA-Za-z ]", " ").toLowerCase().split(" ");	// convert text to lower-case and remove "non-letter"

						String queryCrawler = "\""+ rs.getString(1) +"\""; 	// for specific crawler
						String queryDay = "\""+ rs.getString(2).split(" ")[0] +"\""; 	// for specific crawler
						String id = "\""+ rs.getString(3) +"\"";

						Query q_crawler = new QueryParser("\"crawler\"", analyzer2).parse(queryCrawler); // set the query for the current crawler
						Query q_day = new QueryParser("\"day\"", analyzer2).parse(queryDay); // set the query for the current crawler
						Query q_id = new QueryParser("\"id\"", analyzer2).parse(id); // set the query for the current crawler id

						BooleanQuery finalQuery = new BooleanQuery(); // sub-query -- AND
						finalQuery.add(q_day, Occur.SHOULD);
						finalQuery.add(q_crawler, Occur.MUST);  // SHOULD implies that the keyword should occur for the specific crawler
						finalQuery.add(q_id, Occur.MUST);  // MUST implies that the keyword must occur for the specific crawler id

						w2.deleteDocuments(finalQuery);

						for (int j=0;j<contentTokens.length;j++){	// applying the stemmer to each word of clean text
							char [] contentArray = contentTokens[j].toCharArray();
							if (contentArray.length==0)// if there is not text
								continue;
							myStemmer.add(contentArray, contentArray.length);// add word in "collation" of chars
							myStemmer.stem();// execute stemming
							addDoc(w2, myStemmer.toString().trim(), rs.getString(2).split(" ")[0], rs.getString(1), rs.getString(3));	// add a new doc with 3 fields
						}w2.commit();w2.close();rsIndexContent.close();
					}
				}
			}rs.close();st1.close();st2.close();conn.close();
		} catch (SQLException ex) { // handle any errors
			myLog.info("SQLException: " + ex.getMessage());
			myLog.info("SQLState: " + ex.getSQLState());
			myLog.info("VendorError: " + ex.getErrorCode());
		} catch (IOException e) { e.printStackTrace();} catch (ParseException e) {e.printStackTrace();}
	}

	private static void addDoc(IndexWriter w, String text, String day, String crawler, String crawl_id) throws IOException, ParseException{
		Document doc = new Document();
		doc.add(new TextField("\"text\"", text, Field.Store.YES));// a text field will be tokenized
		doc.add(new StringField("\"day\"", day, Field.Store.YES));// we use a string field for the day of crawling
		doc.add(new StringField("\"crawler\"", crawler, Field.Store.YES));// we use a string field for the name of crawler
		doc.add(new StringField("\"id\"", crawl_id, Field.Store.YES));// we use a string field for the name of crawler

		w.addDocument(doc); // store data
	}
}
