<%--
 @create on:	     1/04/2014
 @author:			 Konstantinos Pechlivanis
 @research program:  PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 @project:			 Analytic Platform (Work package 2)
  		-Task 2.1:   Development of Scientific means 
  		-Task 2.3:   Technical Platform
  		
 @document: 		 view.jsp from file /html/jsps/sender
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.FileReader" %>
<%@ page import="java.io.IOException" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ page import="java.io.FileInputStream" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="java.awt.List" %>

<%@ page import="org.openrdf.OpenRDFException" %>
<%@ page import="org.openrdf.query.BindingSet" %>
<%@ page import="org.openrdf.query.QueryLanguage" %>
<%@ page import="org.openrdf.query.TupleQuery" %>
<%@ page import="org.openrdf.query.TupleQueryResult" %>
<%@ page import="org.openrdf.repository.Repository" %>
<%@ page import="org.openrdf.repository.RepositoryConnection" %>
<%@ page import="org.openrdf.repository.RepositoryException" %>
<%@ page import="org.openrdf.repository.sail.SailRepository" %>
<%@ page import="org.openrdf.rio.RDFFormat" %>
<%@ page import="org.openrdf.sail.memory.MemoryStore" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:resourceURL var="getTermValue"></portlet:resourceURL>
<portlet:resourceURL var="removeCrawler"></portlet:resourceURL>

<!DOCTYPE html>
<html>
    <head>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type='text/javascript' src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
    <script type='text/javascript' src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type='text/javascript' src='http://code.jquery.com/jquery.min.js'></script>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	
	<script>var id_lang; var termArray; var termArrayUS; var termArrayFR;var myTerm;var term3lang;</script>
		
	<script type="text/javascript"> 	<!-- expand - collapse the list -->
     $(function() {
         $("h4").click(function(event) {
             event.preventDefault();
             $(this).nextAll("ul:first").toggle();
         });
     });
	</script>
	
	<script>	<!-- catch the right clicked term, and colour background of seleced terms -->
	$(document).ready(function(){
   		$("div").delegate("cl","mousedown",function(e){
   		   if( e.button == 2 ){ // right mouse button
   		   		myTerm = $(this).text().trim(); // take right clicked term
   		   		if (id_lang==2) // term in english
	   		   		term3lang = '2' + termArrayUS[termArrayUS.indexOf(myTerm)] + "," + termArrayFR[termArrayUS.indexOf(myTerm)];		
   		   		else if (id_lang==4) // term in german
   		   			term3lang = '4' + termArrayUS[termArrayFR.indexOf(myTerm)] + "," + termArrayFR[termArrayFR.indexOf(myTerm)];
 
   	         	$(this).css("background-color","#C3C3C3");
   	         	return false;
   	       }else if (e.button == 0){// catch the left clicked, expand - collapse the list
   	    		$("li").nextAll("ul").toggle();
   	    	    return false;
   	       }
   		});
 	});
	</script>
	
	<script src="<%=request.getContextPath()%>/js/jquery-1.8.2.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.ui.position.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.contextMenu.js"></script>
	<script src="<%=request.getContextPath()%>/js/prettify.js"></script>
	<script src="<%=request.getContextPath()%>/js/screen.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery.contextMenu.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/css/prettify.sunburst.css" />
	
	<script>
	var tempTerm='';var termValue='';
	$(function(){
		$.contextMenu({
			selector: '.context-menu-one', 
			callback: function(key, options) {
			    var m = "clicked: " + key;
			    window.console && console.log(m);
			    if (key!='quit') {// case termination
				    if (selCrawler != ''){ // check if a crawler have selected, if it is crawler for searching
				    	tempTerm = myTerm;
					    if (key=='insert'){
					    	if (selCrawler.indexOf("_monitor")==-1) {
					    		termValue = term3lang + "--" + selCrawler;// insert term
					    	}else if (selCrawler.indexOf("_monitor")>-1) {
					    		alert('You have to select a crawler for searching if you want to insert one of these terms!')}
					    } else if (key=='delete') {
					    	termValue = "Delete:" + tempTerm + "--" + selCrawler; // delet term
				    	} else if (key=='write') {
					    	termValue = "Write:" + tempTerm + "--" + selCrawler;} // write term to file
				    
				    } else
				    	alert('You have to select a crawler!')
			    
			    } else
			    	alert('Not available option yet');
			},
			items:{
			    "insert": {name: "Insert Term", icon: "insert"},
			    "delete": {name: "Delete Term", icon: "delete"},
			    "write": {name: "Write term to file", icon: "write"},
			    "sep2": "---------",
			    "quit": {name: "Quit", icon: "quit"}
			}//"paste": {name: "Paste", icon: "paste"}
		});
		$('.context-menu-one').on('click', function(e){
			console.log('clicked', this);
		});
	});
	</script>
	
	<%-- send the value of selected term in SenderPortletAction class --%>
	<script type="text/javascript">
	$(function(){
		$(":contains(tempTerm)").click(function(event) { // if is clicked a term
			if (tempTerm != '' && termValue != ("undefined--" + selCrawler)){// use this case to prevent empty request
				$.ajax({
					url:'<%=getTermValue%>',
					dataType: "json",
					data:{<portlet:namespace/>term:termValue},
					type: "get",
					success: function(data){
					Liferay.fire('getTermValue', {termValue:data});
					},
					beforeSend: function(){},	// before send this method will be called
					complete: function(){termValue='';tempTerm='';}		// after completed request then this method will be called.
				});
			}
		});
	});</script>
	
		
	<%-- receive the selected crawler from CrawlersPortletAction class --%>
	<script>
	var selCrawler = '';
	Liferay.on('getSelectCrawler',function(event) {
		if (event.selectCrawler.jsCrawlerTerm[0].search("-->") == -1){// selected crawler is not activated
			selCrawler = event.selectCrawler.jsCrawlerTerm[0]; 	//get the name of selected crawler
		}else
			selCrawler = event.selectCrawler.jsCrawlerTerm[0].split("-->")[0]; 	//get the name of selected crawler
	});
	</script>
	
	<%-- receive the crawler from CrawlersPortletAction class which is selected to delete --%>
	<script>
	Liferay.on('rightSelectCrawler',function(event) {// return the name of crawler
		termValue = event.RigSelCrawler.RgClickCr; 	// get the name of the crawler which user select to delete
		$.ajax({
			url:'<%=removeCrawler%>',
			dataType: "json",
			data:{<portlet:namespace/>term:termValue},
			type: "get",
			success: function(data){
			Liferay.fire('removeCrawler', {termValue:data});
			},
			beforeSend: function(){},	// before send this method will be called
			complete: function(){}		// after completed request then this method will be called.
		});
		termValue='';
	});
	</script>
	
	<%-- receive the term/account from CrawlersPortletAction class which is inserted manually --%>
	<script>
	Liferay.on('getTermAccount',function(event) {// return the name of crawler
		termValue = event.termAcc.TermAccount; 	// get the name of the crawler which user select to delete
		$.ajax({
			url:'<%=getTermValue%>',
			dataType: "json",
			data:{<portlet:namespace/>term:termValue},
			type: "get",
			success: function(data){
			Liferay.fire('getTermValue', {termValue:data});
			},
			beforeSend: function(){},	// before send this method will be called
			complete: function(){}		// after completed request then this method will be called.
		});
		termValue='';
	});
	</script>
	
    </head>
	
    <a href="#" onMouseDown="usaLang()">
    <img src="<%=request.getContextPath()%>/images/uk.jpg" width="25" height="17" border="0" alt="javascript button" title="Click flag to show terms in English"></a>
    <a href="#" onMouseDown="frenchLang()">
    <img src="<%=request.getContextPath()%>/images/france.jpg" width="25" height="17" border="0" alt="javascript button" title="Click flag to show terms in French"></a>
    
   	
    <%final String inputFile = "/home/"+System.getProperty("user.name")+"/.PrepareData/ontology/"+"tmt.ttl";
    ArrayList<String> termList = new ArrayList<String>();
	String parent="", child="", grandchild="", greatGrandchild="";
    try {
		//Create a new main memory repository 
		MemoryStore store = new MemoryStore();
		Repository repo = new SailRepository(store);
		repo.initialize();

		//Store file
		try {
			InputStream in = new FileInputStream(inputFile);
			RDFFormat fileRDFFormat = RDFFormat.N3;	//set the type of file
			RepositoryConnection con = repo.getConnection();
			
			try {
				con.add(in, "http://www.w3.org/2004/02/skos/core#", fileRDFFormat);//store the file
			}finally {
				con.close();
			}
		} catch (OpenRDFException e) {
			e.printStackTrace();
		} catch (java.io.IOException e) {// handle io exception
			e.printStackTrace();
		}

		try {
			RepositoryConnection con = repo.getConnection();
			try {
				String queryString = " PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
									 " PREFIX skos:	<http://www.w3.org/2004/02/skos/core#>  " +
									 " PREFIX owl: 	<http://www.w3.org/2002/07/owl#> 	    " +
									 " SELECT ?x  ?enLabel ?grLabel ?deLabel 				" +
									 " WHERE{ ?x rdf:type owl:NamedIndividual . 			" +
									 "  OPTIONAL{											" +	
									 "		    ?x skos:prefLabel ?enLabel . 				" +
									 "		    FILTER(langMatches(lang(?enLabel), \"EN\")) " +
									 "  }													" +
									 "	OPTIONAL{ 											" +
									 "		 	?x skos:prefLabel ?frLabel . 				" +
									 "		 	FILTER(langMatches(lang(?frLabel), \"FR\")) " +
									 "  }													" +
									 " 		 FILTER NOT EXISTS{?x skos:broader ?pateras } 	" +
									 " 	}";

				TupleQuery tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);	//execute sparql query
				TupleQueryResult result = tupleQuery.evaluate();%>
	
				<%try{
					boolean noPrefLabel;
					while (result.hasNext()) { //save all fathers in a list
						noPrefLabel = false;
						BindingSet bindingSet = result.next();

						String enTerm;
						if(bindingSet.getValue("enLabel")!=null)
							enTerm = bindingSet.getValue("enLabel").toString().replace("\"", "").replace("@en", "").trim();
						else{
							noPrefLabel = true;
							if (bindingSet.getValue("x").toString().contains("#"))
								enTerm = bindingSet.getValue("x").toString().split("#")[1].trim();
							else
								enTerm = bindingSet.getValue("x").toString().split("/")[bindingSet.getValue("x").toString().split("/").length-1].trim();
						}
						if (noPrefLabel == false){
							parent = enTerm;
							//case for French terms
							if(bindingSet.getValue("frLabel")==null)
								parent = parent + " & " + enTerm;//if term is not available in French, use the English term
							else//case for term in French
								parent = parent + " & " + bindingSet.getValue("frLabel").toString().replace("\"", "").replace("@fr", "").trim();
							termList.add("par:" + parent);
						}
						
						String uri = bindingSet.getValue("x").toString();
						queryString = " PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
							 " PREFIX skos:	<http://www.w3.org/2004/02/skos/core#> 		" +
							 " PREFIX owl: 	<http://www.w3.org/2002/07/owl#> 			" +
							 " SELECT ?x  ?enLabel ?grLabel ?deLabel 					" +
							 " WHERE{ ?x rdf:type owl:NamedIndividual . 				" +
							 "        ?x skos:broader <" + uri + "> .					" +
							 "		  ?x skos:prefLabel ?enLabel . 						" +
							 "		  FILTER(langMatches(lang(?enLabel), \"EN\")) 		" +
							 "	OPTIONAL{ 												" +
							 "		 	?x skos:prefLabel ?frLabel . 					" +
							 "		 	FILTER(langMatches(lang(?frLabel), \"FR\")) 	" +
							 "  }														" +
							 " }";
					
					 	TupleQuery tupleQuery2 = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);	//execute sparql query
					 	TupleQueryResult result2 = tupleQuery2.evaluate();
						
						while (result2.hasNext()) { //for every child
							BindingSet bindingSet2 = result2.next();
							enTerm = bindingSet2.getValue("enLabel").toString().replace("\"", "").replace("@en", "").trim();
							child = enTerm;
							
							//case for French terms
							if(bindingSet2.getValue("frLabel")==null)
								child = child + " & " + enTerm;//if term is not available in French, use the English term
							else//case for term in French
								child = child + " & " + bindingSet2.getValue("frLabel").toString().replace("\"", "").replace("@fr", "").trim();

							if (noPrefLabel == false)
								termList.add("child:" + child);
							else
								termList.add("par:" + child);
							uri = bindingSet2.getValue("x").toString();
							queryString = " PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
									 " PREFIX skos:	<http://www.w3.org/2004/02/skos/core#> 		" +
									 " PREFIX owl: 	<http://www.w3.org/2002/07/owl#> 			" +
									 " SELECT ?x  ?enLabel ?grLabel ?deLabel 					" +
									 " WHERE{ ?x rdf:type owl:NamedIndividual . 				" +
									 "        ?x skos:broader <" + uri + "> .					" +
									 "		  ?x skos:prefLabel ?enLabel . 						" +
									 "		  FILTER(langMatches(lang(?enLabel), \"EN\")) 		" +
									 "	OPTIONAL{ 												" +
									 "		 	?x skos:prefLabel ?frLabel . 					" +
									 "		 	FILTER(langMatches(lang(?frLabel), \"FR\")) 	" +
									 "  }														" +
									 " }";
							 	
						 	TupleQuery tupleQuery3 = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);	//execute sparql query
							TupleQueryResult result3 = tupleQuery3.evaluate();
							while (result3.hasNext()) { // for every grandchild
								BindingSet bindingSet3 = result3.next();
								enTerm = bindingSet3.getValue("enLabel").toString().replace("\"", "").replace("@en", "");
								grandchild = enTerm;
								
								//case for French terms
								if(bindingSet3.getValue("frLabel")==null)
									grandchild = grandchild + " & " + enTerm; // if term is not available in Greek, use the English term
								else //case for term in French
									grandchild = grandchild + " & " + bindingSet3.getValue("frLabel").toString().replace("\"", "").replace("@fr", "").trim();
								
								if (noPrefLabel == false)
									termList.add("gndchl:" + grandchild);
								else
									termList.add("child:" + grandchild);
								uri = bindingSet3.getValue("x").toString();
								queryString = " PREFIX rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " +
									 " PREFIX skos:	<http://www.w3.org/2004/02/skos/core#> 		" +
									 " PREFIX owl: 	<http://www.w3.org/2002/07/owl#> 			" +
									 " SELECT ?x  ?enLabel ?grLabel ?deLabel 					" +
									 " WHERE{ ?x rdf:type owl:NamedIndividual . 				" +
									 "        ?x skos:broader <" + uri + "> .					" +
									 "		  ?x skos:prefLabel ?enLabel . 						" +
									 "		  FILTER(langMatches(lang(?enLabel), \"EN\")) 		" +
									 "	OPTIONAL{ 												" +
									 "		 	?x skos:prefLabel ?frLabel . 					" +
									 "		 	FILTER(langMatches(lang(?frLabel), \"FR\")) 	" +
									 "  }														" +
									 " }";
									 
									TupleQuery tupleQuery4 = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);//execute sparql query
									TupleQueryResult result4 = tupleQuery4.evaluate();
										
									while (result4.hasNext()) { //for every great grandchild
										BindingSet bindingSet4 = result4.next();
										enTerm = bindingSet4.getValue("enLabel").toString().replace("\"", "").replace("@en", "");
										greatGrandchild = enTerm;
										
										//case for French terms
										if(bindingSet4.getValue("frLabel")==null)
											greatGrandchild = greatGrandchild + " & " + enTerm;//if term is not available in Greek, use the English term
										else//case for term in Greek
											greatGrandchild = greatGrandchild + " & " + bindingSet4.getValue("frLabel").toString().replace("\"", "").replace("@fr", "").trim();

										if (noPrefLabel == false)
											termList.add("gtgrch:" + greatGrandchild);
										else
											termList.add("gndchl:" + greatGrandchild);
									}
								}
							}
						}
					 }
				finally {result.close();}
			}
			finally {con.close();}
		} catch (Exception e) {//handle exception
			e.printStackTrace();
		}
	} catch (RepositoryException e) {// handle exception
		e.printStackTrace();
	}%>

	<%-- catch the selection of user about the language of terms --%>
	<script type="text/javascript">
	termArrayUS = [<% for (int i = 0; i < termList.size(); i++) { %>,'<%= termList.get(i).split(":")[1].split("&")[0].trim() %>'<%= i + 1 < termList.size() ? ",":"" %><% } %>];
	termArrayFR = [<% for (int i = 0; i < termList.size(); i++) { %>,'<%= termList.get(i).split(":")[1].split("&")[1].trim() %>'<%= i + 1 < termList.size() ? ",":"" %><% } %>];

	function usaLang(){
		$("#list").empty();
		id_lang = 2;	// id 2 for English
		termArray = [<% for (int i = 0; i < termList.size(); i++) { %>,'<%= termList.get(i).split("&")[0].trim() %>'<%= i + 1 < termList.size() ? ",":"" %><% } %>];
		printList(termArray);
	}
	function frenchLang(){
		$("#list").empty();
		id_lang = 4;	// id 4 for French
		termArray = [<% for (int i = 0; i < termList.size(); i++) { %>,'<%= termList.get(i).split(":")[0].trim() + ":" + termList.get(i).split("&")[1] %>'<%= i + 1 < termList.size() ? ",":"" %><% } %>];
		printList(termArray);
	}
	function printList(termArray){
		var stringOfList = ""; var prev = "_";
		$.each(termArray, function(i,v) {// dynamic list
			if (v!=null){
				if (v.search('par:') != -1){// parent
					
					if(prev == "child") stringOfList += "</ul>";
					else if(prev == "gndchl") stringOfList += "</ul></ul>";
					else if(prev == "gtgrch") stringOfList += "</ul></ul></ul>";
					stringOfList += "<li><h4 class=\"context-menu-one box menu-1\" style=\"display: inline-block; vertical-align: middle; color:teal;\"><cl>" + v.replace("par:","") + "</cl><a href=\"#expand-collapse\"></a></h4></li>";
	       		}
				else if (v.search('child:') != -1){// if child
					
					if(prev == "gndchl") stringOfList += "</ul>";
					if(prev == "gtgrch") stringOfList += "</ul></ul>";
					if(prev == "par") stringOfList += "<ul style=\"list-style-type:disc\">";
					stringOfList += "<li><h4 class=\"context-menu-one box menu-1\" style=\"display: inline-block; vertical-align: middle; color:teal;\"><cl>" + v.replace("child:","") + "</cl><a href=\"#expand-collapse\"></a></h4></li>";
				}
				else if (v.search('gndchl:') != -1){// if grandchild
					
					if(prev == "gtgrch") stringOfList += "</ul>";
					if(prev == "child") stringOfList += "<ul style=\"list-style-type:disc\">";
					stringOfList += "<li><h4 class=\"context-menu-one box menu-1\" style=\"display: inline-block; vertical-align: middle; color:teal;\"><cl>" + v.replace("gndchl:","") + "</cl><a href=\"#expand-collapse\"></a></h4></li>";
				}
 				else if (v.search('gtgrch:') != -1){// if great grandchild
 					
					if(prev == "gndchl") stringOfList += "<ul style=\"list-style-type:disc\">";
					stringOfList += "<li><h4 class=\"context-menu-one box menu-1\" style=\"display: inline-block; vertical-align: middle; color:teal;\"><cl>" + v.replace("gtgrch:","") + "</cl><a href=\"#expand-collapse\"></a></h4></li>";
 				}
				prev = v.split(":")[0];
		}});
		$("#list").append(stringOfList);
	}
	</script>

	<!---------------create list with keywords--------------------->
	<h4><b style="color:Brown;" title="Click to expand-collapse the list">List of available terms:</b> <a href="#expand-collapse"> </a></h4>
		<ul class="collapsibleList" style="list-style-type:disc">
		<b id="list"></b>
		</ul>
	
	<div id="header" style="background-color:LightSlateGray; opacity: 0.8;">
	<h1 style="text-align:center;margin-bottom:0;">Source Type</h1></div>
	<p><b>1) Bing <br>2) Google Plus <br>3)Twitter (Monitoring, Search API)</b> </p>
	<div id="footer" style="background-color:LightSlateGray;clear:both;text-align:center;"></div>
	
    </body>
</html>