package com.client.search;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.util.PDFTextStripper;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;

public class SchedulePortlet implements MessageListener {
		
	@Override  
	public void receive(Message arg0) throws MessageListenerException {
		Logger myLog = Logger.getLogger(getClass().getName());
		myLog.info("\nListener for PDF_PARSING is awake!!!");
		
		ArrayList<String> nameOfPDFNew = new ArrayList<String>(); // list with the additional inserted pdf
		Hashtable<String, String> nameWithPath = new Hashtable<String, String>(); //hash table with key: name of Pdf's folder from repository, value:path of file in Liferay repository
		File filePdfRep = new File("/home/"+System.getProperty("user.name") + "/.PrepareData/pdf_repository.txt");	// repository with the pdf files

		try {			
			if (!filePdfRep.exists()) {
				filePdfRep.createNewFile();	
			} else {
				BufferedReader bfPDF = new BufferedReader(new FileReader(filePdfRep.getAbsoluteFile()));	// create a buffer for pdf_repository.txt
				String tempTerm;
				while((tempTerm = bfPDF.readLine()) != null ){	// while there are terms in configuration file
					nameWithPath.put(tempTerm.split(" ")[0], tempTerm.split(" ")[1]);	// read the inserted pdfs in repository
				}bfPDF.close();
			}

			long fileEntryId = 0;
			List<DLFileEntry> ls = com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil.getDLFileEntries(-1,-1);

			for(DLFileEntry ts:ls){ // find the list of file entry id
				fileEntryId = ts.getFileEntryId(); // get id
				DLFileEntry fileEntry = com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil.getFileEntry(fileEntryId);
				fileEntry = fileEntry.toEscapedModel();

				if (fileEntry.getExtension().equals("pdf")) {	// only for pdf file
					if (!nameWithPath.containsKey(fileEntry.getFolderId() + "/" + fileEntry.getName())) { // if there is new inserted pdf
						nameOfPDFNew.add(fileEntry.getFolderId() + "/" + fileEntry.getName());	// stored the additional inserted pdfs
					}
				}
			}
		} catch (SystemException e1) {e1.printStackTrace();
		} catch (PortalException e) {e.printStackTrace();
		} catch (IOException e) {e.printStackTrace();}

		//-----------------------------------PARSE PLAIN TEXT OF PDFS----------------------------------\\
		String loadFolder = PropsUtil.get("liferay.home") + "/data/document_library/"; // repository
		PDFParser parser = null;PDFTextStripper pdfStripper = null;PDDocument pdDoc = null;COSDocument cosDoc = null;PDDocumentInformation info = null;
		String title, author, keyword;

		try {
			BufferedWriter bwPDF = new BufferedWriter(new FileWriter(filePdfRep.getAbsoluteFile()));	// create a buffer write
			Enumeration<String> keys = nameWithPath.keys();	// take the keys of hash table
			while(keys.hasMoreElements()) {					// for every key
				String key = (String) keys.nextElement();
				bwPDF.write(key + " " + nameWithPath.get(key) +"\n");
			}

			myLog.info("\n" + nameOfPDFNew.size() + " additional documents-pdfs was found\n");
			File file = new File("/home/" + System.getProperty("user.name") + "/.PrepareData/index_repository/");
			if (!file.exists()) { if (file.mkdir()) myLog.info("Directory is created!");}
						
			for (String key:nameOfPDFNew) {	// for each pdf's folder in repository
				ArrayList<File> myFiles = new ArrayList<File>();	// list with the file which is contained in folder key
				listf(loadFolder, myFiles, key);
				for(int ii=0;ii<myFiles.size();ii++){

					//-------------------------------------STORE NAME INSERTED DATA----------------------------------------------//
					bwPDF.write(key + " " + myFiles.get(ii) +"\n");	// write the name and the path of file in text
					
					//-------------------------------------PARSING OF PDFS----------------------------------------------//
					parser = new PDFParser(new FileInputStream(myFiles.get(ii)));
					parser.parse();
					cosDoc = parser.getDocument();
					pdfStripper = new PDFTextStripper();
					pdDoc = new PDDocument(cosDoc);
					pdfStripper.getStartPage(); 							// return text of article
					String parsedText = pdfStripper.getText(pdDoc).trim();	// get the parsed text of the specific doc
					info = pdDoc.getDocumentInformation();
					
					title = info.getTitle();			// get the title of pdf file
					if (title==null || title.trim().equals(""))
						title = "Not available";

					author = info.getAuthor();			// get the Author of pdf file
					if (author==null || author.trim().equals(""))
						author = "Not available";
					
					keyword = info.getKeywords();		// get the keywords of pdf file
					if (keyword==null || keyword.trim().equals(""))
						keyword = "Not available";				

					//-------------------------------------INDEX DATA----------------------------------------------//
					StandardAnalyzer analyzer = new StandardAnalyzer();	// Specify the analyzer for tokenizing text.
					Directory index = FSDirectory.open(file.toPath()); // store data to the specific folder
					IndexWriterConfig config = new IndexWriterConfig(analyzer);
					IndexWriter w = new IndexWriter(index, config);
					addDoc(w, key, parsedText, title, author, keyword);	// add a new doc with 5 fields
					w.close();
					
					parser.clearResources();cosDoc.close();pdDoc.close();
				}
			}bwPDF.close();
		}
		catch (Exception e) { System.err.println("An exception occured in parsing the PDF Document." + e.getMessage()); } 
		finally {
			try {
				if (cosDoc != null) {cosDoc.close();}
				if (pdDoc != null) {pdDoc.close();}
			} catch (Exception e) {	e.printStackTrace(); }
		}
	}

	
	// return a list with all the pdf files from the specific folder path (==key)
	private ArrayList<File> listf(String directoryName, ArrayList<File> files, String key) {
	    File directory = new File(directoryName);
	    File [] fList = directory.listFiles(); // list with files in path

	    for (File file : fList) {
	        if (file.isFile()) {// file
	        	if (file.getAbsolutePath().contains("/"+key+"/")){// if the absolute path of file contains the name of the current folder
	        		files.add(file);
	        		return files;
	        	}
	        } else if (file.isDirectory()) {// directory
	            listf(file.getAbsolutePath(), files, key);
	        }
	    }
	    return files;
	}

	private static void addDoc(IndexWriter w, String name, String text, String title, String author, String keywords) throws IOException{
		  Document doc = new Document();
		  doc.add(new StringField("\"name\"", name, Field.Store.YES)); // text field will be tokenized
		  doc.add(new TextField("\"text\"", text, Field.Store.YES)); // string field for the text of pdf
		  doc.add(new StringField("\"title\"", title, Field.Store.YES)); // string field for the title of crawler
		  doc.add(new StringField("\"author\"", author, Field.Store.YES)); // string field for the author of pdf
		  doc.add(new StringField("\"keywords\"", keywords, Field.Store.YES)); // string field for the keywords of pdf
		  w.addDocument(doc);
	}
}
