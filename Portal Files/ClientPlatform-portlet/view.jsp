<%--
 @create on:	     1/04/2014
 @author:			 Konstantinos Pechlivanis
 @research program:  PREPARE - innovative integrative tools and platforms to be prepared for radiological emergencies and post-accident response in Europe
 @project:			 Analytic Platform (Work package 2)
  		-Task 2.1:   Development of Scientific means 
  		-Task 2.3:   Technical Platform
  		
 @document: 		 view.jsp from file /html/jsps/sender
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<portlet:defineObjects />
<portlet:resourceURL var="getInputQuery"></portlet:resourceURL>
<portlet:resourceURL var="getDuration"></portlet:resourceURL>


<!DOCTYPE html>
<html>
<head>
<title>Page of client</title>
	<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script type='text/javascript' src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
    <script type='text/javascript' src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script type='text/javascript' src='http://code.jquery.com/jquery.min.js'></script>

	<script>var title;</script>
	
	<!-- CSS styles for search box with placeholder text-->
	<style type="text/css">
	#search, 
	#submit { float: center;}
	
	#search {
	    padding: 5px 9px;
	    height: 33px;
	    width: 90%;
	    border: 1px solid #a4c3ca;
	    font: normal 13px 'trebuchet MS', arial, helvetica;
	    background: #f1f1f1;
	    border-radius: 50px 3px 3px 50px;
	    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.25) inset, 0 1px 0 rgba(255, 255, 255, 1);            
	}	
	#submit	{       
	    background-color: #6cbb6b;
	    background-image: linear-gradient(#95d788, #6cbb6b);
	    border-radius: 3px 50px 50px 3px;    
	    border-width: 1px;
	    border-style: solid;
	    border-color: #7eba7c #578e57 #447d43;
	    box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
	    height: 45px;
	    margin: 0 0 0 10px;
	    padding: 0;
	    width: 120px;
	    cursor: pointer;
	    font: bold 14px Arial, Helvetica;
	    color: #23441e;    
	    text-shadow: 0 1px 0 rgba(255,255,255,0.5);
	}	
	#submit:hover {       
	    background-color: #95d788;
	    background-image: linear-gradient(#6cbb6b, #95d788);
	}	
	#submit:active {       
	    background: #95d788;
	    outline: none;
	    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;        
	}
	#submit::-moz-focus-inner { border: 0;  /* Small centering fix for Firefox */ }
</style>

	<script>
	var query;
	function inputQuery(input){
		console.error("view.jsp inputQuery");
		query = input.inputText.value.trim() // parse html input
		$.ajax({//use ajax to send query input to java class
			url:'<%=getInputQuery%>',
			dataType: "json",
			data:{<portlet:namespace/>query:query},
			type: "get",
			success: function(data){
			Liferay.fire('getInputQuery', {query:data});
			},
			beforeSend: function(){console.log("view.jsp getInputQuery beforeSend");},
			complete: function(){console.log("view.jsp getInputQuery complete");},
			error: function(){console.error("getInputQuery error");}
		});
		return false;
	}</script>
	
	<%-- receive the selected crawler from CrawlersPortletAction class --%>
	<script>
	Liferay.on('getInputQuery',function(event) {
		$("#pdfRecomend").empty();
		for (var i=0;i<event.query.recommendation.length;i++){
			var url = event.query.recommendation[i].split(" Title:")[0].split("Url of article: ")[1];
			title = event.query.recommendation[i].split("Url of article: " + url)[1].split("<br><font")[0];
			var info = event.query.recommendation[i].split("Url of article: " + url)[1].split(title)[1];
			url = url + "?" + title.replace(/ /g, "\&");
			$("#pdfRecomend").append("<li><font size=\"4\"><a href="+ url + " target=\"ref\" onclick=\"popIt(this.href);return false;\" style=\"color:#0000FF\">"+ title +"</a></font>" + info + "</li>"); //gather articles
		}
	});
	</script>

	<script type="text/javascript">
	var ref; var duration;
	var c = 0; var t; var timer_is_on = 0;
	setInterval(function(){ closedWindow(); }, 1000);
	
	function closedWindow(){
		if(ref.closed && duration.indexOf(" seconds")==-1){ // case that user closes the articles clicking to open a new pdf
			ref = null;
			duration = duration + (c-1).toString() + " seconds"; // store of duration
			storeDuration(duration)
			clearTimeout(t);timer_is_on = 0;c=0;			// init time counters
			ref.close();	// close reference of window object
		}		
	}
	
	// function which is call itself every second to estimate the duration that a article is opened
	function timedCount() {
	    c = c + 1;	// counter for the duration
	    t = setTimeout(function(){ timedCount() }, 1000); // recall function every second
	}
	
	// this function is called when user clicks to open a pdf
	function popIt(url) {
		console.error("**************");
		console.log("popIt : "+url);
		title = url.split("?&")[1].trim().replace(/&/g," ").trim();
		if(ref && !ref.closed){ // case that user closes the articles clicking to open a new pdf
			duration = duration + (c-1).toString() + " seconds"; // store of duration
			storeDuration(duration)
			clearTimeout(t);timer_is_on = 0;c=0;			// init time counters
			ref.close();	// close reference of window object
		}
		
		if (!timer_is_on) {timer_is_on = 1;timedCount();}	// init time counters
		
		duration = "Query: " + query + "\n" + title + "\nOpen for : ";
		var leftPos = screen.width - 720;
		ref = window.open(url,"thePop","menubar=1,resizable=1,scrollbars=1,status=1,height=900,width=710,left="+leftPos+",top=0"); // create new window object with specific url and name
		ref.focus();	// sets focus to the current window
	}
	
	function storeDuration(duration){
		$.ajax({//use ajax to send query input to java class
			url:'<%=getDuration%>',
			dataType: "json",
			data:{<portlet:namespace/>query:duration},
			type: "get",
			success: function(data){
			Liferay.fire('getDuration', {duration:data});
			},
			beforeSend: function(){console.log("view.jsp getDuration beforeSend");},
			complete: function(){console.log("view.jsp getDuration complete");},
			error: function(){console.error("getDuration error");}
		});
		return false;
	}</script>

</head>

<body>
	<!-- HTML for SEARCH BAR -->
	<form id="searchbox" action="" method="GET" onsubmit="return inputQuery(this);" style="margin-bottom:30px;">
    	<input id="search" name="inputText" placeholder="Type a query here" style="margin-right: -5px;font-size:18px; font-family:Times New Roman,Times,serif; font-style: italic;">
    	<input id="submit" type="submit" value="Search">
	</form>
	
	<div id="header" style="background-color:DarkSeaGreen;">
	<h1 style="text-align:center;margin-bottom:0;">Recommended Articles</h1></div>
	<ul id="pdfRecomend"></ul>

</body>
</html>