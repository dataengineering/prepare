INTRODUCTION
============

This is the development repository of the PREPARE Crowd Sourcing
system, developed by NCSR "Demokritos" in the context of the
FP7 Euratom PREPARE project.

the PREPARE Crowd Sourcing system integrates:
 - The NOMAD crawler
 - A document manager for jointly managing crawled and other documents
 - Semantic searching through the document collection



BUILD INSTRUCTIONS
==================

In the instructions below, substitute Liferay 6.2 for Liferay 6.1.x
Certain path variables will have to be modified by hand in Eclipse

The PrepareData folder needed will be made available soon.

AnalyticPlatformPortlet requires app-compat-hook to function, this is
available on the Social Office app available in Liferay MarketPlace


Dependencies:

1.
Java version 1.7.0 or later.
Tested with
OpenJDK Runtime Environment (IcedTea 2.5.4) (7u75-2.5.4-1~trusty1)
OpenJDK 64-Bit Server VM (build 24.75-b04, mixed mode)

2.
MySQL 14.14 Distrib 5.6.19, for debian-linux-gnu (i686) using EditLine wrapper
server version: 5.6.19-0ubuntu0.14.04.1 (Ubuntu)

3.
sudo apt-get install jcc
Required for Crawler's service


Install boilerpipe 

sudo apt-get install g++
sudo easy_install boilerpipe
 

Create mySql database from dump file (located in Build 1.1 prepare files)
        -command line:  1)mysql -uroot -p (password: )
                        2)mysql > CREATE DATABASE prepare_crawler_db DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
                        3)mysql > use prepare_crawler_db;
                        4)mysql > source /path_of_dump_file/prepare_crawler_db.sql;
                                -4.1 Alternatively, run the prepare_crawler_db.sql code in mysql console to create database
                        5)mysql > CREATE user 'searchuser'@'localhost' IDENTIFIED BY 'searchUser_@!';
                        6)mysql > GRANT ALL PRIVILEGES ON prepare_crawler_db TO 'searchuser'@'localhost';
                        7)mysql > FLUSH PRIVILEGES
                        8)mysql > exit

        -CHANGES REQUIRED to /etc/mysql/my.cnf to support properly UTF-8:

                [client]
                default-character-set=utf8

                [mysql]
                default-character-set=utf8

                [mysqld]
                collation-server = utf8_unicode_ci
                character-set-server = utf8

4.
Python packages:
        $ sudo easy_install python-gflags
        $ sudo easy_install httplib2
        $ sudo apt-get install libmysqlclient-dev
        $ sudo easy_install MySQL-python
        $ sudo easy_install google-api-python-client

5.
Apache Tomcat 7.0.54
        -External Tomcat is required for Crawler's service
        -Extract file in path of your choice
        -Change the default port of Tomcat (localhost:8080) to localhost:8081
                1) Go to conf folder in tomcat installation directory  e.g. C:\Tomcat 7.0\conf\
                3) Edit following tag in server.xml file <Connector port="8080" protocol="HTTP/1.1"....>
                      --Change the port=8080 value to port=8081
                2) Edit following tag in server.xml file <Server port="8005" shutdown="SHUTDOWN">
                      --Change the port=8005 value to port=8015
                4) Edit following tag in server.xml file <Connector port="8009" protocol="AJP/1.3".... />
                      --Change the port=8009 value to port=8019
                5) Save file.
                6)-In order to accept UTF-8 parameters, add this param-value pair in Connector setting of your tomcat config file(conf/server.xml): URIEncoding="utf-8" (if not exist)

        -Place PrepareCrawlers.war (located in "Build related files" folder) in /webapps folder
        -Startup Tomcat Server (sh path_Of_Tomcat_Folder/bin/startup.sh )
        

6.
Create folder .PrepareData in home path. This folder has to contain a
sub-folder with name ontology. Sub-folder ontology has to contain the
rdf ontology (necessary files are in the "Build related files" of the Repo)

7.
Liferay Portal 6.2-ce-ga4 Bundled with Tomcat from : http://www.liferay.com/downloads/liferay-portal/available-releases 
        -Extract files in a path of your chοice
        -Use default port of Liferay's Tomcat, localhost:8080
        -Copy(deploy) "analyticPlatform-portlet" folder (which is located in the "Portal Files" folder in the repo) in "path_of_Liferay_folder/liferay-portal-6.2-ce-ga4/tomcat-7.0.40/webapps/"
	-Copy(deploy) "ClientPlatform-portlet" folder (which is located in the "Portal Files" folder in the repo) "path_of_Liferay_folder/liferay-portal-6.2-ce-ga4/tomcat-7.0.40/webapps/"
        -Startup Liferay Server (sh path_Of_Liferay_folder/liferay-portal-6.2-ce-ga4/tomcat-7.0.40/bin/startup.sh )
        -Type in your browser, localhost:8080
        -Click "Go To" and choose "My Public Pages"
        -On the top left of page, click "Manage" and choose "Page"
        -Click "Import", to import the available pages (Client-xxx.lar, Crawlers-xxxx.lar, Expert_Service-xxxx.lar)

8.
Cron
Set accesibility of crontab (files cron.deny and cron.allow) for your username





KNOWN ISSUES
============

When the Liferay server is down ιτ is not possible to create
indexes for the crawlers that have finished during in the period 
during which the server was down

When the has Liferay server has been restarted it is possible that it will create the last index
again. As a result, the data of last the index is duplicated.