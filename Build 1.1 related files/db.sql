CREATE DATABASE  IF NOT EXISTS `prepare_crawler_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `prepare_crawler_db`;

-- CREDENTIALS --
-- user: searchuser
-- pw: searchUser_@!
-- CREDENTIALS --
-- create credentials from command line -- 
-- CREATE USER 'searchuser'@'localhost' IDENTIFIED BY 'searchUser_@!';
-- GRANT ALL PRIVILEGES ON prepare_crawler_db.* TO 'searchuser'@'localhost';
-- create credentials from command line DONE --
-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: prepare_crawler_db
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `age`
--

DROP TABLE IF EXISTS `age`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `age` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minAge` int(11) NOT NULL,
  `maxAge` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- insert basic data
INSERT INTO `age` (`id`,`minAge`,`maxAge`) VALUES (1,18,24);
INSERT INTO `age` (`id`,`minAge`,`maxAge`) VALUES (2,25,34);
INSERT INTO `age` (`id`,`minAge`,`maxAge`) VALUES (3,35,44);
INSERT INTO `age` (`id`,`minAge`,`maxAge`) VALUES (4,45,54);
INSERT INTO `age` (`id`,`minAge`,`maxAge`) VALUES (5,55,64);

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bing_result`
--

DROP TABLE IF EXISTS `bing_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bing_result` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `query` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `url` text CHARACTER SET utf8,
  `url_hash` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `title` text CHARACTER SET utf8,
  `html_content` longblob,
  `clean_content` longtext CHARACTER SET utf8,
  `clean_content_hash` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `market` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `language` varchar(2) CHARACTER SET utf8 DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `query_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `url_hash_index` (`url_hash`),
  KEY `fk_bing_result_query1_idx` (`query_id`),
  CONSTRAINT `fk_bing_result_query1` FOREIGN KEY (`query_id`) REFERENCES `query` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `clean_text` mediumtext,
  `timestamp` datetime DEFAULT NULL,
  `crawl_timestamp` datetime NOT NULL,
  `language` varchar(2) DEFAULT NULL,
  `thread_id` int(11) DEFAULT '0',
  `processing_level` int(11) DEFAULT '0',
  `source_table` varchar(45) DEFAULT NULL,
  `query_id` bigint(20) NOT NULL,
  `crawl_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_content_query_idx` (`query_id`),
  KEY `fk_content_crawl_id_idx` (`crawl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crawl_log`
--

DROP TABLE IF EXISTS `crawl_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crawl_log` (
  `crawl_id` int(11) NOT NULL AUTO_INCREMENT,
  `crawl_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `num_docs` int(11) DEFAULT NULL,
  `previous_docs_sum` int(11) DEFAULT NULL,
  PRIMARY KEY (`crawl_id`),
  UNIQUE KEY `crawl_id_name_UNIQUE` (`crawl_id`,`crawl_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `crawl_query_lkp`
--

DROP TABLE IF EXISTS `crawl_query_lkp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crawl_query_lkp` (
  `crawl_id` int(11) DEFAULT NULL,
  `query_id` bigint(20) DEFAULT NULL,
  `crawler_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_docs` int(11) DEFAULT NULL,
  UNIQUE KEY `crawl_query_crawler_unique` (`crawl_id`,`query_id`,`crawler_name`),
  KEY `fk_crawl_query_lkp_1_idx` (`crawl_id`),
  KEY `fk_crawl_query_lkp_2_idx` (`query_id`),
  CONSTRAINT `fk_crawl_query_lkp_1` FOREIGN KEY (`crawl_id`) REFERENCES `crawl_log` (`crawl_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_crawl_query_lkp_2` FOREIGN KEY (`query_id`) REFERENCES `query` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `canonical_string` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `education` (`id`,`canonical_string`) VALUES (1,'No College');
INSERT INTO `education` (`id`,`canonical_string`) VALUES (2,'Some College');
INSERT INTO `education` (`id`,`canonical_string`) VALUES (3,'Graduate School');
INSERT INTO `education` (`id`,`canonical_string`) VALUES (4,'College');
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `education_description`
--

DROP TABLE IF EXISTS `education_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `education_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL,
  `education_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_education_id_idx` (`education_id`),
  KEY `fk_language_id_idx` (`language_id`),
  CONSTRAINT `fk_education_id` FOREIGN KEY (`education_id`) REFERENCES `education` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ed_desc_language_id` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gender`
--

DROP TABLE IF EXISTS `gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `canonical_string` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- inserts
INSERT INTO `gender` (`id`,`canonical_string`) VALUES (1,'Male');
INSERT INTO `gender` (`id`,`canonical_string`) VALUES (2,'Female');
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gender_description`
--

DROP TABLE IF EXISTS `gender_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gender_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) NOT NULL,
  `gender_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gender_desc_gender_id_idx` (`gender_id`),
  KEY `fk_gender_desc_language_id_idx` (`language_id`),
  CONSTRAINT `fk_gender_desc_gender_id` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gender_desc_language_id` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
-- inserts
INSERT INTO `gender_description` (`id`,`description`,`gender_id`,`language_id`) VALUES (1,'Άντρας',1,1);
INSERT INTO `gender_description` (`id`,`description`,`gender_id`,`language_id`) VALUES (2,'Male',1,2);
INSERT INTO `gender_description` (`id`,`description`,`gender_id`,`language_id`) VALUES (3,'Mann',1,3);
INSERT INTO `gender_description` (`id`,`description`,`gender_id`,`language_id`) VALUES (4,'Γυναίκα',2,1);
INSERT INTO `gender_description` (`id`,`description`,`gender_id`,`language_id`) VALUES (5,'Woman',2,2);
INSERT INTO `gender_description` (`id`,`description`,`gender_id`,`language_id`) VALUES (6,'Frau',2,3);

/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `demographics`
--

DROP TABLE IF EXISTS `demographics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `demographics` (
  `location_id` int(11) DEFAULT NULL,
  `age_id` int(11) DEFAULT NULL,
  `gender_id` int(11) DEFAULT NULL,
  `education_id` int(11) DEFAULT NULL,
  `content_id` bigint(20) NOT NULL,
  PRIMARY KEY (`content_id`),
  KEY `fk_nomad_demographics_location1` (`location_id`),
  KEY `fk_nomad_demographics_age1` (`age_id`),
  KEY `fk_nomad_demographics_gender1` (`gender_id`),
  KEY `fk_nomad_demographics_education1` (`education_id`),
  KEY `fk_nomad_demographics_nomad_content_idx` (`content_id`),
  CONSTRAINT `fk_nomad_demographics_age1` FOREIGN KEY (`age_id`) REFERENCES `age` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_nomad_demographics_education1` FOREIGN KEY (`education_id`) REFERENCES `education` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_nomad_demographics_gender1` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_nomad_demographics_location1` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_nomad_demographics_nomad_content` FOREIGN KEY (`content_id`) REFERENCES `content` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `gplus_comment`
--

DROP TABLE IF EXISTS `gplus_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gplus_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_id` varchar(255) NOT NULL,
  `api_self_link` text,
  `published` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `plusoners` bigint(20) DEFAULT NULL,
  `activity` varchar(45) DEFAULT NULL,
  `content` longtext,
  `language` varchar(2) DEFAULT NULL,
  `gplus_post_id` bigint(20) NOT NULL,
  `gplus_user_id` bigint(20) NOT NULL,
  `query_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_gplus_comment_gplus_post1_idx` (`gplus_post_id`),
  KEY `fk_gplus_comment_gplus_user1_idx` (`gplus_user_id`),
  KEY `fk_gplus_comment_query1_idx` (`query_id`),
  CONSTRAINT `fk_gplus_comment_gplus_post1` FOREIGN KEY (`gplus_post_id`) REFERENCES `gplus_post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gplus_comment_gplus_user1` FOREIGN KEY (`gplus_user_id`) REFERENCES `gplus_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gplus_comment_query1` FOREIGN KEY (`query_id`) REFERENCES `query` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gplus_post`
--

DROP TABLE IF EXISTS `gplus_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gplus_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_id` varchar(255) NOT NULL,
  `activity_url` text,
  `published` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `plusoners` bigint(20) DEFAULT NULL,
  `reshares` bigint(20) DEFAULT NULL,
  `title` text,
  `content_url` text,
  `content` longtext,
  `language` varchar(2) DEFAULT NULL,
  `gplus_user_id` bigint(20) NOT NULL,
  `query_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_gplus_post_gplus_user_idx` (`gplus_user_id`),
  KEY `fk_gplus_post_query1_idx` (`query_id`),
  CONSTRAINT `fk_gplus_post_gplus_user` FOREIGN KEY (`gplus_user_id`) REFERENCES `gplus_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gplus_post_query1` FOREIGN KEY (`query_id`) REFERENCES `query` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `gplus_user`
--

DROP TABLE IF EXISTS `gplus_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gplus_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `url` text,
  `name` text,
  `gender` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `iso_code` varchar(2) NOT NULL,
  `depiction` varchar(45),
  PRIMARY KEY (`id`),
  UNIQUE KEY `iso_code_UNIQUE` (`iso_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `language` (`id`, `iso_code`, `depiction`) VALUES (1, "el", "Greek");
INSERT INTO `language` (`id`, `iso_code`, `depiction`) VALUES (2, "en", "English");
INSERT INTO `language` (`id`, `iso_code`, `depiction`) VALUES (3, "de", "German");
INSERT INTO `language` (`id`, `iso_code`, `depiction`) VALUES (4, "fr", "French");
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `canonical_string` varchar(45) NOT NULL,
  `geoname_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `geoname_id_UNIQUE` (`geoname_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `location_description`
--

DROP TABLE IF EXISTS `location_description`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `region_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `language_id_idx` (`language_id`),
  KEY `fk_location_id_idx` (`location_id`),
  CONSTRAINT `fk_language_id` FOREIGN KEY (`language_id`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_id` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `query`
--

DROP TABLE IF EXISTS `query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `query` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `query_term` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL,
  `num_of_executions` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `query_term_UNIQUE` (`query_term`),
  KEY `fk_query_language_idx` (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `youtube_category`
--

DROP TABLE IF EXISTS `youtube_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_category` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_UNIQUE` (`category`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `youtube_comment`
--

DROP TABLE IF EXISTS `youtube_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment_id` varchar(255) NOT NULL,
  `url` text,
  `content` text,
  `date` datetime DEFAULT NULL,
  `rating` varchar(45) DEFAULT NULL,
  `language` varchar(2) DEFAULT NULL,
  `youtube_video_id` bigint(20) NOT NULL,
  `youtube_user_id` bigint(20) NOT NULL,
  `query_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_youtube_comment_youtube_video1_idx` (`youtube_video_id`),
  KEY `fk_youtube_comment_youtube_user1_idx` (`youtube_user_id`),
  KEY `fk_youtube_comment_query1_idx` (`query_id`),
  CONSTRAINT `fk_youtube_comment_query1` FOREIGN KEY (`query_id`) REFERENCES `query` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_youtube_comment_youtube_user1` FOREIGN KEY (`youtube_user_id`) REFERENCES `youtube_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_youtube_comment_youtube_video1` FOREIGN KEY (`youtube_video_id`) REFERENCES `youtube_video` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `youtube_user`
--

DROP TABLE IF EXISTS `youtube_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `age` smallint(6) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `hometown` text,
  `location` text,
  `occupation` text,
  `relationship` text,
  `school` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `youtube_video`
--

DROP TABLE IF EXISTS `youtube_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_video` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `video_id` varchar(255) NOT NULL,
  `title` text,
  `description` text,
  `average_rating` float DEFAULT NULL,
  `view_count` bigint(20) DEFAULT NULL,
  `favorite_count` bigint(20) DEFAULT NULL,
  `location` text,
  `publiced` datetime DEFAULT NULL,
  `recorded` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `youtube_user_id` bigint(20) NOT NULL,
  `query_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `fk_youtube_video_youtube_user1_idx` (`youtube_user_id`),
  KEY `fk_youtube_video_query1_idx` (`query_id`),
  CONSTRAINT `fk_youtube_video_query1` FOREIGN KEY (`query_id`) REFERENCES `query` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_youtube_video_youtube_user1` FOREIGN KEY (`youtube_user_id`) REFERENCES `youtube_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `youtube_video_has_category`
--

DROP TABLE IF EXISTS `youtube_video_has_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_video_has_category` (
  `youtube_video_id` bigint(20) NOT NULL,
  `youtube_category_id` bigint(20) NOT NULL,
  PRIMARY KEY (`youtube_video_id`,`youtube_category_id`),
  KEY `fk_youtube_video_has_youtube_category_youtube_category1_idx` (`youtube_category_id`),
  KEY `fk_youtube_video_has_youtube_category_youtube_video1_idx` (`youtube_video_id`),
  CONSTRAINT `fk_youtube_video_has_youtube_category_youtube_category1` FOREIGN KEY (`youtube_category_id`) REFERENCES `youtube_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_youtube_video_has_youtube_category_youtube_video1` FOREIGN KEY (`youtube_video_id`) REFERENCES `youtube_video` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `twitter_source`;
CREATE TABLE `twitter_source` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(45) NOT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `account_name_UNIQUE` (`account_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `twitter_user`;
CREATE TABLE `twitter_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `followers_count` int(11) DEFAULT NULL,
  `friends_count` int(11) DEFAULT NULL,
  `listed_count` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `screen_name` varchar(45) DEFAULT NULL,
  `location` varchar(45) DEFAULT NULL,
  `statuses_count` int(11) DEFAULT NULL,
  `timezone` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `twitter_post`;
CREATE TABLE `twitter_post` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `coordinates` varchar(255) DEFAULT NULL,
  `place` text,
  `retweet_count` bigint(20) DEFAULT NULL,
  `followers_when_published` int(11) DEFAULT NULL,
  `text` varchar(140) DEFAULT NULL,
  `language` varchar(2) DEFAULT NULL,
  `url` text,
  `twitter_user_id` bigint(20) NOT NULL,
  `twitter_source_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `twitter_id_UNIQUE` (`post_id`),
  KEY `fk_twitter_post_twitter_user1_idx` (`twitter_user_id`),
  KEY `fk_twitter_post_twitter_source1_idx` (`twitter_source_id`),
  KEY `created_at_idx` (`created_at`),
  KEY `language_idx` (`language`),
  CONSTRAINT `fk_twitter_post_twitter_source1` FOREIGN KEY (`twitter_source_id`) REFERENCES `twitter_source` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_twitter_post_twitter_user1` FOREIGN KEY (`twitter_user_id`) REFERENCES `twitter_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `twitter_hashtag`;
CREATE TABLE `twitter_hashtag` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `hashtag` varchar(139) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hashtag_UNIQUE` (`hashtag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `twitter_post_has_hashtag`;
CREATE TABLE `twitter_post_has_hashtag` (
  `twitter_post_id` bigint(20) NOT NULL,
  `twitter_hashtag_id` bigint(20) NOT NULL,
  PRIMARY KEY (`twitter_post_id`,`twitter_hashtag_id`),
  KEY `fk_twitter_post_has_twitter_hashtag_twitter_hashtag1_idx` (`twitter_hashtag_id`),
  KEY `fk_twitter_post_has_twitter_hashtag_twitter_post1_idx` (`twitter_post_id`),
  CONSTRAINT `fk_twitter_post_has_twitter_hashtag_twitter_hashtag1` FOREIGN KEY (`twitter_hashtag_id`) REFERENCES `twitter_hashtag` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_twitter_post_has_twitter_hashtag_twitter_post1` FOREIGN KEY (`twitter_post_id`) REFERENCES `twitter_post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `twitter_external_link`;
CREATE TABLE `twitter_external_link` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `post_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index3` (`url`,`post_id`),
  KEY `fk_twitter_external_link_1` (`post_id`),
  CONSTRAINT `fk_twitter_external_link_1` FOREIGN KEY (`post_id`) REFERENCES `twitter_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-10-19 13:45:26
