# run db.sql script to generate db schema (inside are the credentials for a default user)

# requires apache tomcat 7 or later
In order to accept UTF-8 parameters, add this param-value pair in Connector setting of your tomcat config file(conf/server.xml): 
URIEncoding="utf-8"
(if it does not exist)

# uses dbcp for database access
injection params in META-INF/context.xml

# CHANGES REQUIRED to /etc/mysql/my.cnf to support properly UTF-8

[client]
default-character-set=utf8

[mysql]
default-character-set=utf8

[mysqld]
collation-server = utf8_unicode_ci
character-set-server = utf8

# in order for GplusCrawler to run, boilerpipe python bindings must be installed in the machine. 
See WEB-INF/gplus_sript/gplus_main.py for instructions or http://jcc.readthedocs.org/en/latest/

#place PrepareCrawlers.war in secondary apache server

#recreate the database with db.sql and insert.sql (Either load files via terminal live in version 1.0 or copy-paste the code in the MySQL Workbench scripting interface)
