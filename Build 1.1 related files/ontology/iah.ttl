@prefix : <http://rdf.iit.demokritos.gr/2014/iah/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix iaea: <http://rdf.iit.demokritos.gr/2014/iaeaSafetyGlossary#> .
@prefix nreo: <http://rdf.iit.demokritos.gr/2014/nreo#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix nreo-ent: <http://rdf.demokritos.gr/2014/nreo-entities/> .
@base <http://rdf.iit.demokritos.gr/2014/iah> .

<http://rdf.iit.demokritos.gr/2014/iah> rdf:type owl:Ontology ;
                                       
                                       owl:versionInfo "1.0"^^xsd:string ;
                                       
                                       dct:title "Inhabited Areas Handbook Terminology (IAH)"^^xsd:string ;
                                       
                                       dct:creator "Stasinos Konstantopoulos <stasinos@konstant.gr>" ;
                                       
                                       dct:description "This SKOS terminology organizes the concepts in the Glossary of the \"Generic handbook for assisting in the management of contaminated inhabited areas in Europe following a radiological emergency\", EURANOS project, March 2010."@en ;
                                       
                                       dct:license <http://creativecommons.org/licenses/by/3.0/> ;
                                       
                                       owl:imports <http://rdf.iit.demokritos.gr/2014/iaeaSafetyGlossary> ,
                                                   <http://rdf.iit.demokritos.gr/2014/nreo-entities> .


#################################################################
#
#    Annotation properties
#
#################################################################


###  http://purl.org/dc/terms/title

dct:title rdf:type owl:AnnotationProperty .





#################################################################
#
#    Individuals
#
#################################################################


###  http://rdf.iit.demokritos.gr/2014/iah/Activity

:Activity rdf:type nreo:MeasurementTerm ,
                   owl:NamedIndividual ;
          
          skos:definition "The rate at which nuclear decays occur in a given amount of radioactive material."@en ;
          
          skos:prefLabel "activity"@en ;
          
          skos:inScheme :IAHGlossary ;
          
          nreo:usesUnit nreo:Becquerel ;
          
          skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/ActivityConcentration

:ActivityConcentration rdf:type nreo:MeasurementTerm ,
                                owl:NamedIndividual ;
                       
                       skos:definition "The activity per unit mass of a radioactive material."@en ;
                       
                       skos:prefLabel "activity concentration"@en ;
                       
                       skos:inScheme :IAHGlossary ;
                       
                       nreo:usesUnit nreo:BqperKg ;
                       
                       skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/ActivityLevel

:ActivityLevel rdf:type nreo:MeasurementTerm ,
                        nreo:NREmergencyManagementTerm ,
                        owl:NamedIndividual ;
               
               skos:definition "The level of dose rate, activity concentration or any other measurable quantity above which intervention should be undertaken during chronic or emergency exposure."@en ;
               
               skos:prefLabel "activity level"@en ;
               
               skos:inScheme :IAHGlossary ;
               
               skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/AlphaRadiation

:AlphaRadiation rdf:type nreo:NuclearEngineeringTerm ,
                         owl:NamedIndividual ;
                
                skos:definition "A particle which consists of two protons and two neutrons (identical to a nucleus of helium). Emitted by the nucleus of a radionuclide during alpha decay."@en ;
                
                skos:prefLabel "alpha particle"@en ;
                
                skos:altLabel "α particle"@en ;
                
                skos:inScheme :IAHGlossary ;
                
                skos:broader nreo:Radiation .



###  http://rdf.iit.demokritos.gr/2014/iah/BetaRadiation

:BetaRadiation rdf:type nreo:NuclearEngineeringTerm ,
                        owl:NamedIndividual ;
               
               skos:definition "A particle consisting of a fast moving electron or positron. Emitted by the nucleus during beta decay."@en ;
               
               skos:prefLabel "beta particle"@en ;
               
               skos:altLabel "β particle"@en ;
               
               skos:inScheme :IAHGlossary ;
               
               skos:broader nreo:Radiation .



###  http://rdf.iit.demokritos.gr/2014/iah/CollectiveDose

:CollectiveDose rdf:type nreo:MeasurementTerm ,
                         owl:NamedIndividual ;
                
                skos:definition "The sum of individual doses in a specified population. Often approximated to be the average effective dose in a population exposed to a particular source of ionising radiation multiplied by the number of people exposed."@en ;
                
                skos:prefLabel "collective dose"@en ;
                
                skos:broader :Dose ;
                
                skos:inScheme :IAHGlossary ;
                
                nreo:usesUnit nreo:manSievert .



###  http://rdf.iit.demokritos.gr/2014/iah/Datasheet

:Datasheet rdf:type nreo:NREmergencyManagementTerm ,
                    owl:NamedIndividual ;
           
           skos:definition "A compilation of data and information about a management option designed to support decision-makers in the evaluation of an option and the impact of its implementation."@en ;
           
           skos:prefLabel "datasheet"@en ;
           
           skos:inScheme :IAHGlossary ;
           
           skos:related :ManagementOption ;
           
           skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/DecontaminationFactor

:DecontaminationFactor rdf:type nreo:MeasurementTerm ,
                                owl:NamedIndividual ;
                       
                       nreo:abbreviation "DF"@en ;
                       
                       skos:definition "Effectiveness of a removal option is expressed as a Decontamination Factor (DF). The DF is the ratio of the amount of contamination initially present on a specific surface (e.g. buildings, paved surfaces, grass, soil, and shrubs, etc.) to that remaining after implementing the option. For example, a DF of 5 indicates that 80% of the activity can be removed."@en ;
                       
                       skos:prefLabel "decontamination factor"@en ;
                       
                       skos:inScheme :IAHGlossary ;
                       
                       skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/DeterministicEffect

:DeterministicEffect rdf:type nreo:EffectsTerm ,
                              owl:NamedIndividual ;
                     
                     skos:definition "Deterministic effect: Previously known as a non-stochastic effect. A radiation-induced health effect characterised by a severity which increases with dose above some clinical threshold, and above which threshold such effects are always observed. Examples of deterministic effects are nausea and radiation burns."@en ;
                     
                     nreo:inContextAltLabel "deterministic effect"@en ;
                     
                     skos:prefLabel "deterministic health effect"@en ;
                     
                     nreo:obsoleteLabel "non-stochastic effect"@en ;
                     
                     skos:inScheme :IAHGlossary ;
                     
                     skos:broader nreo:Effect .



###  http://rdf.iit.demokritos.gr/2014/iah/Dose

:Dose rdf:type nreo:MeasurementTerm ,
               owl:NamedIndividual ;
      
      skos:definition "General term used for a quantity of ionising radiation. Unless used in a specific context, it refers to the effective dose."@en ;
      
      skos:prefLabel "dose"@en ;
      
      skos:inScheme :IAHGlossary ;
      
      skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/EffectiveDose

:EffectiveDose rdf:type nreo:MeasurementTerm ,
                        owl:NamedIndividual ;
               
               skos:definition "The effective dose is the sum of the weighted equivalent doses in all the tissues and organs of the body. It takes account of the relative biological effectiveness of different types of radiation and variation in the susceptibility of organs and tissues to radiation damage."@en ;
               
               nreo:inContextAltLabel "dose"@en ;
               
               skos:prefLabel "effective dose"@en ;
               
               skos:broader :Dose ;
               
               skos:related :EffectiveDoseRate ;
               
               skos:inScheme :IAHGlossary ;
               
               skos:related nreo:Radiation ;
               
               nreo:usesUnit nreo:Sievert .



###  http://rdf.iit.demokritos.gr/2014/iah/EffectiveDoseRate

:EffectiveDoseRate rdf:type nreo:MeasurementTerm ,
                            owl:NamedIndividual ;
                   
                   skos:definition "Dose rate: General term used for a quantity of ionising radiation received per unit time. Unless used in reference to a particular organ in the body, it refers to the effective dose rate."@en ;
                   
                   nreo:inContextAltLabel "dose rate"@en ;
                   
                   skos:prefLabel "effective dose rate"@en ;
                   
                   skos:inScheme :IAHGlossary ;
                   
                   skos:related :IonisingRadiation ;
                   
                   skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/EmergencyCountermeasure

:EmergencyCountermeasure rdf:type nreo:NREmergencyManagementTerm ,
                                  owl:NamedIndividual ;
                         
                         skos:definition "Actions taken during the emergency phase with the aim of protecting people from short-term relatively high radiation exposures, e.g. evacuation, sheltering, taking stable iodine tablets."@en ;
                         
                         skos:prefLabel "emergency countermeasure"@en ;
                         
                         skos:related :EmergencyPhase ;
                         
                         skos:inScheme :IAHGlossary ;
                         
                         skos:broader :ManagementOption .



###  http://rdf.iit.demokritos.gr/2014/iah/EmergencyPhase

:EmergencyPhase rdf:type nreo:SpatialTemporalTerm ,
                         owl:NamedIndividual ;
                
                skos:definition "The time period during which urgent actions are required to protect people from short-term relatively high radiation exposures in the event of a radiological emergency or incident."@en ;
                
                skos:altLabel "early phase"@en ;
                
                skos:prefLabel "emergency phase"@en ;
                
                skos:inScheme :IAHGlossary ;
                
                skos:broader nreo:Procedure .



###  http://rdf.iit.demokritos.gr/2014/iah/EquivalentDose

:EquivalentDose rdf:type nreo:MeasurementTerm ,
                         owl:NamedIndividual ;
                
                skos:definition "A quantity used in radiological protection dosimetry, which incorporates the ability of different types of radiation to cause harm in living tissue."@en ;
                
                skos:prefLabel "equivalent dose"@en ;
                
                skos:broader :Dose ;
                
                skos:inScheme :IAHGlossary ;
                
                nreo:usesUnit nreo:Sievert .



###  http://rdf.iit.demokritos.gr/2014/iah/GammaRadiation

:GammaRadiation rdf:type nreo:NuclearEngineeringTerm ,
                         owl:NamedIndividual ;
                
                skos:definition "High energy photons, without mass or charge, emitted from the nucleus of a radionuclide following radioactive decay, as an electromagnetic wave. They are very penetrating."@en ;
                
                skos:prefLabel "gamma ray"@en ;
                
                nreo:abbreviation "γ"@en ;
                
                skos:altLabel "γ ray"@en ;
                
                skos:inScheme :IAHGlossary ;
                
                skos:broader nreo:Radiation .



###  http://rdf.iit.demokritos.gr/2014/iah/HalfLife

:HalfLife rdf:type nreo:MeasurementTerm ,
                   owl:NamedIndividual ;
          
          skos:definition "The time taken for the activity of a radionuclide to lose half its value by decay."@en ;
          
          skos:prefLabel "half-life"@en ;
          
          skos:inScheme :IAHGlossary ;
          
          skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/IAHGlossary

:IAHGlossary rdf:type owl:NamedIndividual ,
                      skos:ConceptScheme ;
             
             dct:title "Glossary of the \"Generic handbook for assisting in the management of contaminated inhabited areas in Europe following a radiological emergency\", EURANOS project, March 2010."@en ;
             
             skos:editorialNote "This scheme organizes the concepts in the Glossary of the \"Generic handbook for assisting in the management of contaminated inhabited areas in Europe following a radiological emergency\", EURANOS project, March 2010."@en ;
             
             dct:description "This scheme organizes the concepts in the Glossary of the \"Generic handbook for assisting in the management of contaminated inhabited areas in Europe following a radiological emergency\", EURANOS project, March 2010."@en .



###  http://rdf.iit.demokritos.gr/2014/iah/IncrementalDose

:IncrementalDose rdf:type nreo:MeasurementTerm ,
                          owl:NamedIndividual ;
                 
                 skos:definition "The additional dose received by an individual as a result of implementing a management option that specifically does not take into account exposure to activity already present in the environment as a result of deposition of radionuclides on the ground."@en ;
                 
                 skos:prefLabel "incremental dose"@en ;
                 
                 skos:broader :Dose ;
                 
                 skos:inScheme :IAHGlossary .



###  http://rdf.iit.demokritos.gr/2014/iah/InhabitedArea

:InhabitedArea rdf:type nreo:SpatialTemporalTerm ,
                        owl:NamedIndividual ;
               
               skos:definition "Places where people spend time (e.g. at home, at work and during recreation)."@en ;
               
               skos:prefLabel "inhabited area"@en ;
               
               skos:inScheme :IAHGlossary ;
               
               skos:broader nreo:Location .



###  http://rdf.iit.demokritos.gr/2014/iah/IonisingRadiation

:IonisingRadiation rdf:type nreo:NuclearEngineeringTerm ,
                            owl:NamedIndividual ;
                   
                   skos:definition "Radiation that produces ionisation in matter. Examples are alpha particles, gamma rays, x-rays and neutrons. When these radiations pass through the tissues of the body, they have sufficient energy to damage DNA."@en ;
                   
                   skos:prefLabel "ionising radiation"@en ;
                   
                   skos:inScheme :IAHGlossary ;
                   
                   skos:broader nreo:Radiation .



###  http://rdf.iit.demokritos.gr/2014/iah/Isotope

:Isotope rdf:type nreo:NuclearEngineeringTerm ,
                  owl:NamedIndividual ;
         
         skos:definition "Nuclides with the same number of protons (i.e. same atomic number) but different numbers of neutrons. Not a synonym for nuclide."@en ;
         
         skos:prefLabel "isotope"@en ;
         
         skos:inScheme :IAHGlossary ;
         
         skos:broader nreo:PhysicsThing .



###  http://rdf.iit.demokritos.gr/2014/iah/LocationFactor

:LocationFactor rdf:type nreo:MeasurementTerm ,
                         owl:NamedIndividual ;
                
                skos:definition "Ratio of the dose rate determined at a particular location to that in a reference location. Typically used in the estimation of doses to people indoors from measurements made in an outdoor reference location. For example, the dose rate inside a typical residential building could be ten times lower than that above a reference outdoor open grass area; in this case the location factor would have a value of 0.1."@en ;
                
                skos:prefLabel "location factor"@en ;
                
                skos:inScheme :IAHGlossary ;
                
                skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/LonglivedRadionuclide

:LonglivedRadionuclide rdf:type nreo:NuclearEngineeringTerm ,
                                owl:NamedIndividual ;
                       
                       skos:definition "Defined for the Handbook as radionuclides with a radioactive half-life greater than three weeks."@en ;
                       
                       skos:prefLabel "long-lived radionuclide"@en ;
                       
                       skos:inScheme :IAHGlossary ;
                       
                       skos:broader :Radionuclide .



###  http://rdf.iit.demokritos.gr/2014/iah/ManagementOption

:ManagementOption rdf:type nreo:NREmergencyManagementTerm ,
                           owl:NamedIndividual ;
                  
                  skos:definition "An action, which is part of an intervention, intended to reduce or avert the contamination or likelihood of contamination of food production systems. Previously known as a \"countermeasure\"."@en ;
                  
                  skos:hiddenLabel "countermeasure"@en ;
                  
                  skos:prefLabel "management option"@en ;
                  
                  skos:inScheme :IAHGlossary ;
                  
                  skos:broader nreo:Procedure .



###  http://rdf.iit.demokritos.gr/2014/iah/Molecule

:Molecule rdf:type nreo:NuclearEngineeringTerm ,
                   owl:NamedIndividual ;
          
          skos:definition "The smallest division of a substance that can exist independently while retaining the properties of that substance."@en ;
          
          skos:prefLabel "molecule"@en ;
          
          skos:inScheme :IAHGlossary ;
          
          skos:broader nreo:PhysicsThing .



###  http://rdf.iit.demokritos.gr/2014/iah/NormalLifestyle

:NormalLifestyle rdf:type nreo:NREmergencyManagementTerm ,
                          owl:NamedIndividual ;
                 
                 skos:definition "Situation where people can live and work in an area without the radiological emergency and its consequences being foremost in their minds."@en ;
                 
                 skos:prefLabel "normal lifestyle"@en ;
                 
                 skos:altLabel "normal living"@en ;
                 
                 skos:inScheme :IAHGlossary ;
                 
                 skos:broader nreo:Procedure .



###  http://rdf.iit.demokritos.gr/2014/iah/OccupancyFactor

:OccupancyFactor rdf:type nreo:MeasurementTerm ,
                          owl:NamedIndividual ;
                 
                 skos:definition "Fraction of the time spent in a particular location, e.g. inside and outside buildings. Typically used in the estimation of \"normal living\" doses, i.e. taking into account normal day-to-day activities."@en ;
                 
                 skos:prefLabel "occupancy factor"@en ;
                 
                 skos:inScheme :IAHGlossary ;
                 
                 skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/PersonalProtectiveEquipment

:PersonalProtectiveEquipment rdf:type nreo:ResourcesTerm ,
                                      owl:NamedIndividual ;
                             
                             skos:definition "Equipment worn by a person at work to protect against one or more health or safety risks e.g. safety helmets, gloves, eye protection, high-visibility clothing, safety footwear and safety harnesses."@en ;
                             
                             nreo:abbreviation "PPE"@en ;
                             
                             skos:prefLabel "personal protective equipment"@en ;
                             
                             skos:inScheme :IAHGlossary ;
                             
                             skos:broader nreo:Equipment .



###  http://rdf.iit.demokritos.gr/2014/iah/Photon

:Photon rdf:type nreo:NuclearEngineeringTerm ,
                 owl:NamedIndividual ;
        
        skos:definition "A quantum or packet of electromagnetic radiation (e.g. gamma rays or visible light) which may be considered a particle."@en ;
        
        skos:prefLabel "photon"@en ;
        
        skos:inScheme :IAHGlossary ;
        
        skos:broader nreo:PhysicsThing .



###  http://rdf.iit.demokritos.gr/2014/iah/RadioactiveContamination

:RadioactiveContamination rdf:type nreo:NuclearEngineeringTerm ,
                                   owl:NamedIndividual ;
                          
                          skos:definition "The deposition of radioactive material on the surfaces in inhabited areas or onto or into drinking water sources and supplies."@en ;
                          
                          skos:hiddenLabel "contamination"@en ;
                          
                          skos:prefLabel "radioactive contamination"@en ;
                          
                          skos:inScheme :IAHGlossary ;
                          
                          skos:broader nreo:PhysicsThing .



###  http://rdf.iit.demokritos.gr/2014/iah/RadioactiveDecay

:RadioactiveDecay rdf:type nreo:NuclearEngineeringTerm ,
                           owl:NamedIndividual ;
                  
                  skos:definition "The process by which radionuclides undergo spontaneous nuclear change, thereby emitting ionising radiation."@en ;
                  
                  skos:prefLabel "radioactive decay"@en ;
                  
                  skos:inScheme :IAHGlossary ;
                  
                  skos:broader nreo:PhysicsThing .



###  http://rdf.iit.demokritos.gr/2014/iah/Radioactivity

:Radioactivity rdf:type nreo:MeasurementTerm ,
                        owl:NamedIndividual ;
               
               skos:definition "The spontaneous emission of ionising radiation from a radionuclide as a result of atomic or nuclear changes. Measured in Becquerels, Bq."@en ;
               
               skos:prefLabel "radioactivity"@en ;
               
               skos:inScheme :IAHGlossary ;
               
               nreo:usesUnit nreo:Becquerel ;
               
               skos:broader nreo:Quantity .



###  http://rdf.iit.demokritos.gr/2014/iah/RadiologicalIncident

:RadiologicalIncident rdf:type nreo:SpatialTemporalTerm ,
                               owl:NamedIndividual ;
                      
                      skos:definition "Any event, accidental or otherwise, which involves a release of radioactivity into the environment."@en ;
                      
                      skos:altLabel "radiological emergency"@en ;
                      
                      skos:prefLabel "radiological incident"@en ;
                      
                      skos:inScheme :IAHGlossary ;
                      
                      skos:broader nreo:Event .



###  http://rdf.iit.demokritos.gr/2014/iah/Radionuclide

:Radionuclide rdf:type nreo:NuclearEngineeringTerm ,
                       owl:NamedIndividual ;
              
              skos:definition "A type of atomic nucleus which is unstable and which may undergo spontaneous decay to another atom by emission of ionising radiation, usually alpha, beta or gamma radiation."@en ;
              
              skos:prefLabel "radionuclide"@en ;
              
              skos:inScheme :IAHGlossary ;
              
              skos:broader nreo:PhysicsThing .



###  http://rdf.iit.demokritos.gr/2014/iah/RecoveryPhase

:RecoveryPhase rdf:type nreo:SpatialTemporalTerm ,
                        owl:NamedIndividual ;
               
               skos:definition "The time period during which activities focus on the restoration of normal lifestyles for all affected populations. There are no exact boundaries between the emergency phase and the recovery phase. However, within the Handbook the recovery phase should be seen as starting after the incident has been contained."@en ;
               
               skos:prefLabel "recovery phase"@en ;
               
               skos:inScheme :IAHGlossary ;
               
               skos:broader nreo:Procedure .



###  http://rdf.iit.demokritos.gr/2014/iah/RecoveryStrategy

:RecoveryStrategy rdf:type nreo:NREmergencyManagementTerm ,
                           owl:NamedIndividual ;
                  
                  skos:definition "A strategy which aims for a return to normal living. It covers all aspects of the long-term management of the contaminated area and the implementation of specific management options. The development of the strategy should involve all stakeholders."@en ;
                  
                  skos:prefLabel "recovery strategy"@en ;
                  
                  skos:inScheme :IAHGlossary ;
                  
                  skos:broader nreo:Procedure .



###  http://rdf.iit.demokritos.gr/2014/iah/RespiratoryProtection

:RespiratoryProtection rdf:type nreo:ResourcesTerm ,
                                owl:NamedIndividual ;
                       
                       skos:definition "Equipment designed to prevent or reduce the inhalation of radioactive material by individuals."@en ;
                       
                       skos:prefLabel "respiratory protection"@en ;
                       
                       skos:inScheme :IAHGlossary ;
                       
                       skos:broader nreo:Equipment .



###  http://rdf.iit.demokritos.gr/2014/iah/Resuspension

:Resuspension rdf:type nreo:NuclearEngineeringTerm ,
                       nreo:SpatialTemporalTerm ,
                       owl:NamedIndividual ;
              
              skos:definition "A renewed suspension of contaminated particles in the air. The subsequent inhalation of radioactivity is recognised as a potentially significant exposure pathway. Many factors influence resuspension, including climate, wind speed, time since deposition, etc."@en ;
              
              skos:prefLabel "resuspension"@en ;
              
              skos:inScheme :IAHGlossary ;
              
              skos:broader nreo:Event .



###  http://rdf.iit.demokritos.gr/2014/iah/ShortlivedRadionuclide

:ShortlivedRadionuclide rdf:type nreo:NuclearEngineeringTerm ,
                                 owl:NamedIndividual ;
                        
                        skos:definition "Defined for the Handbook as radionuclides with a radioactive half-life of less than three weeks."@en ;
                        
                        skos:prefLabel "short-lived radionuclide"@en ;
                        
                        skos:inScheme :IAHGlossary ;
                        
                        skos:broader :Radionuclide .



###  http://rdf.iit.demokritos.gr/2014/iah/Stakeholder

:Stakeholder rdf:type nreo:AgentTerm ,
                      owl:NamedIndividual ;
             
             skos:definition "A person or group of persons with a direct or perceived interest, involvement, or investment in something."@en ;
             
             skos:prefLabel "stakeholder"@en ;
             
             skos:inScheme :IAHGlossary ;
             
             skos:broader nreo:Role .



###  http://rdf.iit.demokritos.gr/2014/iah/StochasticEffect

:StochasticEffect rdf:type nreo:EffectsTerm ,
                           owl:NamedIndividual ;
                  
                  skos:definition "A radiation induced health effect characterised by a severity which does not depend on dose and for which no lower threshold exists. The probability of such an effect being observed is proportional to the dose. An example of a stochastic effect is cancer."@en ;
                  
                  skos:prefLabel "stochastic effect"@en ;
                  
                  skos:altLabel "stochastic health effect"@en ;
                  
                  skos:inScheme :IAHGlossary ;
                  
                  skos:broader nreo:Effect .



###  http://rdf.iit.demokritos.gr/2014/iah/Surface

:Surface rdf:type nreo:SpatialTemporalTerm ,
                  owl:NamedIndividual ;
         
         skos:definition "Examples of surfaces considered in this Handbook include: soil, vegetation and buildings. Management options usually target a specific surface. A surface can have a depth, (e.g. soil) and this can influence the effectiveness of management options in removing contamination from the surface."@en ;
         
         skos:prefLabel "surface"@en ;
         
         skos:inScheme :IAHGlossary ;
         
         skos:broader nreo:Location .



###  http://rdf.iit.demokritos.gr/2014/iah/Worker

:Worker rdf:type nreo:AgentTerm ,
                 owl:NamedIndividual ;
        
        skos:definition "In the Handbook, a worker is defined as an individual who is formally involved with the practical implementation of a recovery strategy. Exposures to workers must be controlled."@en ;
        
        skos:prefLabel "worker"@en ;
        
        skos:inScheme :IAHGlossary ;
        
        skos:broader nreo:Role .



###  http://www.iit.demokritos.gr/ont/nreo#Effect

nreo:Effect rdf:type owl:NamedIndividual ;
            
            skos:topConceptOf :IAHGlossary .



###  http://www.iit.demokritos.gr/ont/nreo#Equipment

nreo:Equipment rdf:type owl:NamedIndividual ;
               
               skos:topConceptOf :IAHGlossary .



###  http://www.iit.demokritos.gr/ont/nreo#Event

nreo:Event rdf:type owl:NamedIndividual ;
           
           skos:topConceptOf :IAHGlossary .



###  http://www.iit.demokritos.gr/ont/nreo#Location

nreo:Location rdf:type owl:NamedIndividual ;
              
              skos:topConceptOf :IAHGlossary .



###  http://www.iit.demokritos.gr/ont/nreo#PhysicsThing

nreo:PhysicsThing rdf:type owl:NamedIndividual ;
                  
                  skos:topConceptOf :IAHGlossary .



###  http://www.iit.demokritos.gr/ont/nreo#Procedure

nreo:Procedure rdf:type owl:NamedIndividual ;
               
               skos:topConceptOf :IAHGlossary .



###  http://www.iit.demokritos.gr/ont/nreo#Quantity

nreo:Quantity rdf:type owl:NamedIndividual ;
              
              skos:topConceptOf :IAHGlossary .



###  http://www.iit.demokritos.gr/ont/nreo#Radiation

nreo:Radiation rdf:type owl:NamedIndividual ;
               
               skos:inScheme :IAHGlossary .



###  http://www.iit.demokritos.gr/ont/nreo#Role

nreo:Role rdf:type owl:NamedIndividual ;
          
          skos:topConceptOf :IAHGlossary .




###  Generated by the OWL API (version 3.4.2) http://owlapi.sourceforge.net

